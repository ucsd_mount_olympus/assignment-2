#include "mbed.h"
#include "OledColorDisplay.h"
#include "LSM303DLHC.h"

#include <string>
#include <iostream>
#include <algorithm>
#include <iomanip>
#include <sstream>

Serial pc(USBTX, USBRX); // tx, rx

PwmOut myled1(LED1);
PwmOut myled2(LED2);
PwmOut myled3(LED3);
PwmOut myled4(LED4);

// Stepper motor Connections
// - to board ground
// + to board 5V power
// IN1 to Mbed p30
// IN2 to Mbed p29
// IN3 to Mbed p28
// IN4 to Mbed p27

DigitalOut gpio30(p26); //p26
DigitalOut gpio29(p25); //p25
DigitalOut gpio28(p24); //p24
DigitalOut gpio27(p23); //p23

Timer sonar;

// Oled Connections
// GND to board ground
// Vin to board 5V power
// MISO to Mbed p6 (miso)
// OLEDCS to Mbed p8
// RESET to Mbed p9
// DC to Mbed p10
// SCK to Mbed Mbed p7 (sck)
// MOSI to Mbed p5 (mosi)

OledColorDisplay OledDisplay(p5, p6, p7, p8, p9, p10);

// Pushbutton connections
// Board ground --> Push Button --> p11

InterruptIn pb(p11);

// Sonar Connections

// Gnd to board ground
// Vcc to board 5V power
// Echo to mbed p12
// Trig to mbed p13

InterruptIn echo(p12);
DigitalOut trigger(p13);

// Accelerometer Connections
// GND to board ground
// Vin to board 5V power
// SDA to p28 (I2C sda)
// SDC to p27 (I2C scl)

LSM303DLHC magnetometer(p28, p27);

int magReading[3] = {0};

char *cardDir;

#define Rad2Dree       57.295779513082320876798154814105f
#define YOFF  0.0    // Y-Offset
#define XOFF  0.0      // X-Offset
#define WOFF  0.0     // Winkel-Offset
//#define DECLINATION 11.33 // Declination (degrees) in San Diego,CA.

int MyUltrasonicSensor(float sensor_angle) {

  int my_distance = 0;
  int my_correction = 0;

  trigger = 1;
  sonar.reset();
  wait_us(5.0);
  trigger = 0;
  //wait for echo high
  while (echo==0) {};
  //echo high, so start timer
  sonar.start();
  //wait for echo low
  while (echo==1) {};
  //stop timer and read value
  sonar.stop();
  //subtract software overhead timer delay and scale to inches
  my_distance = (sonar.read_us()-my_correction)/148.0;
    //wait so that any echo(s) return before sending another ping
  //wait(0.02);

  if ( my_distance <= 30 ) {
    pc.printf("\n");
    pc.printf("MO_INFO: Found an object %d inches away, %f degrees from center\n",my_distance,sensor_angle);
    pc.printf("\n");
    return my_distance;
  } else {
    return 0;
  }

}

void MyDrawString(std::string &mydrawstring, int start_x, int start_y, bool clearmode, bool pagemode) {

  int n = mydrawstring.length();

  int current_x = start_x;
  int current_y = start_y;

  for (int i=0; i<n; i++) {

    string tempc;

    tempc = mydrawstring[i];

    //pc.printf("\n");
    //pc.printf("MO_INFO: Trying to draw %s string at X %i Y %i \n",tempc.c_str(),current_x, current_y);
    //pc.printf("\n");

    if ( current_x == start_x && clearmode == 1 ) {

      OledDisplay.DrawFilledRect(1, current_y, 128, 9, BLACK);

      if ( (current_y + 9) < 128 && pagemode == 1 ) {

        OledDisplay.DrawFilledRect(1, current_y + 8, 128, 9, BLACK);

      }

    }

    OledDisplay.MyDrawChar(tempc,current_x,current_y);

    current_x = current_x + 6;

    if ( (current_x + 5) > 128 ) {

      current_x = start_x;

      if ( (current_y + 9) < 128 ) {

        current_y = current_y + 9;

      } else {

        current_y = start_y;

      }

    }

  }

}

void MyDrawStringWithFloat(std::string &mydrawstring, int start_x, int start_y, bool clearmode, bool pagemode, float &mydrawwithfloat, int sigdigits) {

    //pc.printf("\n");
    //pc.printf("MO_INFO: Trying to draw %s string at X %i Y %i %f\n",mydrawstring.c_str(),start_x, start_y, mydrawwithfloat);
    //pc.printf("\n");

    // Convert float to string
    stringstream stream;
    stream << fixed << setprecision(sigdigits) << mydrawwithfloat;
    string mydrawwithfloatstring = stream.str();

    // Find the %f, currently only supports one %f
    int found=mydrawstring.find("%f");

    // Replace the %f with the float value
    std::string mytempstr=mydrawstring;
    mytempstr.replace(found,2,mydrawwithfloatstring);

    std::string mynewdrawstring=mytempstr;

    //pc.printf("\n");
    //pc.printf("MO_INFO: Trying to draw %s string at X %i Y %i %f\n",mynewdrawstring.c_str(),start_x, start_y, mydrawwithfloat);
    //pc.printf("\n");

    MyDrawString(mynewdrawstring,start_x,start_y,clearmode,pagemode);

}


void MyDrawStringWithString(std::string &mydrawstring, int start_x, int start_y, bool clearmode, bool pagemode, std::string &mydrawwithstring) {

    //pc.printf("\n");
    //pc.printf("MO_INFO: Trying to draw %s string at X %i Y %i %s\n",mydrawstring.c_str(),start_x, start_y, mydrawwithstring.c_str());
    //pc.printf("\n");

    // Find the %s, currently only supports one %s
    int found=mydrawstring.find("%s");

    // Replace the %s with the float value
    std::string mytempstr=mydrawstring;
    mytempstr.replace(found,2,mydrawwithstring);

    std::string mynewdrawstring=mytempstr;

    //pc.printf("\n");
    //pc.printf("MO_INFO: Trying to draw %s string at X %i Y %i %s\n",mynewdrawstring.c_str(),start_x, start_y, mydrawwithstring.c_str());
    //pc.printf("\n");

    MyDrawString(mynewdrawstring,start_x,start_y,clearmode,pagemode);

}

void MyDrawStringWithInt(std::string &mydrawstring, int start_x, int start_y, bool clearmode, bool pagemode, int &mydrawwithint) {

    // Convert int to string
    stringstream stream;
    stream << fixed << mydrawwithint;
    string mydrawwithintstring = stream.str();

    // Find the %i, currently only supports one %i
    int found=mydrawstring.find("%i");

    // Replace the %i with the int value
    std::string mytempstr=mydrawstring;
    mytempstr.replace(found,2,mydrawwithintstring);

    std::string mynewdrawstring=mytempstr;

    MyDrawString(mynewdrawstring,start_x,start_y,clearmode,pagemode);

}

void MyDrawStringOfStrings(std::string &mydrawstring, int start_x, int start_y, bool clearmode, bool pagemode) {

  int n = mydrawstring.length();

  int current_x = start_x;
  int current_y = start_y;

  for (int i=0; i<n; i++) {

    string tempc;

    tempc = mydrawstring[i];

    //pc.printf("\n");
    //pc.printf("MO_INFO: Trying to draw %s string at X %i Y %i \n",tempc.c_str(),current_x, current_y);
    //pc.printf("\n");

    OledDisplay.MyDrawChar(tempc,current_x,current_y);

    current_x = current_x + 6;

  }

}



double getSecondsSinceY2K () {

   time_t timer;
   struct tm y2k = {0};
   double seconds;

   y2k.tm_hour = 0;
   y2k.tm_min = 0;
   y2k.tm_sec = 0;
   y2k.tm_year = 100;
   y2k.tm_mon = 0;
   y2k.tm_mday = 1;
   time(&timer);  /* get current time; same as: timer = time(NULL)  */

   seconds = difftime(timer,mktime(&y2k));

   //pc.printf ("%.f seconds since January 1, 2000 in the current timezone", seconds);

   return seconds;

}

void InitializeRadarScreen () {

  const float PI = 3.1415927;

  // Plot the area we will scan
  OledDisplay.DrawLine(64, 64, 64 + cos((2*PI/32)*0)*60, 64 - sin((2*PI/32)*0)*60, GREEN);
  OledDisplay.DrawLine(64, 64, 64 + cos((2*PI/32)*16)*60, 64 - sin((2*PI/32)*16)*60, GREEN);

  for (int i=0; i<256; i++) {

     OledDisplay.DrawPixel(64 + cos((2*PI/512)*i)*60, 64 - sin((2*PI/512)*i)*60, GREEN);
     OledDisplay.DrawPixel(64 + cos((2*PI/512)*i)*45, 64 - sin((2*PI/512)*i)*45, GREEN);
     OledDisplay.DrawPixel(64 + cos((2*PI/512)*i)*30, 64 - sin((2*PI/512)*i)*30, GREEN);
     OledDisplay.DrawPixel(64 + cos((2*PI/512)*i)*15, 64 - sin((2*PI/512)*i)*15, GREEN);

  }

}


void drawXmarkstheSpot (int distancetox, int rot_angle, std::string &startdirection) {

  const float PI = 3.1415927;

  int startdirectionpointer;

  // Initialize Compass Array
  string directions[16] = {"W","WNW","NW","NNW","N","NNE","NE","ENE","E","ESE","SE","SSE","S","SSW","SW","WSW"};

  std::string print_current_direction;

  std::string current_direction;

  // Find start direction
  for (int i = 0; i < 16; i++) {
      if ( directions[i] == startdirection ) {
          startdirectionpointer = i;
      }
  }

  //pc.printf("\n");
  //pc.printf("MO_INFO: Direction pointer is %i\n",startdirectionpointer);
  //pc.printf("\n");

  int directionadder = rot_angle/32;

  int currentdirectionpointer = startdirectionpointer+directionadder;

  if ( currentdirectionpointer > 15 ) {

      currentdirectionpointer = currentdirectionpointer - 16;
  }

  //pc.printf("\n");
  //pc.printf("MO_INFO: Direction pointer is %i, rot_angle is %i, adder is %i\n",startdirectionpointer,rot_angle,directionadder);
  //pc.printf("\n");

  current_direction = directions[currentdirectionpointer];

  if ( distancetox > 0 ) {

     OledDisplay.DrawPixel(64 - cos((2*PI/512)*rot_angle)*distancetox*2+0,  64 - sin((2*PI/512)*rot_angle)*distancetox*2+0,  YELLOW);

     OledDisplay.DrawPixel(64 - cos((2*PI/512)*rot_angle)*distancetox*2-2,  64 - sin((2*PI/512)*rot_angle)*distancetox*2+0,  YELLOW);
     OledDisplay.DrawPixel(64 - cos((2*PI/512)*rot_angle)*distancetox*2-1,  64 - sin((2*PI/512)*rot_angle)*distancetox*2+0,  YELLOW);
     OledDisplay.DrawPixel(64 - cos((2*PI/512)*rot_angle)*distancetox*2+1,  64 - sin((2*PI/512)*rot_angle)*distancetox*2+0,  YELLOW);
     OledDisplay.DrawPixel(64 - cos((2*PI/512)*rot_angle)*distancetox*2+2,  64 - sin((2*PI/512)*rot_angle)*distancetox*2+0,  YELLOW);

     OledDisplay.DrawPixel(64 - cos((2*PI/512)*rot_angle)*distancetox*2+0,  64 - sin((2*PI/512)*rot_angle)*distancetox*2-2,  YELLOW);
     OledDisplay.DrawPixel(64 - cos((2*PI/512)*rot_angle)*distancetox*2+0,  64 - sin((2*PI/512)*rot_angle)*distancetox*2-1,  YELLOW);
     OledDisplay.DrawPixel(64 - cos((2*PI/512)*rot_angle)*distancetox*2+0,  64 - sin((2*PI/512)*rot_angle)*distancetox*2+1,  YELLOW);
     OledDisplay.DrawPixel(64 - cos((2*PI/512)*rot_angle)*distancetox*2+0,  64 - sin((2*PI/512)*rot_angle)*distancetox*2+2,  YELLOW);

     OledDisplay.DrawPixel(64 - cos((2*PI/512)*rot_angle)*distancetox*2-1,  64 - sin((2*PI/512)*rot_angle)*distancetox*2-1,  YELLOW);
     OledDisplay.DrawPixel(64 - cos((2*PI/512)*rot_angle)*distancetox*2-1,  64 - sin((2*PI/512)*rot_angle)*distancetox*2+1,  YELLOW);
     OledDisplay.DrawPixel(64 - cos((2*PI/512)*rot_angle)*distancetox*2+1,  64 - sin((2*PI/512)*rot_angle)*distancetox*2-1,  YELLOW);
     OledDisplay.DrawPixel(64 - cos((2*PI/512)*rot_angle)*distancetox*2+1,  64 - sin((2*PI/512)*rot_angle)*distancetox*2+1,  YELLOW);

     //pc.printf("\n");
     //pc.printf("MO_INFO: Making it here 1\n");
     //pc.printf("\n");

     print_current_direction = "   ";
     MyDrawString(print_current_direction,106,2,false,false);
     print_current_direction = "%s";
     MyDrawStringWithString(print_current_direction,106,2,false,false,current_direction);

     std::string object_found_details;
     object_found_details = "Object found,"; MyDrawString(object_found_details,2,80,true,false);
     object_found_details = "%i"; MyDrawStringWithInt(object_found_details,80,80,false,false,distancetox);
     object_found_details = "inches away, "; MyDrawString(object_found_details,2,89,true,false);
     object_found_details = "%s"; MyDrawStringWithString(object_found_details,80,89,false,false,current_direction);

   } else {

     //pc.printf("\n");
     //pc.printf("MO_INFO: Making it here 2\n");
     //pc.printf("\n");

     print_current_direction = "   ";
     MyDrawString(print_current_direction,106,2,false,false);
     print_current_direction = "%s";
     MyDrawStringWithString(print_current_direction,106,2,false,false,current_direction);

   }

}

string calcHeading(void) {

    double x,y;
    double hdg;

    magnetometer.readMag(magReading);

    x = magReading[0] + XOFF;
    y = magReading[1] + YOFF;

    hdg = atan2(y, x);
    hdg *= Rad2Dree;

    hdg -= WOFF;
    if (hdg <0) hdg = 360.0 + hdg; {
    //sp.printf("Mag angle = %f\r\n", hdg);
    //return (hdg);
    }

    if (337.5 <= hdg && hdg < 360) {
        cardDir = "N";
    } else if (0 <= hdg && hdg < 22.5) {
        cardDir = "N";
    } else if (22.5 <= hdg && hdg < 67.5) {
        cardDir = "NE";
    } else if (67.5 <= hdg && hdg < 112.5) {
        cardDir = "E";
    } else if (112.5 <= hdg && hdg < 157.5) {
        cardDir = "SE";
    } else if (157.5 <= hdg && hdg < 202.5) {
        cardDir = "S";
    } else if (202.5 <= hdg && hdg < 247.5) {
        cardDir = "SW";
    } else if (247.5 <= hdg && hdg < 292.5) {
        cardDir = "W";
    } else if (292.5 <= hdg && hdg < 337.5) {
        cardDir = "NW";
    } else {
        cardDir = "W";
        pc.printf("\n");
        pc.printf("MO_INFO: Something went wrong, defaulting to South direction\n");
        pc.printf("\n");
    }

    return cardDir;

}


int main() {

  float assignment = 2.9;

  const float PI = 3.1415927;

  // Priority One Exit
  float priority_one_exit_count = 0;
  int priority_one_exit = 0;

  int initialize_radar_screen = 1;

  // Will overwrite this once compass is working
  std::string initial_radar_direction = "W";

  pc.printf("\n");
  pc.printf("               Mount Olympus Code Initializing              \n");
  pc.printf("        Brought to you by Olu Afolayan and Dan Taylor       \n");
  pc.printf("                 WES237A - Assignment %0.1f                   \n",assignment);
  pc.printf("\n");

  wait(2.0);
  OledDisplay.DisplayClear();
  wait(0.5);

  std::string mo_logo;

  mo_logo = "       MMM          "; MyDrawString(mo_logo,4,20,true,false);
  mo_logo = "      MMMMM MM      "; MyDrawString(mo_logo,4,28,true,false);
  mo_logo = "     MMMMMMMMMM     "; MyDrawString(mo_logo,4,36,true,false);
  mo_logo = "    MMMM    MMMM    "; MyDrawString(mo_logo,4,44,true,false);
  mo_logo = "   MMMM  MM  MMMM   "; MyDrawString(mo_logo,4,52,true,false);
  mo_logo = "   MMMM  MM  MMMM   "; MyDrawString(mo_logo,4,60,true,false);
  mo_logo = "  MMMMMM    MMMMMM  "; MyDrawString(mo_logo,4,68,true,false);
  mo_logo = " MMMMMMMMMMMMMMMMMM "; MyDrawString(mo_logo,4,76,true,false);
  mo_logo = "MMMMMMMMMMMMMMMMMMMM"; MyDrawString(mo_logo,4,84,true,false);
  mo_logo = "   Assignment %f   "; MyDrawStringWithFloat(mo_logo,4,100,true,false,assignment,1);

  wait(5.0);
  OledDisplay.DisplayClear();

  int count=0;
  int old_count=0;

  // Use internal pullup for pushbutton
  pb.mode(PullUp);

  // Delay for initial pullup to take effect
  wait(.001);

  // Delay for magnetometer to initialize
  magnetometer.init();

  wait(.001);

  int old_pb;
  int new_pb;
  int new_pb_wait;

  int rot_angle;
  int rot_dir;

  //int correction = 0;

  char c ;
  char last_c;

  int displayinstructions = 1;

  sonar.reset();
  // measure actual software polling timer delays
  // delay used later in time correction
  // start timer
  sonar.start();
  // min software polling delay to read echo pin
  while (echo==2) {};
  // stop timer
  sonar.stop();
  // read timer
  //correction = sonar.read_us();
  //printf("MO_INFO: Approximate software overhead timer delay is %d uS\n",correction);

  wait(1.0);
  OledDisplay.DisplayClear();

  OledDisplay.DisplayClear();

  while(1) {

   priority_one_exit = 0;  priority_one_exit_count = 0; count = 0; old_count = 0; old_pb = 1; new_pb = 1; new_pb_wait = 1; myled4 = 0; myled3 = 0; myled2 =0; myled1 = 0; rot_angle = 1; rot_dir = 0;

   if ( displayinstructions == 1 ) {

     pc.printf("\n");
     pc.printf("MO_INFO: Step1 : Position the radar station...\n");
     pc.printf("MO_INFO: --> Press 'f' to rotate the radar station clockwise\n");
     pc.printf("MO_INFO: --> Press 'r' to rotate the radar station counter-clockwise\n");
     pc.printf("MO_INFO: --> Press 's' to stop the radar station in it's current location\n");
     pc.printf("\n");

     std::string radar_instructions;
     radar_instructions = "Step 1 - Position the radar system"; MyDrawString(radar_instructions,2,8,true,false);
     radar_instructions = "-Press 'f' to rotate clockwise"; MyDrawString(radar_instructions,2,26,true,false);
     radar_instructions = "-Press 'r' to rotate counter-clockwise"; MyDrawString(radar_instructions,2,44,true,false);
     radar_instructions = "-Press 's' to stop "; MyDrawString(radar_instructions,2,62,true,false);
     radar_instructions = "Step 2 - Press the   push button to begin radar operation"; MyDrawString(radar_instructions,2,80,true,false);

     pc.printf("\n");
     pc.printf("MO_INFO: Step2 : Press the push button to begin radar operation\n");
     pc.printf("MO_INFO: Step3 : Press the push button again to pause radar operation\n");
     pc.printf("MO_INFO: Step4 : Press the push button again to resume radar operation\n");
     pc.printf("\n");

     displayinstructions = 0;

   }

   while (priority_one_exit == 0) {

    new_pb = pb;

    if (pc.readable()) {
      c = pc.getc();
      last_c = c;
      if ( last_c == 'f') {
          rot_angle=1; rot_dir=0;
      }
      if ( last_c == 'r') {
          rot_angle=1; rot_dir=1;
      }
      if ( last_c == 's') {
         rot_angle=1; rot_dir=0;
      }
    }

    if ( new_pb == 0 && old_pb == 1 ) {

      // Push button needs to be held for 0.005
      wait(.005);

      new_pb_wait = pb;

      if ( new_pb_wait == 0 ) {

        pc.printf("\n");
        pc.printf("MO_INFO: Pushbutton Pressed\n");
        pc.printf("\n");

        double pushbuttonpressed = getSecondsSinceY2K();

        //pc.printf("\n");
        //pc.printf("MO_INFO: Push button pressed at %.f\n", pushbuttonpressed);
        //pc.printf("\n");

        while (pb==0) {

          // Waiting for user to release push button

          double pushbuttonreleased = getSecondsSinceY2K();

          //pc.printf("\n");
          //pc.printf("MO_INFO: Push button released at %.f\n", pushbuttonreleased);
          //pc.printf("\n");

          priority_one_exit_count = pushbuttonreleased - pushbuttonpressed;

          if ( priority_one_exit_count >= 3 ) {

             priority_one_exit = 1;

             pc.printf("\n");
             pc.printf("MO_INFO: Priority One Exit Initiated\n");
             pc.printf("\n");

             displayinstructions = 1;

          }

        }

        if ( count == 0 ) {

          count = 1; myled4 = 0; myled3 = 0; myled2 = 0; myled1 = 1; new_pb=1;

        } else if ( count == 1 ) {

          count = 0; myled4 = 0; myled3 = 0; myled2 = 0; myled1 = 0; new_pb=1;

        } else {

          count = 0; myled4 = 0; myled3 = 0; myled2 = 0; myled1 = 0; new_pb=1;

        }

        //pc.printf("MO_INFO: count %d old_count %d old_pb %d new_pb %d pb %f myled4 %f myled3 %f myled2 %f myled1 %f\n",count,old_count,old_pb,new_pb,pb.read(),myled4.read(),myled3.read(),myled2.read(),myled1.read());

      }

    }

    if ( count != old_count && count == 1 ) {

      pc.printf("\n");
      pc.printf("MO_INFO: Press push button to pause the radar\n");
      pc.printf("\n");

      std::string radar_instructions;
      radar_instructions = "Press push button to pause the radar"; MyDrawString(radar_instructions,2,80,true,false);

    }

    if ( count != old_count && count == 0 ) {

      pc.printf("\n");
      pc.printf("MO_INFO: Press push button to resume the radar\n");
      pc.printf("\n");

      std::string radar_instructions;
      radar_instructions = "Press push button to resume operation"; MyDrawString(radar_instructions,2,80,true,false);


    }

    if ( last_c == 'f') {

        gpio30=1; gpio29=0; gpio28=0; gpio27=0; wait(.005); //stepper_position = 1
        gpio30=1; gpio29=1; gpio28=0; gpio27=0; wait(.005); //stepper_position = 2
        gpio30=0; gpio29=1; gpio28=0; gpio27=0; wait(.005); //stepper_position = 3
        gpio30=0; gpio29=1; gpio28=1; gpio27=0; wait(.005); //stepper_position = 4
        gpio30=0; gpio29=0; gpio28=1; gpio27=0; wait(.005); //stepper_position = 5
        gpio30=0; gpio29=0; gpio28=1; gpio27=1; wait(.005); //stepper_position = 6
        gpio30=0; gpio29=0; gpio28=0; gpio27=1; wait(.005); //stepper_position = 7
        gpio30=1; gpio29=0; gpio28=0; gpio27=1; wait(.005); //stepper_position = 8

        if ( rot_angle < 512 ) {
          rot_angle++;
        } else {
          rot_angle=1;
        }

    }

    if ( last_c == 'r') {

        gpio30=1; gpio29=0; gpio28=0; gpio27=0; wait(.005); //stepper_position = 1
        gpio30=1; gpio29=0; gpio28=0; gpio27=1; wait(.005); //stepper_position = 8
        gpio30=0; gpio29=0; gpio28=0; gpio27=1; wait(.005); //stepper_position = 7
        gpio30=0; gpio29=0; gpio28=1; gpio27=1; wait(.005); //stepper_position = 6
        gpio30=0; gpio29=0; gpio28=1; gpio27=0; wait(.005); //stepper_position = 5
        gpio30=0; gpio29=1; gpio28=1; gpio27=0; wait(.005); //stepper_position = 4
        gpio30=0; gpio29=1; gpio28=0; gpio27=0; wait(.005); //stepper_position = 3
        gpio30=1; gpio29=1; gpio28=0; gpio27=0; wait(.005); //stepper_position = 2

        if ( rot_angle == 1 ) {
          rot_angle=512;
        } else {
          rot_angle--;
        }

    }

    if ( last_c == 's' ) {

      // Stop in this position

      if ( initialize_radar_screen == 1 ) {

        OledDisplay.DisplayClear();

        InitializeRadarScreen ();

        std::string radar_instructions;
        radar_instructions = "Press push button to begin operation"; MyDrawString(radar_instructions,2,80,true,false);

        if ( assignment > 2.8999 && assignment < 2.9001 ) {

          pc.printf("\n");
          pc.printf("MO_INFO: Making it here....\n");
          pc.printf("\n");

          initial_radar_direction = calcHeading();

        } else {
          initial_radar_direction = "W";
        }

      }

      initialize_radar_screen = 0;

    }

    if ( count == 1 ) {

        if ( rot_angle <= 257 && rot_dir == 0  ) {

          gpio30=1; gpio29=0; gpio28=0; gpio27=0; wait(.005); //stepper_position = 1
          gpio30=1; gpio29=1; gpio28=0; gpio27=0; wait(.005); //stepper_position = 2
          gpio30=0; gpio29=1; gpio28=0; gpio27=0; wait(.005); //stepper_position = 3
          gpio30=0; gpio29=1; gpio28=1; gpio27=0; wait(.005); //stepper_position = 4
          gpio30=0; gpio29=0; gpio28=1; gpio27=0; wait(.005); //stepper_position = 5
          gpio30=0; gpio29=0; gpio28=1; gpio27=1; wait(.005); //stepper_position = 6
          gpio30=0; gpio29=0; gpio28=0; gpio27=1; wait(.005); //stepper_position = 7
          gpio30=1; gpio29=0; gpio28=0; gpio27=1; wait(.005); //stepper_position = 8

          if (rot_angle==32)  {
            int foundobject = MyUltrasonicSensor(-67.50);
            OledDisplay.DrawLine(64, 64, 64 - cos((2*PI/512)*rot_angle)*60,  64 - sin((2*PI/512)*rot_angle)*60,  GREEN);
            drawXmarkstheSpot(foundobject,rot_angle,initial_radar_direction);
          }
          if (rot_angle==64)  {
            OledDisplay.DrawLine(64, 64, 64 - cos((2*PI/512)*(rot_angle-32))*60,  64 - sin((2*PI/512)*(rot_angle-32))*60,  BLACK);
            int foundobject = MyUltrasonicSensor(-45.00);
            OledDisplay.DrawLine(64, 64, 64 - cos((2*PI/512)*rot_angle)*60,  64 - sin((2*PI/512)*rot_angle)*60,  GREEN);
            drawXmarkstheSpot(foundobject,rot_angle,initial_radar_direction);
          }
          if (rot_angle==96)  {
            OledDisplay.DrawLine(64, 64, 64 - cos((2*PI/512)*(rot_angle-32))*60,  64 - sin((2*PI/512)*(rot_angle-32))*60,  BLACK);
            int foundobject = MyUltrasonicSensor(-22.50);
            OledDisplay.DrawLine(64, 64, 64 - cos((2*PI/512)*rot_angle)*60,  64 - sin((2*PI/512)*rot_angle)*60,  GREEN);
            drawXmarkstheSpot(foundobject,rot_angle,initial_radar_direction);
          }
          if (rot_angle==128) {
            OledDisplay.DrawLine(64, 64, 64 - cos((2*PI/512)*(rot_angle-32))*60,  64 - sin((2*PI/512)*(rot_angle-32))*60,  BLACK);
            int foundobject = MyUltrasonicSensor(0.00);
            OledDisplay.DrawLine(64, 64, 64 - cos((2*PI/512)*rot_angle)*60,  64 - sin((2*PI/512)*rot_angle)*60,  GREEN);
            drawXmarkstheSpot(foundobject,rot_angle,initial_radar_direction);
          }
          if (rot_angle==160) {
            OledDisplay.DrawLine(64, 64, 64 - cos((2*PI/512)*(rot_angle-32))*60,  64 - sin((2*PI/512)*(rot_angle-32))*60,  BLACK);
            int foundobject = MyUltrasonicSensor(22.50);
            OledDisplay.DrawLine(64, 64, 64 - cos((2*PI/512)*rot_angle)*60,  64 - sin((2*PI/512)*rot_angle)*60,  GREEN);
            drawXmarkstheSpot(foundobject,rot_angle,initial_radar_direction);
          }
          if (rot_angle==192) {
            OledDisplay.DrawLine(64, 64, 64 - cos((2*PI/512)*(rot_angle-32))*60,  64 - sin((2*PI/512)*(rot_angle-32))*60,  BLACK);
            int foundobject = MyUltrasonicSensor(45.00);
            OledDisplay.DrawLine(64, 64, 64 - cos((2*PI/512)*rot_angle)*60,  64 - sin((2*PI/512)*rot_angle)*60,  GREEN);
            drawXmarkstheSpot(foundobject,rot_angle,initial_radar_direction);
          }
          if (rot_angle==224) {
            OledDisplay.DrawLine(64, 64, 64 - cos((2*PI/512)*(rot_angle-32))*60,  64 - sin((2*PI/512)*(rot_angle-32))*60,  BLACK);
            int foundobject = MyUltrasonicSensor(67.50);
            OledDisplay.DrawLine(64, 64, 64 - cos((2*PI/512)*rot_angle)*60,  64 - sin((2*PI/512)*rot_angle)*60,  GREEN);
            drawXmarkstheSpot(foundobject,rot_angle,initial_radar_direction);
          }

          if (rot_angle == 256) {

            std::string radar_instructions;
            radar_instructions = "Press push button to pause the radar"; MyDrawString(radar_instructions,2,80,true,false);

            OledDisplay.DrawFilledRect(0, 0, 128, 64, BLACK);

            InitializeRadarScreen ();

          }

          rot_angle++;

        } else if ( rot_angle <= 257 && rot_dir == 1 ) {

          gpio30=1; gpio29=0; gpio28=0; gpio27=0; wait(.005); //stepper_position = 1
          gpio30=1; gpio29=0; gpio28=0; gpio27=1; wait(.005); //stepper_position = 8
          gpio30=0; gpio29=0; gpio28=0; gpio27=1; wait(.005); //stepper_position = 7
          gpio30=0; gpio29=0; gpio28=1; gpio27=1; wait(.005); //stepper_position = 6
          gpio30=0; gpio29=0; gpio28=1; gpio27=0; wait(.005); //stepper_position = 5
          gpio30=0; gpio29=1; gpio28=1; gpio27=0; wait(.005); //stepper_position = 4
          gpio30=0; gpio29=1; gpio28=0; gpio27=0; wait(.005); //stepper_position = 3
          gpio30=1; gpio29=1; gpio28=0; gpio27=0; wait(.005); //stepper_position = 2

          if (rot_angle==32)  {
            OledDisplay.DrawLine(64, 64, 64 - cos((2*PI/512)*(rot_angle+32))*60, 64 - sin((2*PI/512)*(rot_angle+32))*60, BLACK);
            int foundobject = MyUltrasonicSensor(-67.50);
            OledDisplay.DrawLine(64, 64, 64 - cos((2*PI/512)*rot_angle)*60,  64 - sin((2*PI/512)*rot_angle)*60,  GREEN);
            drawXmarkstheSpot(foundobject,rot_angle,initial_radar_direction);
          }
          if (rot_angle==64)  {
            OledDisplay.DrawLine(64, 64, 64 - cos((2*PI/512)*(rot_angle+32))*60, 64 - sin((2*PI/512)*(rot_angle+32))*60, BLACK);
            int foundobject = MyUltrasonicSensor(-45.00);
            OledDisplay.DrawLine(64, 64, 64 - cos((2*PI/512)*rot_angle)*60,  64 - sin((2*PI/512)*rot_angle)*60,  GREEN);
            drawXmarkstheSpot(foundobject,rot_angle,initial_radar_direction);
          }
          if (rot_angle==96)  {
            OledDisplay.DrawLine(64, 64, 64 - cos((2*PI/512)*(rot_angle+32))*60, 64 - sin((2*PI/512)*(rot_angle+32))*60, BLACK);
            int foundobject = MyUltrasonicSensor(-22.50);
            OledDisplay.DrawLine(64, 64, 64 - cos((2*PI/512)*rot_angle)*60,  64 - sin((2*PI/512)*rot_angle)*60,  GREEN);
            drawXmarkstheSpot(foundobject,rot_angle,initial_radar_direction);
          }
          if (rot_angle==128) {
            OledDisplay.DrawLine(64, 64, 64 - cos((2*PI/512)*(rot_angle+32))*60, 64 - sin((2*PI/512)*(rot_angle+32))*60, BLACK);
            int foundobject = MyUltrasonicSensor(0.00);
            OledDisplay.DrawLine(64, 64, 64 - cos((2*PI/512)*rot_angle)*60,  64 - sin((2*PI/512)*rot_angle)*60,  GREEN);
            drawXmarkstheSpot(foundobject,rot_angle,initial_radar_direction);
          }
          if (rot_angle==160) {
            OledDisplay.DrawLine(64, 64, 64 - cos((2*PI/512)*(rot_angle+32))*60, 64 - sin((2*PI/512)*(rot_angle+32))*60, BLACK);
            int foundobject = MyUltrasonicSensor(22.50);
            OledDisplay.DrawLine(64, 64, 64 - cos((2*PI/512)*rot_angle)*60,  64 - sin((2*PI/512)*rot_angle)*60,  GREEN);
            drawXmarkstheSpot(foundobject,rot_angle,initial_radar_direction);
          }
          if (rot_angle==192) {
            OledDisplay.DrawLine(64, 64, 64 - cos((2*PI/512)*(rot_angle+32))*60, 64 - sin((2*PI/512)*(rot_angle+32))*60, BLACK);
            int foundobject = MyUltrasonicSensor(45.00);
            OledDisplay.DrawLine(64, 64, 64 - cos((2*PI/512)*rot_angle)*60,  64 - sin((2*PI/512)*rot_angle)*60,  GREEN);
            drawXmarkstheSpot(foundobject,rot_angle,initial_radar_direction);
          }
          if (rot_angle==224) {
            int foundobject = MyUltrasonicSensor(67.50);
            OledDisplay.DrawLine(64, 64, 64 - cos((2*PI/512)*rot_angle)*60,  64 - sin((2*PI/512)*rot_angle)*60,  GREEN);
            drawXmarkstheSpot(foundobject,rot_angle,initial_radar_direction);
          }

          if (rot_angle == 2) {

            std::string radar_instructions;
            radar_instructions = "Press push button to pause the radar"; MyDrawString(radar_instructions,2,80,true,false);

            OledDisplay.DrawFilledRect(0, 0, 128, 64, BLACK);

            InitializeRadarScreen ();

          }

          rot_angle--;

        } else {

          // Pause in this position

        }

        if (rot_angle==1) { rot_dir=0;}
        if (rot_angle==257) { rot_dir=1;}

    }

    old_pb = new_pb;
    old_count = count;

    if ( last_c == 'e') {

    }

   } // Priority One Exit

  }
}

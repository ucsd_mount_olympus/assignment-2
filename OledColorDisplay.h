//-----------------------------------------------------------------------------
// OledColorDisplay.h
// Notes:
//      https://github.com/adafruit/Adafruit-SSD1351-library/blob/master/Adafruit_SSD1351.cpp
//      https://github.com/adafruit/Adafruit-SSD1351-library/blob/master/Adafruit_SSD1351.h
// Mount Olympus Notes
// --> Worked out a licensing deal with Old_School_Wes for the following:
// ----> Majority of OledColorDisplay.h
// ----> Lines 1-504 of OledColorDisplay.cpp
//-----------------------------------------------------------------------------


#include <iostream>

#ifndef OLED_COLOR_DISPLAY_H
#define OLED_COLOR_DISPLAY_H


// 565 bits each for 16 bit color
#define RGB(red, green, blue) (((red >> 3) << 11) | ((green >> 2) << 5) | (blue >> 3))
#define STRING_STACK_LIMIT  120


// SSD1351 SPI Commands
typedef enum tagRGBColor
{
    BLACK           = RGB(0x00, 0x00, 0x00),
    BROWN           = RGB(0xa5, 0x2a, 0x2a),
    RED             = RGB(0xff, 0x00, 0x00),
    ORANGE          = RGB(0xff, 0xa5, 0x00),
    LIGHTYELLOW     = RGB(0xff, 0xff, 0xe0),
    YELLOW          = RGB(0xff, 0xff, 0x00),
    LIGHTGREEN      = RGB(0x90, 0xee, 0x90),
    GREEN           = RGB(0x00, 0xff, 0x00),
    LIGHTBLUE       = RGB(0xad, 0xd8, 0xe6),
    BLUE            = RGB(0x00, 0x00, 0xff),
    NAVYBLUE        = RGB(0x00, 0x00, 0x80),
    VIOLET          = RGB(0xee, 0x82, 0xee),
    LIGHTGRAY       = RGB(0xd3, 0xd3, 0xd3),
    GRAY            = RGB(0x80, 0x80, 0x80),
    DARKGRAY        = RGB(0xa9, 0xa9, 0xa9),
    WHITE           = RGB(0xff, 0xff, 0xff)
} RGBColor;

// SSD1351 SPI Commands
typedef enum tagSpiCommand
{
    SSD1351_CMD_SETCOLUMN           = 0x15,
    SSD1351_CMD_SETROW              = 0x75,
    SSD1351_CMD_WRITERAM            = 0x5C,
    SSD1351_CMD_READRAM             = 0x5D,
    SSD1351_CMD_SETREMAP            = 0xA0,
    SSD1351_CMD_STARTLINE           = 0xA1,
    SSD1351_CMD_DISPLAYOFFSET       = 0xA2,
    SSD1351_CMD_DISPLAYALLOFF       = 0xA4,
    SSD1351_CMD_DISPLAYALLON        = 0xA5,
    SSD1351_CMD_NORMALDISPLAY       = 0xA6,
    SSD1351_CMD_INVERTDISPLAY       = 0xA7,
    SSD1351_CMD_FUNCTIONSELECT      = 0xAB,
    SSD1351_CMD_DISPLAYOFF          = 0xAE,
    SSD1351_CMD_DISPLAYON           = 0xAF,
    SSD1351_CMD_PRECHARGE           = 0xB1, // Set phase length
    SSD1351_CMD_DISPLAYENHANCE      = 0xB2,
    SSD1351_CMD_CLOCKDIV            = 0xB3,
    SSD1351_CMD_SETVSL              = 0xB4,
    SSD1351_CMD_SETGPIO             = 0xB5,
    SSD1351_CMD_PRECHARGE2          = 0xB6,
    SSD1351_CMD_SETGRAY             = 0xB8,
    SSD1351_CMD_USELUT              = 0xB9,
    SSD1351_CMD_PRECHARGELEVEL      = 0xBB,
    SSD1351_CMD_VCOMH               = 0xBE,
    SSD1351_CMD_CONTRASTABC         = 0xC1,
    SSD1351_CMD_CONTRASTMASTER      = 0xC7,
    SSD1351_CMD_MUXRATIO            = 0xCA,
    SSD1351_CMD_COMMANDLOCK         = 0xFD,
    SSD1351_CMD_HORIZSCROLL         = 0x96,
    SSD1351_CMD_STOPSCROLL          = 0x9E,
    SSD1351_CMD_STARTSCROLL         = 0x9F,
    SSD1351_CMD_0xF1                = 0xF1,
    SSD1351_CMD_0x32                = 0x32,
    SSD1351_CMD_0x05                = 0x05
} SpiCommand;


// OLED color display class
class OledColorDisplay
{
public:
    // Constructor/destructor
    OledColorDisplay(PinName mosiPort, PinName misoPort, PinName sclkPort, PinName csPort, PinName resetPort, PinName dcPort);

    // Properties
    inline uint8_t Width()
    {
        return SSD1351_WIDTH;
    }

    inline uint8_t Height()
    {
        return SSD1351_HEIGHT;
    }

    // Methods
    void DisplayClear();
    void DisplayFill(uint16_t color);
    void DisplayInvert(bool invert);
    void DrawPixel(uint16_t x, uint16_t y, uint16_t color);
    void DrawFastHLine(int16_t x, int16_t y, int16_t width, uint16_t color);
    void DrawLine(uint16_t x0, uint16_t y0, uint16_t x1, uint16_t y1, uint16_t color);

    //void goTo(int x, int y);

    void DrawFilledRect(uint16_t x, uint16_t y, uint16_t w, uint16_t h, uint16_t color);
    void DrawBitmap(uint16_t *pBuffer, uint16_t x, uint16_t y, uint16_t width, uint16_t height);
    void DrawTestPattern();

    // Drawing characters pixel by pixel
    void MyDrawChar(std::string &mydrawchar, int start_x, int start_y);

private:
    void Init();

protected:
    void WriteCommand(SpiCommand command);
    void WriteData(uint8_t data);
    void WriteDataWord(uint16_t data);
    void WriteCommandData(SpiCommand command, uint8_t data1);
    void WriteCommandData(SpiCommand command, uint8_t data1, uint8_t data2);
    void WriteCommandData(SpiCommand command, uint8_t data1, uint8_t data2, uint8_t data3);
    void Error(const char *errorStr);

    // Constants
    static const int SSD1351_WIDTH          = 128;
    static const int SSD1351_HEIGHT         = 128;  // Set to 96 FOR 1.27" display

    // Data
    SPI m_Spi;                      // m_MosiPort, m_MisoPort, m_SclkPort
    DigitalOut m_CsOut;             // m_CsPort
    DigitalOut m_ResetOut;          // m_ResetPort
    DigitalOut m_DcOut;             // m_DcPort

    PinName m_MosiPort;             // SPI MOSI port
    PinName m_MisoPort;             // SPI MISO port
    PinName m_SclkPort;             // SPI SCLK port
    PinName m_CsPort;               // SPI chip select port
    PinName m_ResetPort;            // SPI reset port
    PinName m_DcPort;               // SPI MOSI data/command port (0=control, 1=data)
};

#endif

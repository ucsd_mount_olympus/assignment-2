//-----------------------------------------------------------------------------
// OledColorDisplay.cpp
//-----------------------------------------------------------------------------

// Includes
#include "mbed.h"
#include "OledColorDisplay.h"

#include <iostream>

// Constructor
OledColorDisplay::OledColorDisplay(PinName mosiPort, PinName misoPort, PinName sclkPort, PinName csPort, PinName resetPort, PinName dcPort)
:
    m_Spi(mosiPort, misoPort, sclkPort), // mosi, miso, sclk
    m_CsOut(csPort),
    m_ResetOut(resetPort),
    m_DcOut(dcPort)
{
    m_MosiPort = mosiPort;
    m_MisoPort = misoPort;
    m_SclkPort = sclkPort;
    m_CsPort = csPort;
    m_ResetPort = resetPort;
    m_DcPort = dcPort;

    Init();
}


// Clear display with black
void OledColorDisplay::DisplayClear()
{
    DrawFilledRect(0, 0, Width(), Height(), BLACK);
}


// Fill display with a color
void OledColorDisplay::DisplayFill(uint16_t color)
{
    DrawFilledRect(0, 0, Width(), Height(), color);
}


// Invert display
void OledColorDisplay::DisplayInvert(bool invert)
{
    if (invert)
    {
        WriteCommand(SSD1351_CMD_INVERTDISPLAY);
    }
    else
    {
        WriteCommand(SSD1351_CMD_NORMALDISPLAY);
    }
}

// Draw a filled rectangle with no rotation.
void OledColorDisplay::DrawPixel(uint16_t x, uint16_t y, uint16_t color)
{
    // Bounds check
    if ((x >= Width()) || (y >= Height()))
    {
        Error("Coordinates out of bounds");
        return;
    }

    #if 0
    //x = WIDTH - x - 1;
    //y = HEIGHT - y - 1;
    int ww = 1;
    // Bounds check.
    if ((x >= SSD1351_WIDTH) || (y >= SSD1351_HEIGHT)) return;
    if ((x < 0) || (y < 0)) return;
    if (x+1 > SSD1351_WIDTH)
    {
    ww = SSD1351_WIDTH - x - 1;
    }

    // set location
    WriteCommand(SSD1351_CMD_SETCOLUMN);
    WriteData(x);
    WriteData(x+ww-1);
  #endif

    // Set location
    WriteCommand(SSD1351_CMD_SETCOLUMN);
    WriteData(x);
    WriteData(Width() - 1);

    WriteCommand(SSD1351_CMD_SETROW);
    WriteData(y);
    WriteData(Height() - 1);

    // Draw pixel
    WriteCommand(SSD1351_CMD_WRITERAM);
    WriteData(color >> 8);
    WriteData(color);
}


void OledColorDisplay::DrawFastHLine(int16_t x, int16_t y, int16_t width, uint16_t color)
{
    // Bounds check
    if ((x >= Width()) || (y >= Height()))
    {
        Error("XY Coordinates out of bounds");
        return;
    }

    // X bounds check
    if (x + width > Width())
    {
        width = Width() - x - 1;
    }

    if (width < 0)
    {
        return;
    }

    // set location
    WriteCommand(SSD1351_CMD_SETCOLUMN);
    WriteData(x);
    WriteData(x + width - 1);

    WriteCommand(SSD1351_CMD_SETROW);
    WriteData(y);
    WriteData(y + 10);

    // fill!
    WriteCommand(SSD1351_CMD_WRITERAM);

    for (uint16_t ii = 0; ii < width; ii++)
    {
        WriteData(color >> 8);
        WriteData(color);
    }
}


// Draw a line
void OledColorDisplay::DrawLine(uint16_t x0, uint16_t y0, uint16_t x1, uint16_t y1, uint16_t color)
{
    // Bounds check
    if ((x0 >= Width()) || (y0 >= Height()))
    {
        Error("XY Coordinates out of bounds");
        return;
    }

    // Bounds check
    if ((x1 >= Width()) || (y1 >= Height()))
    {
        Error("XY2 Coordinates out of bounds");
        return;
    }

    int steep = abs(y1 - y0) > abs(x1 - x0);
    if (steep)
    {
        int temp = x0;
        x0 = y0;
        y0 = temp;

        temp = x1;
        x1 = y1;
        y1 = temp;
    }

    if (x0 > x1)
    {
        int temp = x0;
        x0 = x1;
        x1 = temp;

        temp = y0;
        y0 = y1;
        y1 = temp;
    }

    int dx, dy;
    dx = x1 - x0;
    dy = abs(y1 - y0);

    int err = dx / 2;
    int ystep;

    if (y0 < y1)
    {
        ystep = 1;
    }
    else
    {
        ystep = -1;
    }

    for (; x0 <= x1; x0++)
    {
        if (steep)
        {
            DrawPixel(y0, x0, color);
        }
        else
        {
            DrawPixel(x0, y0, color);
        }

        err -= dy;
        if (err < 0)
        {
            y0 += ystep;
            err += dx;
        }
    }
}


// Draw a filled rectangle with no rotation.
void OledColorDisplay::DrawFilledRect(uint16_t x, uint16_t y, uint16_t width, uint16_t height, uint16_t color)
{
    // Bounds check
    if ((x >= Width()) || (y >= Height()))
    {
        Error("Coordinates out of bounds");
        return;
    }

    // X bounds check
    if (x + width > Width())
    {
        width = Width() - x - 1;
    }

    // Y bounds check
    if (y + height > Height())
    {
        height = Height() - y - 1;
    }

    // Set location
    WriteCommand(SSD1351_CMD_SETCOLUMN);
    WriteData(x);
    WriteData(x + width - 1);

    WriteCommand(SSD1351_CMD_SETROW);
    WriteData(y);
    WriteData(y + height - 1);

    // Write data to frame buffer
    WriteCommand(SSD1351_CMD_WRITERAM);

    int totalPixels = width * height;
    for (uint16_t loopCnt = 0; loopCnt < totalPixels; loopCnt++)
    {
        WriteData(color >> 8);
        WriteData(color);
    }
}

// Draw bitmap (an array of size width * height * 2 (color 565 format)
void OledColorDisplay::DrawBitmap(uint16_t *pBuffer, uint16_t x, uint16_t y, uint16_t width, uint16_t height)
{
    // Bounds check
    if ((x >= Width()) || (y >= Height()))
    {
        Error("Coordinates out of bounds");
        return;
    }

    // X bounds check
    if (x + width > Width())
    {
        width = Width() - x - 1;
    }

    // Y bounds check
    if (y + height > Height())
    {
        height = Height() - y - 1;
    }

    // Set location
    WriteCommand(SSD1351_CMD_SETCOLUMN);
    WriteData(x);
    WriteData(x + width - 1);

    WriteCommand(SSD1351_CMD_SETROW);
    WriteData(y);
    WriteData(y + height - 1);

    // Write data to frame buffer
    WriteCommand(SSD1351_CMD_WRITERAM);

    int totalBytes = width * height * 2;
    for (uint16_t loopCnt = 0; loopCnt < totalBytes; loopCnt++)
    {
        WriteData(*pBuffer++);
    }
}


// Draw test pattern
void OledColorDisplay::DrawTestPattern()
{
    int sliceWidth = Width() / 10;
    int sliceHeight = Height();
    int slicePos1 = sliceWidth;
    int slicePos2 = sliceWidth * 2;
    int slicePos3 = sliceWidth * 3;
    int slicePos4 = sliceWidth * 4;
    int slicePos5 = sliceWidth * 5;
    int slicePos6 = sliceWidth * 6;
    int slicePos7 = sliceWidth * 7;
    int slicePos8 = sliceWidth * 8;
    int slicePos9 = sliceWidth * 9;
    int slicePos10 = sliceWidth * 10;

    // Draw test pattern
    DrawFilledRect(0, 0, sliceWidth, sliceHeight, BLACK);
    DrawFilledRect(slicePos1, 0, sliceWidth, sliceHeight, BROWN);
    DrawFilledRect(slicePos2, 0, sliceWidth, sliceHeight, RED);
    DrawFilledRect(slicePos3, 0, sliceWidth, sliceHeight, ORANGE);
    DrawFilledRect(slicePos4, 0, sliceWidth, sliceHeight, YELLOW);
    DrawFilledRect(slicePos5, 0, sliceWidth, sliceHeight, GREEN);
    DrawFilledRect(slicePos6, 0, sliceWidth, sliceHeight, BLUE);
    DrawFilledRect(slicePos7, 0, sliceWidth, sliceHeight, VIOLET);
    DrawFilledRect(slicePos8, 0, sliceWidth, sliceHeight, GRAY);
    DrawFilledRect(slicePos9, 0, sliceWidth, sliceHeight, WHITE);
    DrawFilledRect(slicePos10, 0, Width() - sliceWidth, sliceHeight, NAVYBLUE);

    //DrawPixel(20, 20, RGB(0xff, 0xff, 0xff));
    //DrawPixel(30, 30, RGB(0xff, 0xff, 0xff));
    //DrawPixel(40, 40, RGB(0xff, 0xff, 0xff));
    //DrawPixel(50, 50, RGB(0xff, 0xff, 0xff));
    //DrawPixel(60, 60, RGB(0xff, 0xff, 0xff));
}


// Initialization Sequence
void OledColorDisplay::Init()
{
    // Init SPI interface
    m_CsOut = 1;
    m_DcOut = 0;
    wait_ms(200);
    m_ResetOut = 0;
    wait_ms(200);
    m_ResetOut = 1;
    wait_ms(200);

    // Setup the spi for 8 bit data, high steady state clock,
    // second edge capture, with a 1MHz clock rate
    m_Spi.frequency(8000000); // 8000000
    m_Spi.format(8,3);

    // Init display
    WriteCommand(SSD1351_CMD_COMMANDLOCK);      // Set command lock
    WriteData(0x12);

    WriteCommand(SSD1351_CMD_COMMANDLOCK);      // Set command lock
    WriteData(0xB1);

    WriteCommand(SSD1351_CMD_DISPLAYOFF);       // 0xAE

    WriteCommand(SSD1351_CMD_CLOCKDIV);         // 0xB3
    WriteCommand(SSD1351_CMD_0xF1);                         // 7:4 = Oscillator Frequency, 3:0 = CLK Div Ratio (A[3:0]+1 = 1..16)
    ///  IS THIS A BUG ***   WriteCommand(0xF1);                         // 7:4 = Oscillator Frequency, 3:0 = CLK Div Ratio (A[3:0]+1 = 1..16)

    WriteCommand(SSD1351_CMD_MUXRATIO);
    WriteData(127);

    WriteCommand(SSD1351_CMD_SETREMAP); // sets to auto scroll display ram
    WriteData(0x74);

    WriteCommand(SSD1351_CMD_SETCOLUMN);
    WriteData(0x00);
    WriteData(0x7F);
    WriteCommand(SSD1351_CMD_SETROW);
    WriteData(0x00);
    WriteData(0x7F);

    WriteCommand(SSD1351_CMD_STARTLINE);        // 0xA1

    if (Height() == 96)
    {
        WriteData(96);
    }
    else
    {
        WriteData(0);
    }

    WriteCommand(SSD1351_CMD_DISPLAYOFFSET);    // 0xA2
    WriteData(0x0);

    WriteCommand(SSD1351_CMD_SETGPIO);
    WriteData(0x00);

    WriteCommand(SSD1351_CMD_FUNCTIONSELECT);
    WriteData(0x01);                            // internal (diode drop)
    // WriteData(0x01);                         // external bias

    // WriteCommand(SSSD1351_CMD_SETPHASELENGTH);
    // WriteData(0x32);

    WriteCommand(SSD1351_CMD_PRECHARGE);        // 0xB1
    WriteCommand(SSD1351_CMD_0x32);
    /// IS THIS A BUG *** WriteCommand(0x32);

    WriteCommand(SSD1351_CMD_VCOMH);            // 0xBE
    WriteCommand(SSD1351_CMD_0x05);
    /// IS THIS A BUG *** WriteCommand(0x05);

    WriteCommand(SSD1351_CMD_NORMALDISPLAY);    // 0xA6

    WriteCommand(SSD1351_CMD_CONTRASTABC);
    WriteData(0xC8);
    WriteData(0x80);
    WriteData(0xC8);

    WriteCommand(SSD1351_CMD_CONTRASTMASTER);
    WriteData(0x0F);

    WriteCommand(SSD1351_CMD_SETVSL);
    WriteData(0xA0);
    WriteData(0xB5);
    WriteData(0x55);

    WriteCommand(SSD1351_CMD_PRECHARGE2);
    WriteData(0x01);

    WriteCommand(SSD1351_CMD_DISPLAYON); // turn on oled panel
}


// Write command to SPI
void OledColorDisplay::WriteCommand(SpiCommand command)
{
    m_CsOut = 1;
    m_DcOut = 0;
    m_CsOut = 0;
    m_Spi.write((uint8_t) command);
    //wait_ms(1);
    m_CsOut = 1;
}

// Write data (byte) to SPI
void OledColorDisplay::WriteData(uint8_t data)
{
    m_CsOut = 1;
    m_DcOut = 1;
    m_CsOut = 0;
    m_Spi.write(data);
    //wait_ms(1);
    m_CsOut = 1;
}


// Write data (word) to SPI
void OledColorDisplay::WriteDataWord(uint16_t data)
{
    m_CsOut = 1;
    m_DcOut = 1;
    m_CsOut = 0;
    m_Spi.write(data >> 8);
    m_Spi.write(data & 0xff);
    //wait_ms(1);
    m_CsOut = 1;
}


// Write data (byte) to command port for SPI
void OledColorDisplay::WriteCommandData(SpiCommand command, uint8_t data1)
{
    WriteCommand(command);
    WriteData(data1);
}


// Write data to command port for SPI
void OledColorDisplay::WriteCommandData(SpiCommand command, uint8_t data1, uint8_t data2)
{
    WriteCommand(command);
    WriteData(data1);
    WriteData(data2);
}


// Write data to command port for SPI
void OledColorDisplay::WriteCommandData(SpiCommand command, uint8_t data1, uint8_t data2, uint8_t data3)
{
    WriteCommand(command);
    WriteData(data1);
    WriteData(data2);
    WriteData(data3);
}


// Error output
void OledColorDisplay::Error(const char *errorStr)
{
    printf(errorStr);
}

void OledColorDisplay::MyDrawChar(std::string &mydrawchar, int start_x, int start_y) {

if ( mydrawchar == "A" ) {
  OledColorDisplay::DrawPixel(start_x + 0,start_y + 0, BLACK);
  OledColorDisplay::DrawPixel(start_x + 1,start_y + 0, BLACK);
  OledColorDisplay::DrawPixel(start_x + 2,start_y + 0, BLACK);
  OledColorDisplay::DrawPixel(start_x + 3,start_y + 0, BLACK);
  OledColorDisplay::DrawPixel(start_x + 4,start_y + 0, BLACK);
  OledColorDisplay::DrawPixel(start_x + 0,start_y + 1, BLACK);
  OledColorDisplay::DrawPixel(start_x + 1,start_y + 1, GREEN);
  OledColorDisplay::DrawPixel(start_x + 2,start_y + 1, GREEN);
  OledColorDisplay::DrawPixel(start_x + 3,start_y + 1, GREEN);
  OledColorDisplay::DrawPixel(start_x + 4,start_y + 1, BLACK);
  OledColorDisplay::DrawPixel(start_x + 0,start_y + 2, GREEN);
  OledColorDisplay::DrawPixel(start_x + 1,start_y + 2, BLACK);
  OledColorDisplay::DrawPixel(start_x + 2,start_y + 2, BLACK);
  OledColorDisplay::DrawPixel(start_x + 3,start_y + 2, BLACK);
  OledColorDisplay::DrawPixel(start_x + 4,start_y + 2, GREEN);
  OledColorDisplay::DrawPixel(start_x + 0,start_y + 3, GREEN);
  OledColorDisplay::DrawPixel(start_x + 1,start_y + 3, BLACK);
  OledColorDisplay::DrawPixel(start_x + 2,start_y + 3, BLACK);
  OledColorDisplay::DrawPixel(start_x + 3,start_y + 3, BLACK);
  OledColorDisplay::DrawPixel(start_x + 4,start_y + 3, GREEN);
  OledColorDisplay::DrawPixel(start_x + 0,start_y + 4, GREEN);
  OledColorDisplay::DrawPixel(start_x + 1,start_y + 4, BLACK);
  OledColorDisplay::DrawPixel(start_x + 2,start_y + 4, BLACK);
  OledColorDisplay::DrawPixel(start_x + 3,start_y + 4, BLACK);
  OledColorDisplay::DrawPixel(start_x + 4,start_y + 4, GREEN);
  OledColorDisplay::DrawPixel(start_x + 0,start_y + 5, GREEN);
  OledColorDisplay::DrawPixel(start_x + 1,start_y + 5, GREEN);
  OledColorDisplay::DrawPixel(start_x + 2,start_y + 5, GREEN);
  OledColorDisplay::DrawPixel(start_x + 3,start_y + 5, GREEN);
  OledColorDisplay::DrawPixel(start_x + 4,start_y + 5, GREEN);
  OledColorDisplay::DrawPixel(start_x + 0,start_y + 6, GREEN);
  OledColorDisplay::DrawPixel(start_x + 1,start_y + 6, BLACK);
  OledColorDisplay::DrawPixel(start_x + 2,start_y + 6, BLACK);
  OledColorDisplay::DrawPixel(start_x + 3,start_y + 6, BLACK);
  OledColorDisplay::DrawPixel(start_x + 4,start_y + 6, GREEN);
  OledColorDisplay::DrawPixel(start_x + 0,start_y + 7, GREEN);
  OledColorDisplay::DrawPixel(start_x + 1,start_y + 7, BLACK);
  OledColorDisplay::DrawPixel(start_x + 2,start_y + 7, BLACK);
  OledColorDisplay::DrawPixel(start_x + 3,start_y + 7, BLACK);
  OledColorDisplay::DrawPixel(start_x + 4,start_y + 7, GREEN);
} else if ( mydrawchar == "B" ) {
  OledColorDisplay::DrawPixel(start_x + 0,start_y + 0, BLACK);
  OledColorDisplay::DrawPixel(start_x + 1,start_y + 0, BLACK);
  OledColorDisplay::DrawPixel(start_x + 2,start_y + 0, BLACK);
  OledColorDisplay::DrawPixel(start_x + 3,start_y + 0, BLACK);
  OledColorDisplay::DrawPixel(start_x + 4,start_y + 0, BLACK);
  OledColorDisplay::DrawPixel(start_x + 0,start_y + 1, GREEN);
  OledColorDisplay::DrawPixel(start_x + 1,start_y + 1, GREEN);
  OledColorDisplay::DrawPixel(start_x + 2,start_y + 1, GREEN);
  OledColorDisplay::DrawPixel(start_x + 3,start_y + 1, GREEN);
  OledColorDisplay::DrawPixel(start_x + 4,start_y + 1, BLACK);
  OledColorDisplay::DrawPixel(start_x + 0,start_y + 2, GREEN);
  OledColorDisplay::DrawPixel(start_x + 1,start_y + 2, BLACK);
  OledColorDisplay::DrawPixel(start_x + 2,start_y + 2, BLACK);
  OledColorDisplay::DrawPixel(start_x + 3,start_y + 2, BLACK);
  OledColorDisplay::DrawPixel(start_x + 4,start_y + 2, GREEN);
  OledColorDisplay::DrawPixel(start_x + 0,start_y + 3, GREEN);
  OledColorDisplay::DrawPixel(start_x + 1,start_y + 3, BLACK);
  OledColorDisplay::DrawPixel(start_x + 2,start_y + 3, BLACK);
  OledColorDisplay::DrawPixel(start_x + 3,start_y + 3, BLACK);
  OledColorDisplay::DrawPixel(start_x + 4,start_y + 3, GREEN);
  OledColorDisplay::DrawPixel(start_x + 0,start_y + 4, GREEN);
  OledColorDisplay::DrawPixel(start_x + 1,start_y + 4, GREEN);
  OledColorDisplay::DrawPixel(start_x + 2,start_y + 4, GREEN);
  OledColorDisplay::DrawPixel(start_x + 3,start_y + 4, GREEN);
  OledColorDisplay::DrawPixel(start_x + 4,start_y + 4, BLACK);
  OledColorDisplay::DrawPixel(start_x + 0,start_y + 5, GREEN);
  OledColorDisplay::DrawPixel(start_x + 1,start_y + 5, BLACK);
  OledColorDisplay::DrawPixel(start_x + 2,start_y + 5, BLACK);
  OledColorDisplay::DrawPixel(start_x + 3,start_y + 5, BLACK);
  OledColorDisplay::DrawPixel(start_x + 4,start_y + 5, GREEN);
  OledColorDisplay::DrawPixel(start_x + 0,start_y + 6, GREEN);
  OledColorDisplay::DrawPixel(start_x + 1,start_y + 6, BLACK);
  OledColorDisplay::DrawPixel(start_x + 2,start_y + 6, BLACK);
  OledColorDisplay::DrawPixel(start_x + 3,start_y + 6, BLACK);
  OledColorDisplay::DrawPixel(start_x + 4,start_y + 6, GREEN);
  OledColorDisplay::DrawPixel(start_x + 0,start_y + 7, GREEN);
  OledColorDisplay::DrawPixel(start_x + 1,start_y + 7, GREEN);
  OledColorDisplay::DrawPixel(start_x + 2,start_y + 7, GREEN);
  OledColorDisplay::DrawPixel(start_x + 3,start_y + 7, GREEN);
  OledColorDisplay::DrawPixel(start_x + 4,start_y + 7, BLACK);
} else if ( mydrawchar == "C" ) {
  OledColorDisplay::DrawPixel(start_x + 0,start_y + 0, BLACK);
  OledColorDisplay::DrawPixel(start_x + 1,start_y + 0, BLACK);
  OledColorDisplay::DrawPixel(start_x + 2,start_y + 0, BLACK);
  OledColorDisplay::DrawPixel(start_x + 3,start_y + 0, BLACK);
  OledColorDisplay::DrawPixel(start_x + 4,start_y + 0, BLACK);
  OledColorDisplay::DrawPixel(start_x + 0,start_y + 1, BLACK);
  OledColorDisplay::DrawPixel(start_x + 1,start_y + 1, GREEN);
  OledColorDisplay::DrawPixel(start_x + 2,start_y + 1, GREEN);
  OledColorDisplay::DrawPixel(start_x + 3,start_y + 1, GREEN);
  OledColorDisplay::DrawPixel(start_x + 4,start_y + 1, BLACK);
  OledColorDisplay::DrawPixel(start_x + 0,start_y + 2, GREEN);
  OledColorDisplay::DrawPixel(start_x + 1,start_y + 2, BLACK);
  OledColorDisplay::DrawPixel(start_x + 2,start_y + 2, BLACK);
  OledColorDisplay::DrawPixel(start_x + 3,start_y + 2, BLACK);
  OledColorDisplay::DrawPixel(start_x + 4,start_y + 2, GREEN);
  OledColorDisplay::DrawPixel(start_x + 0,start_y + 3, GREEN);
  OledColorDisplay::DrawPixel(start_x + 1,start_y + 3, BLACK);
  OledColorDisplay::DrawPixel(start_x + 2,start_y + 3, BLACK);
  OledColorDisplay::DrawPixel(start_x + 3,start_y + 3, BLACK);
  OledColorDisplay::DrawPixel(start_x + 4,start_y + 3, BLACK);
  OledColorDisplay::DrawPixel(start_x + 0,start_y + 4, GREEN);
  OledColorDisplay::DrawPixel(start_x + 1,start_y + 4, BLACK);
  OledColorDisplay::DrawPixel(start_x + 2,start_y + 4, BLACK);
  OledColorDisplay::DrawPixel(start_x + 3,start_y + 4, BLACK);
  OledColorDisplay::DrawPixel(start_x + 4,start_y + 4, BLACK);
  OledColorDisplay::DrawPixel(start_x + 0,start_y + 5, GREEN);
  OledColorDisplay::DrawPixel(start_x + 1,start_y + 5, BLACK);
  OledColorDisplay::DrawPixel(start_x + 2,start_y + 5, BLACK);
  OledColorDisplay::DrawPixel(start_x + 3,start_y + 5, BLACK);
  OledColorDisplay::DrawPixel(start_x + 4,start_y + 5, BLACK);
  OledColorDisplay::DrawPixel(start_x + 0,start_y + 6, GREEN);
  OledColorDisplay::DrawPixel(start_x + 1,start_y + 6, BLACK);
  OledColorDisplay::DrawPixel(start_x + 2,start_y + 6, BLACK);
  OledColorDisplay::DrawPixel(start_x + 3,start_y + 6, BLACK);
  OledColorDisplay::DrawPixel(start_x + 4,start_y + 6, GREEN);
  OledColorDisplay::DrawPixel(start_x + 0,start_y + 7, BLACK);
  OledColorDisplay::DrawPixel(start_x + 1,start_y + 7, GREEN);
  OledColorDisplay::DrawPixel(start_x + 2,start_y + 7, GREEN);
  OledColorDisplay::DrawPixel(start_x + 3,start_y + 7, GREEN);
  OledColorDisplay::DrawPixel(start_x + 4,start_y + 7, BLACK);
} else if ( mydrawchar == "D" ) {
  OledColorDisplay::DrawPixel(start_x + 0,start_y + 0, BLACK);
  OledColorDisplay::DrawPixel(start_x + 1,start_y + 0, BLACK);
  OledColorDisplay::DrawPixel(start_x + 2,start_y + 0, BLACK);
  OledColorDisplay::DrawPixel(start_x + 3,start_y + 0, BLACK);
  OledColorDisplay::DrawPixel(start_x + 4,start_y + 0, BLACK);
  OledColorDisplay::DrawPixel(start_x + 0,start_y + 1, GREEN);
  OledColorDisplay::DrawPixel(start_x + 1,start_y + 1, GREEN);
  OledColorDisplay::DrawPixel(start_x + 2,start_y + 1, GREEN);
  OledColorDisplay::DrawPixel(start_x + 3,start_y + 1, BLACK);
  OledColorDisplay::DrawPixel(start_x + 4,start_y + 1, BLACK);
  OledColorDisplay::DrawPixel(start_x + 0,start_y + 2, GREEN);
  OledColorDisplay::DrawPixel(start_x + 1,start_y + 2, BLACK);
  OledColorDisplay::DrawPixel(start_x + 2,start_y + 2, BLACK);
  OledColorDisplay::DrawPixel(start_x + 3,start_y + 2, GREEN);
  OledColorDisplay::DrawPixel(start_x + 4,start_y + 2, BLACK);
  OledColorDisplay::DrawPixel(start_x + 0,start_y + 3, GREEN);
  OledColorDisplay::DrawPixel(start_x + 1,start_y + 3, BLACK);
  OledColorDisplay::DrawPixel(start_x + 2,start_y + 3, BLACK);
  OledColorDisplay::DrawPixel(start_x + 3,start_y + 3, BLACK);
  OledColorDisplay::DrawPixel(start_x + 4,start_y + 3, GREEN);
  OledColorDisplay::DrawPixel(start_x + 0,start_y + 4, GREEN);
  OledColorDisplay::DrawPixel(start_x + 1,start_y + 4, BLACK);
  OledColorDisplay::DrawPixel(start_x + 2,start_y + 4, BLACK);
  OledColorDisplay::DrawPixel(start_x + 3,start_y + 4, BLACK);
  OledColorDisplay::DrawPixel(start_x + 4,start_y + 4, GREEN);
  OledColorDisplay::DrawPixel(start_x + 0,start_y + 5, GREEN);
  OledColorDisplay::DrawPixel(start_x + 1,start_y + 5, BLACK);
  OledColorDisplay::DrawPixel(start_x + 2,start_y + 5, BLACK);
  OledColorDisplay::DrawPixel(start_x + 3,start_y + 5, BLACK);
  OledColorDisplay::DrawPixel(start_x + 4,start_y + 5, GREEN);
  OledColorDisplay::DrawPixel(start_x + 0,start_y + 6, GREEN);
  OledColorDisplay::DrawPixel(start_x + 1,start_y + 6, BLACK);
  OledColorDisplay::DrawPixel(start_x + 2,start_y + 6, BLACK);
  OledColorDisplay::DrawPixel(start_x + 3,start_y + 6, GREEN);
  OledColorDisplay::DrawPixel(start_x + 4,start_y + 6, BLACK);
  OledColorDisplay::DrawPixel(start_x + 0,start_y + 7, GREEN);
  OledColorDisplay::DrawPixel(start_x + 1,start_y + 7, GREEN);
  OledColorDisplay::DrawPixel(start_x + 2,start_y + 7, GREEN);
  OledColorDisplay::DrawPixel(start_x + 3,start_y + 7, BLACK);
  OledColorDisplay::DrawPixel(start_x + 4,start_y + 7, BLACK);
} else if ( mydrawchar == "E" ) {
  OledColorDisplay::DrawPixel(start_x + 0,start_y + 0, BLACK);
  OledColorDisplay::DrawPixel(start_x + 1,start_y + 0, BLACK);
  OledColorDisplay::DrawPixel(start_x + 2,start_y + 0, BLACK);
  OledColorDisplay::DrawPixel(start_x + 3,start_y + 0, BLACK);
  OledColorDisplay::DrawPixel(start_x + 4,start_y + 0, BLACK);
  OledColorDisplay::DrawPixel(start_x + 0,start_y + 1, GREEN);
  OledColorDisplay::DrawPixel(start_x + 1,start_y + 1, GREEN);
  OledColorDisplay::DrawPixel(start_x + 2,start_y + 1, GREEN);
  OledColorDisplay::DrawPixel(start_x + 3,start_y + 1, GREEN);
  OledColorDisplay::DrawPixel(start_x + 4,start_y + 1, GREEN);
  OledColorDisplay::DrawPixel(start_x + 0,start_y + 2, GREEN);
  OledColorDisplay::DrawPixel(start_x + 1,start_y + 2, BLACK);
  OledColorDisplay::DrawPixel(start_x + 2,start_y + 2, BLACK);
  OledColorDisplay::DrawPixel(start_x + 3,start_y + 2, BLACK);
  OledColorDisplay::DrawPixel(start_x + 4,start_y + 2, BLACK);
  OledColorDisplay::DrawPixel(start_x + 0,start_y + 3, GREEN);
  OledColorDisplay::DrawPixel(start_x + 1,start_y + 3, BLACK);
  OledColorDisplay::DrawPixel(start_x + 2,start_y + 3, BLACK);
  OledColorDisplay::DrawPixel(start_x + 3,start_y + 3, BLACK);
  OledColorDisplay::DrawPixel(start_x + 4,start_y + 3, BLACK);
  OledColorDisplay::DrawPixel(start_x + 0,start_y + 4, GREEN);
  OledColorDisplay::DrawPixel(start_x + 1,start_y + 4, GREEN);
  OledColorDisplay::DrawPixel(start_x + 2,start_y + 4, GREEN);
  OledColorDisplay::DrawPixel(start_x + 3,start_y + 4, GREEN);
  OledColorDisplay::DrawPixel(start_x + 4,start_y + 4, BLACK);
  OledColorDisplay::DrawPixel(start_x + 0,start_y + 5, GREEN);
  OledColorDisplay::DrawPixel(start_x + 1,start_y + 5, BLACK);
  OledColorDisplay::DrawPixel(start_x + 2,start_y + 5, BLACK);
  OledColorDisplay::DrawPixel(start_x + 3,start_y + 5, BLACK);
  OledColorDisplay::DrawPixel(start_x + 4,start_y + 5, BLACK);
  OledColorDisplay::DrawPixel(start_x + 0,start_y + 6, GREEN);
  OledColorDisplay::DrawPixel(start_x + 1,start_y + 6, BLACK);
  OledColorDisplay::DrawPixel(start_x + 2,start_y + 6, BLACK);
  OledColorDisplay::DrawPixel(start_x + 3,start_y + 6, BLACK);
  OledColorDisplay::DrawPixel(start_x + 4,start_y + 6, BLACK);
  OledColorDisplay::DrawPixel(start_x + 0,start_y + 7, GREEN);
  OledColorDisplay::DrawPixel(start_x + 1,start_y + 7, GREEN);
  OledColorDisplay::DrawPixel(start_x + 2,start_y + 7, GREEN);
  OledColorDisplay::DrawPixel(start_x + 3,start_y + 7, GREEN);
  OledColorDisplay::DrawPixel(start_x + 4,start_y + 7, GREEN);
} else if ( mydrawchar == "F" ) {
  OledColorDisplay::DrawPixel(start_x + 0,start_y + 0, BLACK);
  OledColorDisplay::DrawPixel(start_x + 1,start_y + 0, BLACK);
  OledColorDisplay::DrawPixel(start_x + 2,start_y + 0, BLACK);
  OledColorDisplay::DrawPixel(start_x + 3,start_y + 0, BLACK);
  OledColorDisplay::DrawPixel(start_x + 4,start_y + 0, BLACK);
  OledColorDisplay::DrawPixel(start_x + 0,start_y + 1, GREEN);
  OledColorDisplay::DrawPixel(start_x + 1,start_y + 1, GREEN);
  OledColorDisplay::DrawPixel(start_x + 2,start_y + 1, GREEN);
  OledColorDisplay::DrawPixel(start_x + 3,start_y + 1, GREEN);
  OledColorDisplay::DrawPixel(start_x + 4,start_y + 1, GREEN);
  OledColorDisplay::DrawPixel(start_x + 0,start_y + 2, GREEN);
  OledColorDisplay::DrawPixel(start_x + 1,start_y + 2, BLACK);
  OledColorDisplay::DrawPixel(start_x + 2,start_y + 2, BLACK);
  OledColorDisplay::DrawPixel(start_x + 3,start_y + 2, BLACK);
  OledColorDisplay::DrawPixel(start_x + 4,start_y + 2, BLACK);
  OledColorDisplay::DrawPixel(start_x + 0,start_y + 3, GREEN);
  OledColorDisplay::DrawPixel(start_x + 1,start_y + 3, BLACK);
  OledColorDisplay::DrawPixel(start_x + 2,start_y + 3, BLACK);
  OledColorDisplay::DrawPixel(start_x + 3,start_y + 3, BLACK);
  OledColorDisplay::DrawPixel(start_x + 4,start_y + 3, BLACK);
  OledColorDisplay::DrawPixel(start_x + 0,start_y + 4, GREEN);
  OledColorDisplay::DrawPixel(start_x + 1,start_y + 4, GREEN);
  OledColorDisplay::DrawPixel(start_x + 2,start_y + 4, GREEN);
  OledColorDisplay::DrawPixel(start_x + 3,start_y + 4, BLACK);
  OledColorDisplay::DrawPixel(start_x + 4,start_y + 4, BLACK);
  OledColorDisplay::DrawPixel(start_x + 0,start_y + 5, GREEN);
  OledColorDisplay::DrawPixel(start_x + 1,start_y + 5, BLACK);
  OledColorDisplay::DrawPixel(start_x + 2,start_y + 5, BLACK);
  OledColorDisplay::DrawPixel(start_x + 3,start_y + 5, BLACK);
  OledColorDisplay::DrawPixel(start_x + 4,start_y + 5, BLACK);
  OledColorDisplay::DrawPixel(start_x + 0,start_y + 6, GREEN);
  OledColorDisplay::DrawPixel(start_x + 1,start_y + 6, BLACK);
  OledColorDisplay::DrawPixel(start_x + 2,start_y + 6, BLACK);
  OledColorDisplay::DrawPixel(start_x + 3,start_y + 6, BLACK);
  OledColorDisplay::DrawPixel(start_x + 4,start_y + 6, BLACK);
  OledColorDisplay::DrawPixel(start_x + 0,start_y + 7, GREEN);
  OledColorDisplay::DrawPixel(start_x + 1,start_y + 7, BLACK);
  OledColorDisplay::DrawPixel(start_x + 2,start_y + 7, BLACK);
  OledColorDisplay::DrawPixel(start_x + 3,start_y + 7, BLACK);
  OledColorDisplay::DrawPixel(start_x + 4,start_y + 7, BLACK);
} else if ( mydrawchar == "G" ) {
  OledColorDisplay::DrawPixel(start_x + 0,start_y + 0, BLACK);
  OledColorDisplay::DrawPixel(start_x + 1,start_y + 0, BLACK);
  OledColorDisplay::DrawPixel(start_x + 2,start_y + 0, BLACK);
  OledColorDisplay::DrawPixel(start_x + 3,start_y + 0, BLACK);
  OledColorDisplay::DrawPixel(start_x + 4,start_y + 0, BLACK);
  OledColorDisplay::DrawPixel(start_x + 0,start_y + 1, BLACK);
  OledColorDisplay::DrawPixel(start_x + 1,start_y + 1, GREEN);
  OledColorDisplay::DrawPixel(start_x + 2,start_y + 1, GREEN);
  OledColorDisplay::DrawPixel(start_x + 3,start_y + 1, GREEN);
  OledColorDisplay::DrawPixel(start_x + 4,start_y + 1, BLACK);
  OledColorDisplay::DrawPixel(start_x + 0,start_y + 2, GREEN);
  OledColorDisplay::DrawPixel(start_x + 1,start_y + 2, BLACK);
  OledColorDisplay::DrawPixel(start_x + 2,start_y + 2, BLACK);
  OledColorDisplay::DrawPixel(start_x + 3,start_y + 2, BLACK);
  OledColorDisplay::DrawPixel(start_x + 4,start_y + 2, GREEN);
  OledColorDisplay::DrawPixel(start_x + 0,start_y + 3, GREEN);
  OledColorDisplay::DrawPixel(start_x + 1,start_y + 3, BLACK);
  OledColorDisplay::DrawPixel(start_x + 2,start_y + 3, BLACK);
  OledColorDisplay::DrawPixel(start_x + 3,start_y + 3, BLACK);
  OledColorDisplay::DrawPixel(start_x + 4,start_y + 3, BLACK);
  OledColorDisplay::DrawPixel(start_x + 0,start_y + 4, GREEN);
  OledColorDisplay::DrawPixel(start_x + 1,start_y + 4, BLACK);
  OledColorDisplay::DrawPixel(start_x + 2,start_y + 4, GREEN);
  OledColorDisplay::DrawPixel(start_x + 3,start_y + 4, GREEN);
  OledColorDisplay::DrawPixel(start_x + 4,start_y + 4, GREEN);
  OledColorDisplay::DrawPixel(start_x + 0,start_y + 5, GREEN);
  OledColorDisplay::DrawPixel(start_x + 1,start_y + 5, BLACK);
  OledColorDisplay::DrawPixel(start_x + 2,start_y + 5, BLACK);
  OledColorDisplay::DrawPixel(start_x + 3,start_y + 5, BLACK);
  OledColorDisplay::DrawPixel(start_x + 4,start_y + 5, GREEN);
  OledColorDisplay::DrawPixel(start_x + 0,start_y + 6, GREEN);
  OledColorDisplay::DrawPixel(start_x + 1,start_y + 6, BLACK);
  OledColorDisplay::DrawPixel(start_x + 2,start_y + 6, BLACK);
  OledColorDisplay::DrawPixel(start_x + 3,start_y + 6, BLACK);
  OledColorDisplay::DrawPixel(start_x + 4,start_y + 6, GREEN);
  OledColorDisplay::DrawPixel(start_x + 0,start_y + 7, BLACK);
  OledColorDisplay::DrawPixel(start_x + 1,start_y + 7, GREEN);
  OledColorDisplay::DrawPixel(start_x + 2,start_y + 7, GREEN);
  OledColorDisplay::DrawPixel(start_x + 3,start_y + 7, GREEN);
  OledColorDisplay::DrawPixel(start_x + 4,start_y + 7, GREEN);
} else if ( mydrawchar == "H" ) {
  OledColorDisplay::DrawPixel(start_x + 0,start_y + 0, BLACK);
  OledColorDisplay::DrawPixel(start_x + 1,start_y + 0, BLACK);
  OledColorDisplay::DrawPixel(start_x + 2,start_y + 0, BLACK);
  OledColorDisplay::DrawPixel(start_x + 3,start_y + 0, BLACK);
  OledColorDisplay::DrawPixel(start_x + 4,start_y + 0, BLACK);
  OledColorDisplay::DrawPixel(start_x + 0,start_y + 1, GREEN);
  OledColorDisplay::DrawPixel(start_x + 1,start_y + 1, BLACK);
  OledColorDisplay::DrawPixel(start_x + 2,start_y + 1, BLACK);
  OledColorDisplay::DrawPixel(start_x + 3,start_y + 1, BLACK);
  OledColorDisplay::DrawPixel(start_x + 4,start_y + 1, GREEN);
  OledColorDisplay::DrawPixel(start_x + 0,start_y + 2, GREEN);
  OledColorDisplay::DrawPixel(start_x + 1,start_y + 2, BLACK);
  OledColorDisplay::DrawPixel(start_x + 2,start_y + 2, BLACK);
  OledColorDisplay::DrawPixel(start_x + 3,start_y + 2, BLACK);
  OledColorDisplay::DrawPixel(start_x + 4,start_y + 2, GREEN);
  OledColorDisplay::DrawPixel(start_x + 0,start_y + 3, GREEN);
  OledColorDisplay::DrawPixel(start_x + 1,start_y + 3, BLACK);
  OledColorDisplay::DrawPixel(start_x + 2,start_y + 3, BLACK);
  OledColorDisplay::DrawPixel(start_x + 3,start_y + 3, BLACK);
  OledColorDisplay::DrawPixel(start_x + 4,start_y + 3, GREEN);
  OledColorDisplay::DrawPixel(start_x + 0,start_y + 4, GREEN);
  OledColorDisplay::DrawPixel(start_x + 1,start_y + 4, GREEN);
  OledColorDisplay::DrawPixel(start_x + 2,start_y + 4, GREEN);
  OledColorDisplay::DrawPixel(start_x + 3,start_y + 4, GREEN);
  OledColorDisplay::DrawPixel(start_x + 4,start_y + 4, GREEN);
  OledColorDisplay::DrawPixel(start_x + 0,start_y + 5, GREEN);
  OledColorDisplay::DrawPixel(start_x + 1,start_y + 5, BLACK);
  OledColorDisplay::DrawPixel(start_x + 2,start_y + 5, BLACK);
  OledColorDisplay::DrawPixel(start_x + 3,start_y + 5, BLACK);
  OledColorDisplay::DrawPixel(start_x + 4,start_y + 5, GREEN);
  OledColorDisplay::DrawPixel(start_x + 0,start_y + 6, GREEN);
  OledColorDisplay::DrawPixel(start_x + 1,start_y + 6, BLACK);
  OledColorDisplay::DrawPixel(start_x + 2,start_y + 6, BLACK);
  OledColorDisplay::DrawPixel(start_x + 3,start_y + 6, BLACK);
  OledColorDisplay::DrawPixel(start_x + 4,start_y + 6, GREEN);
  OledColorDisplay::DrawPixel(start_x + 0,start_y + 7, GREEN);
  OledColorDisplay::DrawPixel(start_x + 1,start_y + 7, BLACK);
  OledColorDisplay::DrawPixel(start_x + 2,start_y + 7, BLACK);
  OledColorDisplay::DrawPixel(start_x + 3,start_y + 7, BLACK);
  OledColorDisplay::DrawPixel(start_x + 4,start_y + 7, GREEN);
} else if ( mydrawchar == "I" ) {
  OledColorDisplay::DrawPixel(start_x + 0,start_y + 0, BLACK);
  OledColorDisplay::DrawPixel(start_x + 1,start_y + 0, BLACK);
  OledColorDisplay::DrawPixel(start_x + 2,start_y + 0, BLACK);
  OledColorDisplay::DrawPixel(start_x + 3,start_y + 0, BLACK);
  OledColorDisplay::DrawPixel(start_x + 4,start_y + 0, BLACK);
  OledColorDisplay::DrawPixel(start_x + 0,start_y + 1, GREEN);
  OledColorDisplay::DrawPixel(start_x + 1,start_y + 1, GREEN);
  OledColorDisplay::DrawPixel(start_x + 2,start_y + 1, GREEN);
  OledColorDisplay::DrawPixel(start_x + 3,start_y + 1, GREEN);
  OledColorDisplay::DrawPixel(start_x + 4,start_y + 1, GREEN);
  OledColorDisplay::DrawPixel(start_x + 0,start_y + 2, BLACK);
  OledColorDisplay::DrawPixel(start_x + 1,start_y + 2, BLACK);
  OledColorDisplay::DrawPixel(start_x + 2,start_y + 2, GREEN);
  OledColorDisplay::DrawPixel(start_x + 3,start_y + 2, BLACK);
  OledColorDisplay::DrawPixel(start_x + 4,start_y + 2, BLACK);
  OledColorDisplay::DrawPixel(start_x + 0,start_y + 3, BLACK);
  OledColorDisplay::DrawPixel(start_x + 1,start_y + 3, BLACK);
  OledColorDisplay::DrawPixel(start_x + 2,start_y + 3, GREEN);
  OledColorDisplay::DrawPixel(start_x + 3,start_y + 3, BLACK);
  OledColorDisplay::DrawPixel(start_x + 4,start_y + 3, BLACK);
  OledColorDisplay::DrawPixel(start_x + 0,start_y + 4, BLACK);
  OledColorDisplay::DrawPixel(start_x + 1,start_y + 4, BLACK);
  OledColorDisplay::DrawPixel(start_x + 2,start_y + 4, GREEN);
  OledColorDisplay::DrawPixel(start_x + 3,start_y + 4, BLACK);
  OledColorDisplay::DrawPixel(start_x + 4,start_y + 4, BLACK);
  OledColorDisplay::DrawPixel(start_x + 0,start_y + 5, BLACK);
  OledColorDisplay::DrawPixel(start_x + 1,start_y + 5, BLACK);
  OledColorDisplay::DrawPixel(start_x + 2,start_y + 5, GREEN);
  OledColorDisplay::DrawPixel(start_x + 3,start_y + 5, BLACK);
  OledColorDisplay::DrawPixel(start_x + 4,start_y + 5, BLACK);
  OledColorDisplay::DrawPixel(start_x + 0,start_y + 6, BLACK);
  OledColorDisplay::DrawPixel(start_x + 1,start_y + 6, BLACK);
  OledColorDisplay::DrawPixel(start_x + 2,start_y + 6, GREEN);
  OledColorDisplay::DrawPixel(start_x + 3,start_y + 6, BLACK);
  OledColorDisplay::DrawPixel(start_x + 4,start_y + 6, BLACK);
  OledColorDisplay::DrawPixel(start_x + 0,start_y + 7, GREEN);
  OledColorDisplay::DrawPixel(start_x + 1,start_y + 7, GREEN);
  OledColorDisplay::DrawPixel(start_x + 2,start_y + 7, GREEN);
  OledColorDisplay::DrawPixel(start_x + 3,start_y + 7, GREEN);
  OledColorDisplay::DrawPixel(start_x + 4,start_y + 7, GREEN);
} else if ( mydrawchar == "J" ) {
  OledColorDisplay::DrawPixel(start_x + 0,start_y + 0, BLACK);
  OledColorDisplay::DrawPixel(start_x + 1,start_y + 0, BLACK);
  OledColorDisplay::DrawPixel(start_x + 2,start_y + 0, BLACK);
  OledColorDisplay::DrawPixel(start_x + 3,start_y + 0, BLACK);
  OledColorDisplay::DrawPixel(start_x + 4,start_y + 0, BLACK);
  OledColorDisplay::DrawPixel(start_x + 0,start_y + 1, BLACK);
  OledColorDisplay::DrawPixel(start_x + 1,start_y + 1, BLACK);
  OledColorDisplay::DrawPixel(start_x + 2,start_y + 1, GREEN);
  OledColorDisplay::DrawPixel(start_x + 3,start_y + 1, GREEN);
  OledColorDisplay::DrawPixel(start_x + 4,start_y + 1, GREEN);
  OledColorDisplay::DrawPixel(start_x + 0,start_y + 2, BLACK);
  OledColorDisplay::DrawPixel(start_x + 1,start_y + 2, BLACK);
  OledColorDisplay::DrawPixel(start_x + 2,start_y + 2, BLACK);
  OledColorDisplay::DrawPixel(start_x + 3,start_y + 2, GREEN);
  OledColorDisplay::DrawPixel(start_x + 4,start_y + 2, BLACK);
  OledColorDisplay::DrawPixel(start_x + 0,start_y + 3, BLACK);
  OledColorDisplay::DrawPixel(start_x + 1,start_y + 3, BLACK);
  OledColorDisplay::DrawPixel(start_x + 2,start_y + 3, BLACK);
  OledColorDisplay::DrawPixel(start_x + 3,start_y + 3, GREEN);
  OledColorDisplay::DrawPixel(start_x + 4,start_y + 3, BLACK);
  OledColorDisplay::DrawPixel(start_x + 0,start_y + 4, BLACK);
  OledColorDisplay::DrawPixel(start_x + 1,start_y + 4, BLACK);
  OledColorDisplay::DrawPixel(start_x + 2,start_y + 4, BLACK);
  OledColorDisplay::DrawPixel(start_x + 3,start_y + 4, GREEN);
  OledColorDisplay::DrawPixel(start_x + 4,start_y + 4, BLACK);
  OledColorDisplay::DrawPixel(start_x + 0,start_y + 5, BLACK);
  OledColorDisplay::DrawPixel(start_x + 1,start_y + 5, BLACK);
  OledColorDisplay::DrawPixel(start_x + 2,start_y + 5, BLACK);
  OledColorDisplay::DrawPixel(start_x + 3,start_y + 5, GREEN);
  OledColorDisplay::DrawPixel(start_x + 4,start_y + 5, BLACK);
  OledColorDisplay::DrawPixel(start_x + 0,start_y + 6, GREEN);
  OledColorDisplay::DrawPixel(start_x + 1,start_y + 6, BLACK);
  OledColorDisplay::DrawPixel(start_x + 2,start_y + 6, BLACK);
  OledColorDisplay::DrawPixel(start_x + 3,start_y + 6, GREEN);
  OledColorDisplay::DrawPixel(start_x + 4,start_y + 6, BLACK);
  OledColorDisplay::DrawPixel(start_x + 0,start_y + 7, BLACK);
  OledColorDisplay::DrawPixel(start_x + 1,start_y + 7, GREEN);
  OledColorDisplay::DrawPixel(start_x + 2,start_y + 7, GREEN);
  OledColorDisplay::DrawPixel(start_x + 3,start_y + 7, BLACK);
  OledColorDisplay::DrawPixel(start_x + 4,start_y + 7, BLACK);
} else if ( mydrawchar == "K" ) {
  OledColorDisplay::DrawPixel(start_x + 0,start_y + 0, BLACK);
  OledColorDisplay::DrawPixel(start_x + 1,start_y + 0, BLACK);
  OledColorDisplay::DrawPixel(start_x + 2,start_y + 0, BLACK);
  OledColorDisplay::DrawPixel(start_x + 3,start_y + 0, BLACK);
  OledColorDisplay::DrawPixel(start_x + 4,start_y + 0, BLACK);
  OledColorDisplay::DrawPixel(start_x + 0,start_y + 1, GREEN);
  OledColorDisplay::DrawPixel(start_x + 1,start_y + 1, BLACK);
  OledColorDisplay::DrawPixel(start_x + 2,start_y + 1, BLACK);
  OledColorDisplay::DrawPixel(start_x + 3,start_y + 1, BLACK);
  OledColorDisplay::DrawPixel(start_x + 4,start_y + 1, GREEN);
  OledColorDisplay::DrawPixel(start_x + 0,start_y + 2, GREEN);
  OledColorDisplay::DrawPixel(start_x + 1,start_y + 2, BLACK);
  OledColorDisplay::DrawPixel(start_x + 2,start_y + 2, BLACK);
  OledColorDisplay::DrawPixel(start_x + 3,start_y + 2, GREEN);
  OledColorDisplay::DrawPixel(start_x + 4,start_y + 2, BLACK);
  OledColorDisplay::DrawPixel(start_x + 0,start_y + 3, GREEN);
  OledColorDisplay::DrawPixel(start_x + 1,start_y + 3, BLACK);
  OledColorDisplay::DrawPixel(start_x + 2,start_y + 3, GREEN);
  OledColorDisplay::DrawPixel(start_x + 3,start_y + 3, BLACK);
  OledColorDisplay::DrawPixel(start_x + 4,start_y + 3, BLACK);
  OledColorDisplay::DrawPixel(start_x + 0,start_y + 4, GREEN);
  OledColorDisplay::DrawPixel(start_x + 1,start_y + 4, GREEN);
  OledColorDisplay::DrawPixel(start_x + 2,start_y + 4, BLACK);
  OledColorDisplay::DrawPixel(start_x + 3,start_y + 4, BLACK);
  OledColorDisplay::DrawPixel(start_x + 4,start_y + 4, BLACK);
  OledColorDisplay::DrawPixel(start_x + 0,start_y + 5, GREEN);
  OledColorDisplay::DrawPixel(start_x + 1,start_y + 5, BLACK);
  OledColorDisplay::DrawPixel(start_x + 2,start_y + 5, GREEN);
  OledColorDisplay::DrawPixel(start_x + 3,start_y + 5, BLACK);
  OledColorDisplay::DrawPixel(start_x + 4,start_y + 5, BLACK);
  OledColorDisplay::DrawPixel(start_x + 0,start_y + 6, GREEN);
  OledColorDisplay::DrawPixel(start_x + 1,start_y + 6, BLACK);
  OledColorDisplay::DrawPixel(start_x + 2,start_y + 6, BLACK);
  OledColorDisplay::DrawPixel(start_x + 3,start_y + 6, GREEN);
  OledColorDisplay::DrawPixel(start_x + 4,start_y + 6, BLACK);
  OledColorDisplay::DrawPixel(start_x + 0,start_y + 7, GREEN);
  OledColorDisplay::DrawPixel(start_x + 1,start_y + 7, BLACK);
  OledColorDisplay::DrawPixel(start_x + 2,start_y + 7, BLACK);
  OledColorDisplay::DrawPixel(start_x + 3,start_y + 7, BLACK);
  OledColorDisplay::DrawPixel(start_x + 4,start_y + 7, GREEN);
} else if ( mydrawchar == "L" ) {
  OledColorDisplay::DrawPixel(start_x + 0,start_y + 0, BLACK);
  OledColorDisplay::DrawPixel(start_x + 1,start_y + 0, BLACK);
  OledColorDisplay::DrawPixel(start_x + 2,start_y + 0, BLACK);
  OledColorDisplay::DrawPixel(start_x + 3,start_y + 0, BLACK);
  OledColorDisplay::DrawPixel(start_x + 4,start_y + 0, BLACK);
  OledColorDisplay::DrawPixel(start_x + 0,start_y + 1, GREEN);
  OledColorDisplay::DrawPixel(start_x + 1,start_y + 1, BLACK);
  OledColorDisplay::DrawPixel(start_x + 2,start_y + 1, BLACK);
  OledColorDisplay::DrawPixel(start_x + 3,start_y + 1, BLACK);
  OledColorDisplay::DrawPixel(start_x + 4,start_y + 1, BLACK);
  OledColorDisplay::DrawPixel(start_x + 0,start_y + 2, GREEN);
  OledColorDisplay::DrawPixel(start_x + 1,start_y + 2, BLACK);
  OledColorDisplay::DrawPixel(start_x + 2,start_y + 2, BLACK);
  OledColorDisplay::DrawPixel(start_x + 3,start_y + 2, BLACK);
  OledColorDisplay::DrawPixel(start_x + 4,start_y + 2, BLACK);
  OledColorDisplay::DrawPixel(start_x + 0,start_y + 3, GREEN);
  OledColorDisplay::DrawPixel(start_x + 1,start_y + 3, BLACK);
  OledColorDisplay::DrawPixel(start_x + 2,start_y + 3, BLACK);
  OledColorDisplay::DrawPixel(start_x + 3,start_y + 3, BLACK);
  OledColorDisplay::DrawPixel(start_x + 4,start_y + 3, BLACK);
  OledColorDisplay::DrawPixel(start_x + 0,start_y + 4, GREEN);
  OledColorDisplay::DrawPixel(start_x + 1,start_y + 4, BLACK);
  OledColorDisplay::DrawPixel(start_x + 2,start_y + 4, BLACK);
  OledColorDisplay::DrawPixel(start_x + 3,start_y + 4, BLACK);
  OledColorDisplay::DrawPixel(start_x + 4,start_y + 4, BLACK);
  OledColorDisplay::DrawPixel(start_x + 0,start_y + 5, GREEN);
  OledColorDisplay::DrawPixel(start_x + 1,start_y + 5, BLACK);
  OledColorDisplay::DrawPixel(start_x + 2,start_y + 5, BLACK);
  OledColorDisplay::DrawPixel(start_x + 3,start_y + 5, BLACK);
  OledColorDisplay::DrawPixel(start_x + 4,start_y + 5, BLACK);
  OledColorDisplay::DrawPixel(start_x + 0,start_y + 6, GREEN);
  OledColorDisplay::DrawPixel(start_x + 1,start_y + 6, BLACK);
  OledColorDisplay::DrawPixel(start_x + 2,start_y + 6, BLACK);
  OledColorDisplay::DrawPixel(start_x + 3,start_y + 6, BLACK);
  OledColorDisplay::DrawPixel(start_x + 4,start_y + 6, BLACK);
  OledColorDisplay::DrawPixel(start_x + 0,start_y + 7, GREEN);
  OledColorDisplay::DrawPixel(start_x + 1,start_y + 7, GREEN);
  OledColorDisplay::DrawPixel(start_x + 2,start_y + 7, GREEN);
  OledColorDisplay::DrawPixel(start_x + 3,start_y + 7, GREEN);
  OledColorDisplay::DrawPixel(start_x + 4,start_y + 7, GREEN);
} else if ( mydrawchar == "M" ) {
  OledColorDisplay::DrawPixel(start_x + 0,start_y + 0, BLACK);
  OledColorDisplay::DrawPixel(start_x + 1,start_y + 0, BLACK);
  OledColorDisplay::DrawPixel(start_x + 2,start_y + 0, BLACK);
  OledColorDisplay::DrawPixel(start_x + 3,start_y + 0, BLACK);
  OledColorDisplay::DrawPixel(start_x + 4,start_y + 0, BLACK);
  OledColorDisplay::DrawPixel(start_x + 0,start_y + 1, GREEN);
  OledColorDisplay::DrawPixel(start_x + 1,start_y + 1, BLACK);
  OledColorDisplay::DrawPixel(start_x + 2,start_y + 1, BLACK);
  OledColorDisplay::DrawPixel(start_x + 3,start_y + 1, BLACK);
  OledColorDisplay::DrawPixel(start_x + 4,start_y + 1, GREEN);
  OledColorDisplay::DrawPixel(start_x + 0,start_y + 2, GREEN);
  OledColorDisplay::DrawPixel(start_x + 1,start_y + 2, GREEN);
  OledColorDisplay::DrawPixel(start_x + 2,start_y + 2, BLACK);
  OledColorDisplay::DrawPixel(start_x + 3,start_y + 2, GREEN);
  OledColorDisplay::DrawPixel(start_x + 4,start_y + 2, GREEN);
  OledColorDisplay::DrawPixel(start_x + 0,start_y + 3, GREEN);
  OledColorDisplay::DrawPixel(start_x + 1,start_y + 3, BLACK);
  OledColorDisplay::DrawPixel(start_x + 2,start_y + 3, GREEN);
  OledColorDisplay::DrawPixel(start_x + 3,start_y + 3, BLACK);
  OledColorDisplay::DrawPixel(start_x + 4,start_y + 3, GREEN);
  OledColorDisplay::DrawPixel(start_x + 0,start_y + 4, GREEN);
  OledColorDisplay::DrawPixel(start_x + 1,start_y + 4, BLACK);
  OledColorDisplay::DrawPixel(start_x + 2,start_y + 4, GREEN);
  OledColorDisplay::DrawPixel(start_x + 3,start_y + 4, BLACK);
  OledColorDisplay::DrawPixel(start_x + 4,start_y + 4, GREEN);
  OledColorDisplay::DrawPixel(start_x + 0,start_y + 5, GREEN);
  OledColorDisplay::DrawPixel(start_x + 1,start_y + 5, BLACK);
  OledColorDisplay::DrawPixel(start_x + 2,start_y + 5, BLACK);
  OledColorDisplay::DrawPixel(start_x + 3,start_y + 5, BLACK);
  OledColorDisplay::DrawPixel(start_x + 4,start_y + 5, GREEN);
  OledColorDisplay::DrawPixel(start_x + 0,start_y + 6, GREEN);
  OledColorDisplay::DrawPixel(start_x + 1,start_y + 6, BLACK);
  OledColorDisplay::DrawPixel(start_x + 2,start_y + 6, BLACK);
  OledColorDisplay::DrawPixel(start_x + 3,start_y + 6, BLACK);
  OledColorDisplay::DrawPixel(start_x + 4,start_y + 6, GREEN);
  OledColorDisplay::DrawPixel(start_x + 0,start_y + 7, GREEN);
  OledColorDisplay::DrawPixel(start_x + 1,start_y + 7, BLACK);
  OledColorDisplay::DrawPixel(start_x + 2,start_y + 7, BLACK);
  OledColorDisplay::DrawPixel(start_x + 3,start_y + 7, BLACK);
  OledColorDisplay::DrawPixel(start_x + 4,start_y + 7, GREEN);
} else if ( mydrawchar == "N" ) {
  OledColorDisplay::DrawPixel(start_x + 0,start_y + 0, BLACK);
  OledColorDisplay::DrawPixel(start_x + 1,start_y + 0, BLACK);
  OledColorDisplay::DrawPixel(start_x + 2,start_y + 0, BLACK);
  OledColorDisplay::DrawPixel(start_x + 3,start_y + 0, BLACK);
  OledColorDisplay::DrawPixel(start_x + 4,start_y + 0, BLACK);
  OledColorDisplay::DrawPixel(start_x + 0,start_y + 1, GREEN);
  OledColorDisplay::DrawPixel(start_x + 1,start_y + 1, BLACK);
  OledColorDisplay::DrawPixel(start_x + 2,start_y + 1, BLACK);
  OledColorDisplay::DrawPixel(start_x + 3,start_y + 1, BLACK);
  OledColorDisplay::DrawPixel(start_x + 4,start_y + 1, GREEN);
  OledColorDisplay::DrawPixel(start_x + 0,start_y + 2, GREEN);
  OledColorDisplay::DrawPixel(start_x + 1,start_y + 2, BLACK);
  OledColorDisplay::DrawPixel(start_x + 2,start_y + 2, BLACK);
  OledColorDisplay::DrawPixel(start_x + 3,start_y + 2, BLACK);
  OledColorDisplay::DrawPixel(start_x + 4,start_y + 2, GREEN);
  OledColorDisplay::DrawPixel(start_x + 0,start_y + 3, GREEN);
  OledColorDisplay::DrawPixel(start_x + 1,start_y + 3, GREEN);
  OledColorDisplay::DrawPixel(start_x + 2,start_y + 3, BLACK);
  OledColorDisplay::DrawPixel(start_x + 3,start_y + 3, BLACK);
  OledColorDisplay::DrawPixel(start_x + 4,start_y + 3, GREEN);
  OledColorDisplay::DrawPixel(start_x + 0,start_y + 4, GREEN);
  OledColorDisplay::DrawPixel(start_x + 1,start_y + 4, BLACK);
  OledColorDisplay::DrawPixel(start_x + 2,start_y + 4, GREEN);
  OledColorDisplay::DrawPixel(start_x + 3,start_y + 4, BLACK);
  OledColorDisplay::DrawPixel(start_x + 4,start_y + 4, GREEN);
  OledColorDisplay::DrawPixel(start_x + 0,start_y + 5, GREEN);
  OledColorDisplay::DrawPixel(start_x + 1,start_y + 5, BLACK);
  OledColorDisplay::DrawPixel(start_x + 2,start_y + 5, BLACK);
  OledColorDisplay::DrawPixel(start_x + 3,start_y + 5, GREEN);
  OledColorDisplay::DrawPixel(start_x + 4,start_y + 5, GREEN);
  OledColorDisplay::DrawPixel(start_x + 0,start_y + 6, GREEN);
  OledColorDisplay::DrawPixel(start_x + 1,start_y + 6, BLACK);
  OledColorDisplay::DrawPixel(start_x + 2,start_y + 6, BLACK);
  OledColorDisplay::DrawPixel(start_x + 3,start_y + 6, BLACK);
  OledColorDisplay::DrawPixel(start_x + 4,start_y + 6, GREEN);
  OledColorDisplay::DrawPixel(start_x + 0,start_y + 7, GREEN);
  OledColorDisplay::DrawPixel(start_x + 1,start_y + 7, BLACK);
  OledColorDisplay::DrawPixel(start_x + 2,start_y + 7, BLACK);
  OledColorDisplay::DrawPixel(start_x + 3,start_y + 7, BLACK);
  OledColorDisplay::DrawPixel(start_x + 4,start_y + 7, GREEN);
} else if ( mydrawchar == "O" ) {
  OledColorDisplay::DrawPixel(start_x + 0,start_y + 0, BLACK);
  OledColorDisplay::DrawPixel(start_x + 1,start_y + 0, BLACK);
  OledColorDisplay::DrawPixel(start_x + 2,start_y + 0, BLACK);
  OledColorDisplay::DrawPixel(start_x + 3,start_y + 0, BLACK);
  OledColorDisplay::DrawPixel(start_x + 4,start_y + 0, BLACK);
  OledColorDisplay::DrawPixel(start_x + 0,start_y + 1, BLACK);
  OledColorDisplay::DrawPixel(start_x + 1,start_y + 1, GREEN);
  OledColorDisplay::DrawPixel(start_x + 2,start_y + 1, GREEN);
  OledColorDisplay::DrawPixel(start_x + 3,start_y + 1, GREEN);
  OledColorDisplay::DrawPixel(start_x + 4,start_y + 1, BLACK);
  OledColorDisplay::DrawPixel(start_x + 0,start_y + 2, GREEN);
  OledColorDisplay::DrawPixel(start_x + 1,start_y + 2, BLACK);
  OledColorDisplay::DrawPixel(start_x + 2,start_y + 2, BLACK);
  OledColorDisplay::DrawPixel(start_x + 3,start_y + 2, BLACK);
  OledColorDisplay::DrawPixel(start_x + 4,start_y + 2, GREEN);
  OledColorDisplay::DrawPixel(start_x + 0,start_y + 3, GREEN);
  OledColorDisplay::DrawPixel(start_x + 1,start_y + 3, BLACK);
  OledColorDisplay::DrawPixel(start_x + 2,start_y + 3, BLACK);
  OledColorDisplay::DrawPixel(start_x + 3,start_y + 3, BLACK);
  OledColorDisplay::DrawPixel(start_x + 4,start_y + 3, GREEN);
  OledColorDisplay::DrawPixel(start_x + 0,start_y + 4, GREEN);
  OledColorDisplay::DrawPixel(start_x + 1,start_y + 4, BLACK);
  OledColorDisplay::DrawPixel(start_x + 2,start_y + 4, BLACK);
  OledColorDisplay::DrawPixel(start_x + 3,start_y + 4, BLACK);
  OledColorDisplay::DrawPixel(start_x + 4,start_y + 4, GREEN);
  OledColorDisplay::DrawPixel(start_x + 0,start_y + 5, GREEN);
  OledColorDisplay::DrawPixel(start_x + 1,start_y + 5, BLACK);
  OledColorDisplay::DrawPixel(start_x + 2,start_y + 5, BLACK);
  OledColorDisplay::DrawPixel(start_x + 3,start_y + 5, BLACK);
  OledColorDisplay::DrawPixel(start_x + 4,start_y + 5, GREEN);
  OledColorDisplay::DrawPixel(start_x + 0,start_y + 6, GREEN);
  OledColorDisplay::DrawPixel(start_x + 1,start_y + 6, BLACK);
  OledColorDisplay::DrawPixel(start_x + 2,start_y + 6, BLACK);
  OledColorDisplay::DrawPixel(start_x + 3,start_y + 6, BLACK);
  OledColorDisplay::DrawPixel(start_x + 4,start_y + 6, GREEN);
  OledColorDisplay::DrawPixel(start_x + 0,start_y + 7, BLACK);
  OledColorDisplay::DrawPixel(start_x + 1,start_y + 7, GREEN);
  OledColorDisplay::DrawPixel(start_x + 2,start_y + 7, GREEN);
  OledColorDisplay::DrawPixel(start_x + 3,start_y + 7, GREEN);
  OledColorDisplay::DrawPixel(start_x + 4,start_y + 7, BLACK);
} else if ( mydrawchar == "P" ) {
  OledColorDisplay::DrawPixel(start_x + 0,start_y + 0, BLACK);
  OledColorDisplay::DrawPixel(start_x + 1,start_y + 0, BLACK);
  OledColorDisplay::DrawPixel(start_x + 2,start_y + 0, BLACK);
  OledColorDisplay::DrawPixel(start_x + 3,start_y + 0, BLACK);
  OledColorDisplay::DrawPixel(start_x + 4,start_y + 0, BLACK);
  OledColorDisplay::DrawPixel(start_x + 0,start_y + 1, GREEN);
  OledColorDisplay::DrawPixel(start_x + 1,start_y + 1, GREEN);
  OledColorDisplay::DrawPixel(start_x + 2,start_y + 1, GREEN);
  OledColorDisplay::DrawPixel(start_x + 3,start_y + 1, GREEN);
  OledColorDisplay::DrawPixel(start_x + 4,start_y + 1, BLACK);
  OledColorDisplay::DrawPixel(start_x + 0,start_y + 2, GREEN);
  OledColorDisplay::DrawPixel(start_x + 1,start_y + 2, BLACK);
  OledColorDisplay::DrawPixel(start_x + 2,start_y + 2, BLACK);
  OledColorDisplay::DrawPixel(start_x + 3,start_y + 2, BLACK);
  OledColorDisplay::DrawPixel(start_x + 4,start_y + 2, GREEN);
  OledColorDisplay::DrawPixel(start_x + 0,start_y + 3, GREEN);
  OledColorDisplay::DrawPixel(start_x + 1,start_y + 3, BLACK);
  OledColorDisplay::DrawPixel(start_x + 2,start_y + 3, BLACK);
  OledColorDisplay::DrawPixel(start_x + 3,start_y + 3, BLACK);
  OledColorDisplay::DrawPixel(start_x + 4,start_y + 3, GREEN);
  OledColorDisplay::DrawPixel(start_x + 0,start_y + 4, GREEN);
  OledColorDisplay::DrawPixel(start_x + 1,start_y + 4, GREEN);
  OledColorDisplay::DrawPixel(start_x + 2,start_y + 4, GREEN);
  OledColorDisplay::DrawPixel(start_x + 3,start_y + 4, GREEN);
  OledColorDisplay::DrawPixel(start_x + 4,start_y + 4, BLACK);
  OledColorDisplay::DrawPixel(start_x + 0,start_y + 5, GREEN);
  OledColorDisplay::DrawPixel(start_x + 1,start_y + 5, BLACK);
  OledColorDisplay::DrawPixel(start_x + 2,start_y + 5, BLACK);
  OledColorDisplay::DrawPixel(start_x + 3,start_y + 5, BLACK);
  OledColorDisplay::DrawPixel(start_x + 4,start_y + 5, BLACK);
  OledColorDisplay::DrawPixel(start_x + 0,start_y + 6, GREEN);
  OledColorDisplay::DrawPixel(start_x + 1,start_y + 6, BLACK);
  OledColorDisplay::DrawPixel(start_x + 2,start_y + 6, BLACK);
  OledColorDisplay::DrawPixel(start_x + 3,start_y + 6, BLACK);
  OledColorDisplay::DrawPixel(start_x + 4,start_y + 6, BLACK);
  OledColorDisplay::DrawPixel(start_x + 0,start_y + 7, GREEN);
  OledColorDisplay::DrawPixel(start_x + 1,start_y + 7, BLACK);
  OledColorDisplay::DrawPixel(start_x + 2,start_y + 7, BLACK);
  OledColorDisplay::DrawPixel(start_x + 3,start_y + 7, BLACK);
  OledColorDisplay::DrawPixel(start_x + 4,start_y + 7, BLACK);
} else if ( mydrawchar == "Q" ) {
  OledColorDisplay::DrawPixel(start_x + 0,start_y + 0, BLACK);
  OledColorDisplay::DrawPixel(start_x + 1,start_y + 0, BLACK);
  OledColorDisplay::DrawPixel(start_x + 2,start_y + 0, BLACK);
  OledColorDisplay::DrawPixel(start_x + 3,start_y + 0, BLACK);
  OledColorDisplay::DrawPixel(start_x + 4,start_y + 0, BLACK);
  OledColorDisplay::DrawPixel(start_x + 0,start_y + 1, BLACK);
  OledColorDisplay::DrawPixel(start_x + 1,start_y + 1, GREEN);
  OledColorDisplay::DrawPixel(start_x + 2,start_y + 1, GREEN);
  OledColorDisplay::DrawPixel(start_x + 3,start_y + 1, GREEN);
  OledColorDisplay::DrawPixel(start_x + 4,start_y + 1, BLACK);
  OledColorDisplay::DrawPixel(start_x + 0,start_y + 2, GREEN);
  OledColorDisplay::DrawPixel(start_x + 1,start_y + 2, BLACK);
  OledColorDisplay::DrawPixel(start_x + 2,start_y + 2, BLACK);
  OledColorDisplay::DrawPixel(start_x + 3,start_y + 2, BLACK);
  OledColorDisplay::DrawPixel(start_x + 4,start_y + 2, GREEN);
  OledColorDisplay::DrawPixel(start_x + 0,start_y + 3, GREEN);
  OledColorDisplay::DrawPixel(start_x + 1,start_y + 3, BLACK);
  OledColorDisplay::DrawPixel(start_x + 2,start_y + 3, BLACK);
  OledColorDisplay::DrawPixel(start_x + 3,start_y + 3, BLACK);
  OledColorDisplay::DrawPixel(start_x + 4,start_y + 3, GREEN);
  OledColorDisplay::DrawPixel(start_x + 0,start_y + 4, GREEN);
  OledColorDisplay::DrawPixel(start_x + 1,start_y + 4, BLACK);
  OledColorDisplay::DrawPixel(start_x + 2,start_y + 4, BLACK);
  OledColorDisplay::DrawPixel(start_x + 3,start_y + 4, BLACK);
  OledColorDisplay::DrawPixel(start_x + 4,start_y + 4, GREEN);
  OledColorDisplay::DrawPixel(start_x + 0,start_y + 5, GREEN);
  OledColorDisplay::DrawPixel(start_x + 1,start_y + 5, BLACK);
  OledColorDisplay::DrawPixel(start_x + 2,start_y + 5, GREEN);
  OledColorDisplay::DrawPixel(start_x + 3,start_y + 5, BLACK);
  OledColorDisplay::DrawPixel(start_x + 4,start_y + 5, GREEN);
  OledColorDisplay::DrawPixel(start_x + 0,start_y + 6, GREEN);
  OledColorDisplay::DrawPixel(start_x + 1,start_y + 6, BLACK);
  OledColorDisplay::DrawPixel(start_x + 2,start_y + 6, BLACK);
  OledColorDisplay::DrawPixel(start_x + 3,start_y + 6, GREEN);
  OledColorDisplay::DrawPixel(start_x + 4,start_y + 6, BLACK);
  OledColorDisplay::DrawPixel(start_x + 0,start_y + 7, BLACK);
  OledColorDisplay::DrawPixel(start_x + 1,start_y + 7, GREEN);
  OledColorDisplay::DrawPixel(start_x + 2,start_y + 7, GREEN);
  OledColorDisplay::DrawPixel(start_x + 3,start_y + 7, BLACK);
  OledColorDisplay::DrawPixel(start_x + 4,start_y + 7, GREEN);
} else if ( mydrawchar == "R" ) {
  OledColorDisplay::DrawPixel(start_x + 0,start_y + 0, BLACK);
  OledColorDisplay::DrawPixel(start_x + 1,start_y + 0, BLACK);
  OledColorDisplay::DrawPixel(start_x + 2,start_y + 0, BLACK);
  OledColorDisplay::DrawPixel(start_x + 3,start_y + 0, BLACK);
  OledColorDisplay::DrawPixel(start_x + 4,start_y + 0, BLACK);
  OledColorDisplay::DrawPixel(start_x + 0,start_y + 1, GREEN);
  OledColorDisplay::DrawPixel(start_x + 1,start_y + 1, GREEN);
  OledColorDisplay::DrawPixel(start_x + 2,start_y + 1, GREEN);
  OledColorDisplay::DrawPixel(start_x + 3,start_y + 1, GREEN);
  OledColorDisplay::DrawPixel(start_x + 4,start_y + 1, BLACK);
  OledColorDisplay::DrawPixel(start_x + 0,start_y + 2, GREEN);
  OledColorDisplay::DrawPixel(start_x + 1,start_y + 2, BLACK);
  OledColorDisplay::DrawPixel(start_x + 2,start_y + 2, BLACK);
  OledColorDisplay::DrawPixel(start_x + 3,start_y + 2, BLACK);
  OledColorDisplay::DrawPixel(start_x + 4,start_y + 2, GREEN);
  OledColorDisplay::DrawPixel(start_x + 0,start_y + 3, GREEN);
  OledColorDisplay::DrawPixel(start_x + 1,start_y + 3, BLACK);
  OledColorDisplay::DrawPixel(start_x + 2,start_y + 3, BLACK);
  OledColorDisplay::DrawPixel(start_x + 3,start_y + 3, BLACK);
  OledColorDisplay::DrawPixel(start_x + 4,start_y + 3, GREEN);
  OledColorDisplay::DrawPixel(start_x + 0,start_y + 4, GREEN);
  OledColorDisplay::DrawPixel(start_x + 1,start_y + 4, GREEN);
  OledColorDisplay::DrawPixel(start_x + 2,start_y + 4, GREEN);
  OledColorDisplay::DrawPixel(start_x + 3,start_y + 4, GREEN);
  OledColorDisplay::DrawPixel(start_x + 4,start_y + 4, BLACK);
  OledColorDisplay::DrawPixel(start_x + 0,start_y + 5, GREEN);
  OledColorDisplay::DrawPixel(start_x + 1,start_y + 5, BLACK);
  OledColorDisplay::DrawPixel(start_x + 2,start_y + 5, GREEN);
  OledColorDisplay::DrawPixel(start_x + 3,start_y + 5, BLACK);
  OledColorDisplay::DrawPixel(start_x + 4,start_y + 5, BLACK);
  OledColorDisplay::DrawPixel(start_x + 0,start_y + 6, GREEN);
  OledColorDisplay::DrawPixel(start_x + 1,start_y + 6, BLACK);
  OledColorDisplay::DrawPixel(start_x + 2,start_y + 6, BLACK);
  OledColorDisplay::DrawPixel(start_x + 3,start_y + 6, GREEN);
  OledColorDisplay::DrawPixel(start_x + 4,start_y + 6, BLACK);
  OledColorDisplay::DrawPixel(start_x + 0,start_y + 7, GREEN);
  OledColorDisplay::DrawPixel(start_x + 1,start_y + 7, BLACK);
  OledColorDisplay::DrawPixel(start_x + 2,start_y + 7, BLACK);
  OledColorDisplay::DrawPixel(start_x + 3,start_y + 7, BLACK);
  OledColorDisplay::DrawPixel(start_x + 4,start_y + 7, GREEN);
} else if ( mydrawchar == "S" ) {
  OledColorDisplay::DrawPixel(start_x + 0,start_y + 0, BLACK);
  OledColorDisplay::DrawPixel(start_x + 1,start_y + 0, BLACK);
  OledColorDisplay::DrawPixel(start_x + 2,start_y + 0, BLACK);
  OledColorDisplay::DrawPixel(start_x + 3,start_y + 0, BLACK);
  OledColorDisplay::DrawPixel(start_x + 4,start_y + 0, BLACK);
  OledColorDisplay::DrawPixel(start_x + 0,start_y + 1, BLACK);
  OledColorDisplay::DrawPixel(start_x + 1,start_y + 1, GREEN);
  OledColorDisplay::DrawPixel(start_x + 2,start_y + 1, GREEN);
  OledColorDisplay::DrawPixel(start_x + 3,start_y + 1, GREEN);
  OledColorDisplay::DrawPixel(start_x + 4,start_y + 1, GREEN);
  OledColorDisplay::DrawPixel(start_x + 0,start_y + 2, GREEN);
  OledColorDisplay::DrawPixel(start_x + 1,start_y + 2, BLACK);
  OledColorDisplay::DrawPixel(start_x + 2,start_y + 2, BLACK);
  OledColorDisplay::DrawPixel(start_x + 3,start_y + 2, BLACK);
  OledColorDisplay::DrawPixel(start_x + 4,start_y + 2, BLACK);
  OledColorDisplay::DrawPixel(start_x + 0,start_y + 3, GREEN);
  OledColorDisplay::DrawPixel(start_x + 1,start_y + 3, BLACK);
  OledColorDisplay::DrawPixel(start_x + 2,start_y + 3, BLACK);
  OledColorDisplay::DrawPixel(start_x + 3,start_y + 3, BLACK);
  OledColorDisplay::DrawPixel(start_x + 4,start_y + 3, BLACK);
  OledColorDisplay::DrawPixel(start_x + 0,start_y + 4, BLACK);
  OledColorDisplay::DrawPixel(start_x + 1,start_y + 4, GREEN);
  OledColorDisplay::DrawPixel(start_x + 2,start_y + 4, GREEN);
  OledColorDisplay::DrawPixel(start_x + 3,start_y + 4, GREEN);
  OledColorDisplay::DrawPixel(start_x + 4,start_y + 4, BLACK);
  OledColorDisplay::DrawPixel(start_x + 0,start_y + 5, BLACK);
  OledColorDisplay::DrawPixel(start_x + 1,start_y + 5, BLACK);
  OledColorDisplay::DrawPixel(start_x + 2,start_y + 5, BLACK);
  OledColorDisplay::DrawPixel(start_x + 3,start_y + 5, BLACK);
  OledColorDisplay::DrawPixel(start_x + 4,start_y + 5, GREEN);
  OledColorDisplay::DrawPixel(start_x + 0,start_y + 6, BLACK);
  OledColorDisplay::DrawPixel(start_x + 1,start_y + 6, BLACK);
  OledColorDisplay::DrawPixel(start_x + 2,start_y + 6, BLACK);
  OledColorDisplay::DrawPixel(start_x + 3,start_y + 6, BLACK);
  OledColorDisplay::DrawPixel(start_x + 4,start_y + 6, GREEN);
  OledColorDisplay::DrawPixel(start_x + 0,start_y + 7, GREEN);
  OledColorDisplay::DrawPixel(start_x + 1,start_y + 7, GREEN);
  OledColorDisplay::DrawPixel(start_x + 2,start_y + 7, GREEN);
  OledColorDisplay::DrawPixel(start_x + 3,start_y + 7, GREEN);
  OledColorDisplay::DrawPixel(start_x + 4,start_y + 7, BLACK);
} else if ( mydrawchar == "T" ) {
  OledColorDisplay::DrawPixel(start_x + 0,start_y + 0, BLACK);
  OledColorDisplay::DrawPixel(start_x + 1,start_y + 0, BLACK);
  OledColorDisplay::DrawPixel(start_x + 2,start_y + 0, BLACK);
  OledColorDisplay::DrawPixel(start_x + 3,start_y + 0, BLACK);
  OledColorDisplay::DrawPixel(start_x + 4,start_y + 0, BLACK);
  OledColorDisplay::DrawPixel(start_x + 0,start_y + 1, GREEN);
  OledColorDisplay::DrawPixel(start_x + 1,start_y + 1, GREEN);
  OledColorDisplay::DrawPixel(start_x + 2,start_y + 1, GREEN);
  OledColorDisplay::DrawPixel(start_x + 3,start_y + 1, GREEN);
  OledColorDisplay::DrawPixel(start_x + 4,start_y + 1, GREEN);
  OledColorDisplay::DrawPixel(start_x + 0,start_y + 2, BLACK);
  OledColorDisplay::DrawPixel(start_x + 1,start_y + 2, BLACK);
  OledColorDisplay::DrawPixel(start_x + 2,start_y + 2, GREEN);
  OledColorDisplay::DrawPixel(start_x + 3,start_y + 2, BLACK);
  OledColorDisplay::DrawPixel(start_x + 4,start_y + 2, BLACK);
  OledColorDisplay::DrawPixel(start_x + 0,start_y + 3, BLACK);
  OledColorDisplay::DrawPixel(start_x + 1,start_y + 3, BLACK);
  OledColorDisplay::DrawPixel(start_x + 2,start_y + 3, GREEN);
  OledColorDisplay::DrawPixel(start_x + 3,start_y + 3, BLACK);
  OledColorDisplay::DrawPixel(start_x + 4,start_y + 3, BLACK);
  OledColorDisplay::DrawPixel(start_x + 0,start_y + 4, BLACK);
  OledColorDisplay::DrawPixel(start_x + 1,start_y + 4, BLACK);
  OledColorDisplay::DrawPixel(start_x + 2,start_y + 4, GREEN);
  OledColorDisplay::DrawPixel(start_x + 3,start_y + 4, BLACK);
  OledColorDisplay::DrawPixel(start_x + 4,start_y + 4, BLACK);
  OledColorDisplay::DrawPixel(start_x + 0,start_y + 5, BLACK);
  OledColorDisplay::DrawPixel(start_x + 1,start_y + 5, BLACK);
  OledColorDisplay::DrawPixel(start_x + 2,start_y + 5, GREEN);
  OledColorDisplay::DrawPixel(start_x + 3,start_y + 5, BLACK);
  OledColorDisplay::DrawPixel(start_x + 4,start_y + 5, BLACK);
  OledColorDisplay::DrawPixel(start_x + 0,start_y + 6, BLACK);
  OledColorDisplay::DrawPixel(start_x + 1,start_y + 6, BLACK);
  OledColorDisplay::DrawPixel(start_x + 2,start_y + 6, GREEN);
  OledColorDisplay::DrawPixel(start_x + 3,start_y + 6, BLACK);
  OledColorDisplay::DrawPixel(start_x + 4,start_y + 6, BLACK);
  OledColorDisplay::DrawPixel(start_x + 0,start_y + 7, BLACK);
  OledColorDisplay::DrawPixel(start_x + 1,start_y + 7, BLACK);
  OledColorDisplay::DrawPixel(start_x + 2,start_y + 7, GREEN);
  OledColorDisplay::DrawPixel(start_x + 3,start_y + 7, BLACK);
  OledColorDisplay::DrawPixel(start_x + 4,start_y + 7, BLACK);
} else if ( mydrawchar == "U" ) {
  OledColorDisplay::DrawPixel(start_x + 0,start_y + 0, BLACK);
  OledColorDisplay::DrawPixel(start_x + 1,start_y + 0, BLACK);
  OledColorDisplay::DrawPixel(start_x + 2,start_y + 0, BLACK);
  OledColorDisplay::DrawPixel(start_x + 3,start_y + 0, BLACK);
  OledColorDisplay::DrawPixel(start_x + 4,start_y + 0, BLACK);
  OledColorDisplay::DrawPixel(start_x + 0,start_y + 1, GREEN);
  OledColorDisplay::DrawPixel(start_x + 1,start_y + 1, BLACK);
  OledColorDisplay::DrawPixel(start_x + 2,start_y + 1, BLACK);
  OledColorDisplay::DrawPixel(start_x + 3,start_y + 1, BLACK);
  OledColorDisplay::DrawPixel(start_x + 4,start_y + 1, GREEN);
  OledColorDisplay::DrawPixel(start_x + 0,start_y + 2, GREEN);
  OledColorDisplay::DrawPixel(start_x + 1,start_y + 2, BLACK);
  OledColorDisplay::DrawPixel(start_x + 2,start_y + 2, BLACK);
  OledColorDisplay::DrawPixel(start_x + 3,start_y + 2, BLACK);
  OledColorDisplay::DrawPixel(start_x + 4,start_y + 2, GREEN);
  OledColorDisplay::DrawPixel(start_x + 0,start_y + 3, GREEN);
  OledColorDisplay::DrawPixel(start_x + 1,start_y + 3, BLACK);
  OledColorDisplay::DrawPixel(start_x + 2,start_y + 3, BLACK);
  OledColorDisplay::DrawPixel(start_x + 3,start_y + 3, BLACK);
  OledColorDisplay::DrawPixel(start_x + 4,start_y + 3, GREEN);
  OledColorDisplay::DrawPixel(start_x + 0,start_y + 4, GREEN);
  OledColorDisplay::DrawPixel(start_x + 1,start_y + 4, BLACK);
  OledColorDisplay::DrawPixel(start_x + 2,start_y + 4, BLACK);
  OledColorDisplay::DrawPixel(start_x + 3,start_y + 4, BLACK);
  OledColorDisplay::DrawPixel(start_x + 4,start_y + 4, GREEN);
  OledColorDisplay::DrawPixel(start_x + 0,start_y + 5, GREEN);
  OledColorDisplay::DrawPixel(start_x + 1,start_y + 5, BLACK);
  OledColorDisplay::DrawPixel(start_x + 2,start_y + 5, BLACK);
  OledColorDisplay::DrawPixel(start_x + 3,start_y + 5, BLACK);
  OledColorDisplay::DrawPixel(start_x + 4,start_y + 5, GREEN);
  OledColorDisplay::DrawPixel(start_x + 0,start_y + 6, GREEN);
  OledColorDisplay::DrawPixel(start_x + 1,start_y + 6, BLACK);
  OledColorDisplay::DrawPixel(start_x + 2,start_y + 6, BLACK);
  OledColorDisplay::DrawPixel(start_x + 3,start_y + 6, BLACK);
  OledColorDisplay::DrawPixel(start_x + 4,start_y + 6, GREEN);
  OledColorDisplay::DrawPixel(start_x + 0,start_y + 7, BLACK);
  OledColorDisplay::DrawPixel(start_x + 1,start_y + 7, GREEN);
  OledColorDisplay::DrawPixel(start_x + 2,start_y + 7, GREEN);
  OledColorDisplay::DrawPixel(start_x + 3,start_y + 7, GREEN);
  OledColorDisplay::DrawPixel(start_x + 4,start_y + 7, BLACK);
} else if ( mydrawchar == "V" ) {
  OledColorDisplay::DrawPixel(start_x + 0,start_y + 0, BLACK);
  OledColorDisplay::DrawPixel(start_x + 1,start_y + 0, BLACK);
  OledColorDisplay::DrawPixel(start_x + 2,start_y + 0, BLACK);
  OledColorDisplay::DrawPixel(start_x + 3,start_y + 0, BLACK);
  OledColorDisplay::DrawPixel(start_x + 4,start_y + 0, BLACK);
  OledColorDisplay::DrawPixel(start_x + 0,start_y + 1, GREEN);
  OledColorDisplay::DrawPixel(start_x + 1,start_y + 1, BLACK);
  OledColorDisplay::DrawPixel(start_x + 2,start_y + 1, BLACK);
  OledColorDisplay::DrawPixel(start_x + 3,start_y + 1, BLACK);
  OledColorDisplay::DrawPixel(start_x + 4,start_y + 1, GREEN);
  OledColorDisplay::DrawPixel(start_x + 0,start_y + 2, GREEN);
  OledColorDisplay::DrawPixel(start_x + 1,start_y + 2, BLACK);
  OledColorDisplay::DrawPixel(start_x + 2,start_y + 2, BLACK);
  OledColorDisplay::DrawPixel(start_x + 3,start_y + 2, BLACK);
  OledColorDisplay::DrawPixel(start_x + 4,start_y + 2, GREEN);
  OledColorDisplay::DrawPixel(start_x + 0,start_y + 3, GREEN);
  OledColorDisplay::DrawPixel(start_x + 1,start_y + 3, BLACK);
  OledColorDisplay::DrawPixel(start_x + 2,start_y + 3, BLACK);
  OledColorDisplay::DrawPixel(start_x + 3,start_y + 3, BLACK);
  OledColorDisplay::DrawPixel(start_x + 4,start_y + 3, GREEN);
  OledColorDisplay::DrawPixel(start_x + 0,start_y + 4, GREEN);
  OledColorDisplay::DrawPixel(start_x + 1,start_y + 4, BLACK);
  OledColorDisplay::DrawPixel(start_x + 2,start_y + 4, BLACK);
  OledColorDisplay::DrawPixel(start_x + 3,start_y + 4, BLACK);
  OledColorDisplay::DrawPixel(start_x + 4,start_y + 4, GREEN);
  OledColorDisplay::DrawPixel(start_x + 0,start_y + 5, GREEN);
  OledColorDisplay::DrawPixel(start_x + 1,start_y + 5, BLACK);
  OledColorDisplay::DrawPixel(start_x + 2,start_y + 5, BLACK);
  OledColorDisplay::DrawPixel(start_x + 3,start_y + 5, BLACK);
  OledColorDisplay::DrawPixel(start_x + 4,start_y + 5, GREEN);
  OledColorDisplay::DrawPixel(start_x + 0,start_y + 6, BLACK);
  OledColorDisplay::DrawPixel(start_x + 1,start_y + 6, GREEN);
  OledColorDisplay::DrawPixel(start_x + 2,start_y + 6, BLACK);
  OledColorDisplay::DrawPixel(start_x + 3,start_y + 6, GREEN);
  OledColorDisplay::DrawPixel(start_x + 4,start_y + 6, BLACK);
  OledColorDisplay::DrawPixel(start_x + 0,start_y + 7, BLACK);
  OledColorDisplay::DrawPixel(start_x + 1,start_y + 7, BLACK);
  OledColorDisplay::DrawPixel(start_x + 2,start_y + 7, GREEN);
  OledColorDisplay::DrawPixel(start_x + 3,start_y + 7, BLACK);
  OledColorDisplay::DrawPixel(start_x + 4,start_y + 7, BLACK);
} else if ( mydrawchar == "W" ) {
  OledColorDisplay::DrawPixel(start_x + 0,start_y + 0, BLACK);
  OledColorDisplay::DrawPixel(start_x + 1,start_y + 0, BLACK);
  OledColorDisplay::DrawPixel(start_x + 2,start_y + 0, BLACK);
  OledColorDisplay::DrawPixel(start_x + 3,start_y + 0, BLACK);
  OledColorDisplay::DrawPixel(start_x + 4,start_y + 0, BLACK);
  OledColorDisplay::DrawPixel(start_x + 0,start_y + 1, GREEN);
  OledColorDisplay::DrawPixel(start_x + 1,start_y + 1, BLACK);
  OledColorDisplay::DrawPixel(start_x + 2,start_y + 1, BLACK);
  OledColorDisplay::DrawPixel(start_x + 3,start_y + 1, BLACK);
  OledColorDisplay::DrawPixel(start_x + 4,start_y + 1, GREEN);
  OledColorDisplay::DrawPixel(start_x + 0,start_y + 2, GREEN);
  OledColorDisplay::DrawPixel(start_x + 1,start_y + 2, BLACK);
  OledColorDisplay::DrawPixel(start_x + 2,start_y + 2, BLACK);
  OledColorDisplay::DrawPixel(start_x + 3,start_y + 2, BLACK);
  OledColorDisplay::DrawPixel(start_x + 4,start_y + 2, GREEN);
  OledColorDisplay::DrawPixel(start_x + 0,start_y + 3, GREEN);
  OledColorDisplay::DrawPixel(start_x + 1,start_y + 3, BLACK);
  OledColorDisplay::DrawPixel(start_x + 2,start_y + 3, BLACK);
  OledColorDisplay::DrawPixel(start_x + 3,start_y + 3, BLACK);
  OledColorDisplay::DrawPixel(start_x + 4,start_y + 3, GREEN);
  OledColorDisplay::DrawPixel(start_x + 0,start_y + 4, GREEN);
  OledColorDisplay::DrawPixel(start_x + 1,start_y + 4, BLACK);
  OledColorDisplay::DrawPixel(start_x + 2,start_y + 4, GREEN);
  OledColorDisplay::DrawPixel(start_x + 3,start_y + 4, BLACK);
  OledColorDisplay::DrawPixel(start_x + 4,start_y + 4, GREEN);
  OledColorDisplay::DrawPixel(start_x + 0,start_y + 5, GREEN);
  OledColorDisplay::DrawPixel(start_x + 1,start_y + 5, BLACK);
  OledColorDisplay::DrawPixel(start_x + 2,start_y + 5, GREEN);
  OledColorDisplay::DrawPixel(start_x + 3,start_y + 5, BLACK);
  OledColorDisplay::DrawPixel(start_x + 4,start_y + 5, GREEN);
  OledColorDisplay::DrawPixel(start_x + 0,start_y + 6, GREEN);
  OledColorDisplay::DrawPixel(start_x + 1,start_y + 6, BLACK);
  OledColorDisplay::DrawPixel(start_x + 2,start_y + 6, GREEN);
  OledColorDisplay::DrawPixel(start_x + 3,start_y + 6, BLACK);
  OledColorDisplay::DrawPixel(start_x + 4,start_y + 6, GREEN);
  OledColorDisplay::DrawPixel(start_x + 0,start_y + 7, BLACK);
  OledColorDisplay::DrawPixel(start_x + 1,start_y + 7, GREEN);
  OledColorDisplay::DrawPixel(start_x + 2,start_y + 7, BLACK);
  OledColorDisplay::DrawPixel(start_x + 3,start_y + 7, GREEN);
  OledColorDisplay::DrawPixel(start_x + 4,start_y + 7, BLACK);
} else if ( mydrawchar == "X" ) {
  OledColorDisplay::DrawPixel(start_x + 0,start_y + 0, BLACK);
  OledColorDisplay::DrawPixel(start_x + 1,start_y + 0, BLACK);
  OledColorDisplay::DrawPixel(start_x + 2,start_y + 0, BLACK);
  OledColorDisplay::DrawPixel(start_x + 3,start_y + 0, BLACK);
  OledColorDisplay::DrawPixel(start_x + 4,start_y + 0, BLACK);
  OledColorDisplay::DrawPixel(start_x + 0,start_y + 1, GREEN);
  OledColorDisplay::DrawPixel(start_x + 1,start_y + 1, BLACK);
  OledColorDisplay::DrawPixel(start_x + 2,start_y + 1, BLACK);
  OledColorDisplay::DrawPixel(start_x + 3,start_y + 1, BLACK);
  OledColorDisplay::DrawPixel(start_x + 4,start_y + 1, GREEN);
  OledColorDisplay::DrawPixel(start_x + 0,start_y + 2, GREEN);
  OledColorDisplay::DrawPixel(start_x + 1,start_y + 2, BLACK);
  OledColorDisplay::DrawPixel(start_x + 2,start_y + 2, BLACK);
  OledColorDisplay::DrawPixel(start_x + 3,start_y + 2, BLACK);
  OledColorDisplay::DrawPixel(start_x + 4,start_y + 2, GREEN);
  OledColorDisplay::DrawPixel(start_x + 0,start_y + 3, BLACK);
  OledColorDisplay::DrawPixel(start_x + 1,start_y + 3, GREEN);
  OledColorDisplay::DrawPixel(start_x + 2,start_y + 3, BLACK);
  OledColorDisplay::DrawPixel(start_x + 3,start_y + 3, GREEN);
  OledColorDisplay::DrawPixel(start_x + 4,start_y + 3, BLACK);
  OledColorDisplay::DrawPixel(start_x + 0,start_y + 4, BLACK);
  OledColorDisplay::DrawPixel(start_x + 1,start_y + 4, BLACK);
  OledColorDisplay::DrawPixel(start_x + 2,start_y + 4, GREEN);
  OledColorDisplay::DrawPixel(start_x + 3,start_y + 4, BLACK);
  OledColorDisplay::DrawPixel(start_x + 4,start_y + 4, BLACK);
  OledColorDisplay::DrawPixel(start_x + 0,start_y + 5, BLACK);
  OledColorDisplay::DrawPixel(start_x + 1,start_y + 5, GREEN);
  OledColorDisplay::DrawPixel(start_x + 2,start_y + 5, BLACK);
  OledColorDisplay::DrawPixel(start_x + 3,start_y + 5, GREEN);
  OledColorDisplay::DrawPixel(start_x + 4,start_y + 5, BLACK);
  OledColorDisplay::DrawPixel(start_x + 0,start_y + 6, GREEN);
  OledColorDisplay::DrawPixel(start_x + 1,start_y + 6, BLACK);
  OledColorDisplay::DrawPixel(start_x + 2,start_y + 6, BLACK);
  OledColorDisplay::DrawPixel(start_x + 3,start_y + 6, BLACK);
  OledColorDisplay::DrawPixel(start_x + 4,start_y + 6, GREEN);
  OledColorDisplay::DrawPixel(start_x + 0,start_y + 7, GREEN);
  OledColorDisplay::DrawPixel(start_x + 1,start_y + 7, BLACK);
  OledColorDisplay::DrawPixel(start_x + 2,start_y + 7, BLACK);
  OledColorDisplay::DrawPixel(start_x + 3,start_y + 7, BLACK);
  OledColorDisplay::DrawPixel(start_x + 4,start_y + 7, GREEN);
} else if ( mydrawchar == "Y" ) {
  OledColorDisplay::DrawPixel(start_x + 0,start_y + 0, BLACK);
  OledColorDisplay::DrawPixel(start_x + 1,start_y + 0, BLACK);
  OledColorDisplay::DrawPixel(start_x + 2,start_y + 0, BLACK);
  OledColorDisplay::DrawPixel(start_x + 3,start_y + 0, BLACK);
  OledColorDisplay::DrawPixel(start_x + 4,start_y + 0, BLACK);
  OledColorDisplay::DrawPixel(start_x + 0,start_y + 1, GREEN);
  OledColorDisplay::DrawPixel(start_x + 1,start_y + 1, BLACK);
  OledColorDisplay::DrawPixel(start_x + 2,start_y + 1, BLACK);
  OledColorDisplay::DrawPixel(start_x + 3,start_y + 1, BLACK);
  OledColorDisplay::DrawPixel(start_x + 4,start_y + 1, GREEN);
  OledColorDisplay::DrawPixel(start_x + 0,start_y + 2, GREEN);
  OledColorDisplay::DrawPixel(start_x + 1,start_y + 2, BLACK);
  OledColorDisplay::DrawPixel(start_x + 2,start_y + 2, BLACK);
  OledColorDisplay::DrawPixel(start_x + 3,start_y + 2, BLACK);
  OledColorDisplay::DrawPixel(start_x + 4,start_y + 2, GREEN);
  OledColorDisplay::DrawPixel(start_x + 0,start_y + 3, GREEN);
  OledColorDisplay::DrawPixel(start_x + 1,start_y + 3, BLACK);
  OledColorDisplay::DrawPixel(start_x + 2,start_y + 3, BLACK);
  OledColorDisplay::DrawPixel(start_x + 3,start_y + 3, BLACK);
  OledColorDisplay::DrawPixel(start_x + 4,start_y + 3, GREEN);
  OledColorDisplay::DrawPixel(start_x + 0,start_y + 4, BLACK);
  OledColorDisplay::DrawPixel(start_x + 1,start_y + 4, GREEN);
  OledColorDisplay::DrawPixel(start_x + 2,start_y + 4, BLACK);
  OledColorDisplay::DrawPixel(start_x + 3,start_y + 4, GREEN);
  OledColorDisplay::DrawPixel(start_x + 4,start_y + 4, BLACK);
  OledColorDisplay::DrawPixel(start_x + 0,start_y + 5, BLACK);
  OledColorDisplay::DrawPixel(start_x + 1,start_y + 5, BLACK);
  OledColorDisplay::DrawPixel(start_x + 2,start_y + 5, GREEN);
  OledColorDisplay::DrawPixel(start_x + 3,start_y + 5, BLACK);
  OledColorDisplay::DrawPixel(start_x + 4,start_y + 5, BLACK);
  OledColorDisplay::DrawPixel(start_x + 0,start_y + 6, BLACK);
  OledColorDisplay::DrawPixel(start_x + 1,start_y + 6, BLACK);
  OledColorDisplay::DrawPixel(start_x + 2,start_y + 6, GREEN);
  OledColorDisplay::DrawPixel(start_x + 3,start_y + 6, BLACK);
  OledColorDisplay::DrawPixel(start_x + 4,start_y + 6, BLACK);
  OledColorDisplay::DrawPixel(start_x + 0,start_y + 7, BLACK);
  OledColorDisplay::DrawPixel(start_x + 1,start_y + 7, BLACK);
  OledColorDisplay::DrawPixel(start_x + 2,start_y + 7, GREEN);
  OledColorDisplay::DrawPixel(start_x + 3,start_y + 7, BLACK);
  OledColorDisplay::DrawPixel(start_x + 4,start_y + 7, BLACK);
} else if ( mydrawchar == "Z" ) {
  OledColorDisplay::DrawPixel(start_x + 0,start_y + 0, BLACK);
  OledColorDisplay::DrawPixel(start_x + 1,start_y + 0, BLACK);
  OledColorDisplay::DrawPixel(start_x + 2,start_y + 0, BLACK);
  OledColorDisplay::DrawPixel(start_x + 3,start_y + 0, BLACK);
  OledColorDisplay::DrawPixel(start_x + 4,start_y + 0, BLACK);
  OledColorDisplay::DrawPixel(start_x + 0,start_y + 1, GREEN);
  OledColorDisplay::DrawPixel(start_x + 1,start_y + 1, GREEN);
  OledColorDisplay::DrawPixel(start_x + 2,start_y + 1, GREEN);
  OledColorDisplay::DrawPixel(start_x + 3,start_y + 1, GREEN);
  OledColorDisplay::DrawPixel(start_x + 4,start_y + 1, GREEN);
  OledColorDisplay::DrawPixel(start_x + 0,start_y + 2, BLACK);
  OledColorDisplay::DrawPixel(start_x + 1,start_y + 2, BLACK);
  OledColorDisplay::DrawPixel(start_x + 2,start_y + 2, BLACK);
  OledColorDisplay::DrawPixel(start_x + 3,start_y + 2, BLACK);
  OledColorDisplay::DrawPixel(start_x + 4,start_y + 2, GREEN);
  OledColorDisplay::DrawPixel(start_x + 0,start_y + 3, BLACK);
  OledColorDisplay::DrawPixel(start_x + 1,start_y + 3, BLACK);
  OledColorDisplay::DrawPixel(start_x + 2,start_y + 3, BLACK);
  OledColorDisplay::DrawPixel(start_x + 3,start_y + 3, GREEN);
  OledColorDisplay::DrawPixel(start_x + 4,start_y + 3, BLACK);
  OledColorDisplay::DrawPixel(start_x + 0,start_y + 4, BLACK);
  OledColorDisplay::DrawPixel(start_x + 1,start_y + 4, BLACK);
  OledColorDisplay::DrawPixel(start_x + 2,start_y + 4, GREEN);
  OledColorDisplay::DrawPixel(start_x + 3,start_y + 4, BLACK);
  OledColorDisplay::DrawPixel(start_x + 4,start_y + 4, BLACK);
  OledColorDisplay::DrawPixel(start_x + 0,start_y + 5, BLACK);
  OledColorDisplay::DrawPixel(start_x + 1,start_y + 5, GREEN);
  OledColorDisplay::DrawPixel(start_x + 2,start_y + 5, BLACK);
  OledColorDisplay::DrawPixel(start_x + 3,start_y + 5, BLACK);
  OledColorDisplay::DrawPixel(start_x + 4,start_y + 5, BLACK);
  OledColorDisplay::DrawPixel(start_x + 0,start_y + 6, GREEN);
  OledColorDisplay::DrawPixel(start_x + 1,start_y + 6, BLACK);
  OledColorDisplay::DrawPixel(start_x + 2,start_y + 6, BLACK);
  OledColorDisplay::DrawPixel(start_x + 3,start_y + 6, BLACK);
  OledColorDisplay::DrawPixel(start_x + 4,start_y + 6, BLACK);
  OledColorDisplay::DrawPixel(start_x + 0,start_y + 7, GREEN);
  OledColorDisplay::DrawPixel(start_x + 1,start_y + 7, GREEN);
  OledColorDisplay::DrawPixel(start_x + 2,start_y + 7, GREEN);
  OledColorDisplay::DrawPixel(start_x + 3,start_y + 7, GREEN);
  OledColorDisplay::DrawPixel(start_x + 4,start_y + 7, GREEN);
} else if ( mydrawchar == "a" ) {
  OledColorDisplay::DrawPixel(start_x + 0,start_y + 0, BLACK);
  OledColorDisplay::DrawPixel(start_x + 1,start_y + 0, BLACK);
  OledColorDisplay::DrawPixel(start_x + 2,start_y + 0, BLACK);
  OledColorDisplay::DrawPixel(start_x + 3,start_y + 0, BLACK);
  OledColorDisplay::DrawPixel(start_x + 4,start_y + 0, BLACK);
  OledColorDisplay::DrawPixel(start_x + 0,start_y + 1, BLACK);
  OledColorDisplay::DrawPixel(start_x + 1,start_y + 1, BLACK);
  OledColorDisplay::DrawPixel(start_x + 2,start_y + 1, BLACK);
  OledColorDisplay::DrawPixel(start_x + 3,start_y + 1, BLACK);
  OledColorDisplay::DrawPixel(start_x + 4,start_y + 1, BLACK);
  OledColorDisplay::DrawPixel(start_x + 0,start_y + 2, BLACK);
  OledColorDisplay::DrawPixel(start_x + 1,start_y + 2, BLACK);
  OledColorDisplay::DrawPixel(start_x + 2,start_y + 2, BLACK);
  OledColorDisplay::DrawPixel(start_x + 3,start_y + 2, BLACK);
  OledColorDisplay::DrawPixel(start_x + 4,start_y + 2, BLACK);
  OledColorDisplay::DrawPixel(start_x + 0,start_y + 3, BLACK);
  OledColorDisplay::DrawPixel(start_x + 1,start_y + 3, GREEN);
  OledColorDisplay::DrawPixel(start_x + 2,start_y + 3, GREEN);
  OledColorDisplay::DrawPixel(start_x + 3,start_y + 3, GREEN);
  OledColorDisplay::DrawPixel(start_x + 4,start_y + 3, BLACK);
  OledColorDisplay::DrawPixel(start_x + 0,start_y + 4, GREEN);
  OledColorDisplay::DrawPixel(start_x + 1,start_y + 4, BLACK);
  OledColorDisplay::DrawPixel(start_x + 2,start_y + 4, BLACK);
  OledColorDisplay::DrawPixel(start_x + 3,start_y + 4, GREEN);
  OledColorDisplay::DrawPixel(start_x + 4,start_y + 4, BLACK);
  OledColorDisplay::DrawPixel(start_x + 0,start_y + 5, GREEN);
  OledColorDisplay::DrawPixel(start_x + 1,start_y + 5, BLACK);
  OledColorDisplay::DrawPixel(start_x + 2,start_y + 5, BLACK);
  OledColorDisplay::DrawPixel(start_x + 3,start_y + 5, GREEN);
  OledColorDisplay::DrawPixel(start_x + 4,start_y + 5, BLACK);
  OledColorDisplay::DrawPixel(start_x + 0,start_y + 6, GREEN);
  OledColorDisplay::DrawPixel(start_x + 1,start_y + 6, BLACK);
  OledColorDisplay::DrawPixel(start_x + 2,start_y + 6, BLACK);
  OledColorDisplay::DrawPixel(start_x + 3,start_y + 6, GREEN);
  OledColorDisplay::DrawPixel(start_x + 4,start_y + 6, BLACK);
  OledColorDisplay::DrawPixel(start_x + 0,start_y + 7, BLACK);
  OledColorDisplay::DrawPixel(start_x + 1,start_y + 7, GREEN);
  OledColorDisplay::DrawPixel(start_x + 2,start_y + 7, GREEN);
  OledColorDisplay::DrawPixel(start_x + 3,start_y + 7, BLACK);
  OledColorDisplay::DrawPixel(start_x + 4,start_y + 7, GREEN);
} else if ( mydrawchar == "b" ) {
  OledColorDisplay::DrawPixel(start_x + 0,start_y + 0, BLACK);
  OledColorDisplay::DrawPixel(start_x + 1,start_y + 0, BLACK);
  OledColorDisplay::DrawPixel(start_x + 2,start_y + 0, BLACK);
  OledColorDisplay::DrawPixel(start_x + 3,start_y + 0, BLACK);
  OledColorDisplay::DrawPixel(start_x + 4,start_y + 0, BLACK);
  OledColorDisplay::DrawPixel(start_x + 0,start_y + 1, GREEN);
  OledColorDisplay::DrawPixel(start_x + 1,start_y + 1, BLACK);
  OledColorDisplay::DrawPixel(start_x + 2,start_y + 1, BLACK);
  OledColorDisplay::DrawPixel(start_x + 3,start_y + 1, BLACK);
  OledColorDisplay::DrawPixel(start_x + 4,start_y + 1, BLACK);
  OledColorDisplay::DrawPixel(start_x + 0,start_y + 2, GREEN);
  OledColorDisplay::DrawPixel(start_x + 1,start_y + 2, BLACK);
  OledColorDisplay::DrawPixel(start_x + 2,start_y + 2, BLACK);
  OledColorDisplay::DrawPixel(start_x + 3,start_y + 2, BLACK);
  OledColorDisplay::DrawPixel(start_x + 4,start_y + 2, BLACK);
  OledColorDisplay::DrawPixel(start_x + 0,start_y + 3, GREEN);
  OledColorDisplay::DrawPixel(start_x + 1,start_y + 3, BLACK);
  OledColorDisplay::DrawPixel(start_x + 2,start_y + 3, GREEN);
  OledColorDisplay::DrawPixel(start_x + 3,start_y + 3, GREEN);
  OledColorDisplay::DrawPixel(start_x + 4,start_y + 3, BLACK);
  OledColorDisplay::DrawPixel(start_x + 0,start_y + 4, GREEN);
  OledColorDisplay::DrawPixel(start_x + 1,start_y + 4, GREEN);
  OledColorDisplay::DrawPixel(start_x + 2,start_y + 4, BLACK);
  OledColorDisplay::DrawPixel(start_x + 3,start_y + 4, BLACK);
  OledColorDisplay::DrawPixel(start_x + 4,start_y + 4, GREEN);
  OledColorDisplay::DrawPixel(start_x + 0,start_y + 5, GREEN);
  OledColorDisplay::DrawPixel(start_x + 1,start_y + 5, BLACK);
  OledColorDisplay::DrawPixel(start_x + 2,start_y + 5, BLACK);
  OledColorDisplay::DrawPixel(start_x + 3,start_y + 5, BLACK);
  OledColorDisplay::DrawPixel(start_x + 4,start_y + 5, GREEN);
  OledColorDisplay::DrawPixel(start_x + 0,start_y + 6, GREEN);
  OledColorDisplay::DrawPixel(start_x + 1,start_y + 6, BLACK);
  OledColorDisplay::DrawPixel(start_x + 2,start_y + 6, BLACK);
  OledColorDisplay::DrawPixel(start_x + 3,start_y + 6, BLACK);
  OledColorDisplay::DrawPixel(start_x + 4,start_y + 6, GREEN);
  OledColorDisplay::DrawPixel(start_x + 0,start_y + 7, GREEN);
  OledColorDisplay::DrawPixel(start_x + 1,start_y + 7, GREEN);
  OledColorDisplay::DrawPixel(start_x + 2,start_y + 7, GREEN);
  OledColorDisplay::DrawPixel(start_x + 3,start_y + 7, GREEN);
  OledColorDisplay::DrawPixel(start_x + 4,start_y + 7, BLACK);
} else if ( mydrawchar == "c" ) {
  OledColorDisplay::DrawPixel(start_x + 0,start_y + 0, BLACK);
  OledColorDisplay::DrawPixel(start_x + 1,start_y + 0, BLACK);
  OledColorDisplay::DrawPixel(start_x + 2,start_y + 0, BLACK);
  OledColorDisplay::DrawPixel(start_x + 3,start_y + 0, BLACK);
  OledColorDisplay::DrawPixel(start_x + 4,start_y + 0, BLACK);
  OledColorDisplay::DrawPixel(start_x + 0,start_y + 1, BLACK);
  OledColorDisplay::DrawPixel(start_x + 1,start_y + 1, BLACK);
  OledColorDisplay::DrawPixel(start_x + 2,start_y + 1, BLACK);
  OledColorDisplay::DrawPixel(start_x + 3,start_y + 1, BLACK);
  OledColorDisplay::DrawPixel(start_x + 4,start_y + 1, BLACK);
  OledColorDisplay::DrawPixel(start_x + 0,start_y + 2, BLACK);
  OledColorDisplay::DrawPixel(start_x + 1,start_y + 2, BLACK);
  OledColorDisplay::DrawPixel(start_x + 2,start_y + 2, BLACK);
  OledColorDisplay::DrawPixel(start_x + 3,start_y + 2, BLACK);
  OledColorDisplay::DrawPixel(start_x + 4,start_y + 2, BLACK);
  OledColorDisplay::DrawPixel(start_x + 0,start_y + 3, BLACK);
  OledColorDisplay::DrawPixel(start_x + 1,start_y + 3, GREEN);
  OledColorDisplay::DrawPixel(start_x + 2,start_y + 3, GREEN);
  OledColorDisplay::DrawPixel(start_x + 3,start_y + 3, GREEN);
  OledColorDisplay::DrawPixel(start_x + 4,start_y + 3, BLACK);
  OledColorDisplay::DrawPixel(start_x + 0,start_y + 4, GREEN);
  OledColorDisplay::DrawPixel(start_x + 1,start_y + 4, BLACK);
  OledColorDisplay::DrawPixel(start_x + 2,start_y + 4, BLACK);
  OledColorDisplay::DrawPixel(start_x + 3,start_y + 4, BLACK);
  OledColorDisplay::DrawPixel(start_x + 4,start_y + 4, BLACK);
  OledColorDisplay::DrawPixel(start_x + 0,start_y + 5, GREEN);
  OledColorDisplay::DrawPixel(start_x + 1,start_y + 5, BLACK);
  OledColorDisplay::DrawPixel(start_x + 2,start_y + 5, BLACK);
  OledColorDisplay::DrawPixel(start_x + 3,start_y + 5, BLACK);
  OledColorDisplay::DrawPixel(start_x + 4,start_y + 5, BLACK);
  OledColorDisplay::DrawPixel(start_x + 0,start_y + 6, GREEN);
  OledColorDisplay::DrawPixel(start_x + 1,start_y + 6, BLACK);
  OledColorDisplay::DrawPixel(start_x + 2,start_y + 6, BLACK);
  OledColorDisplay::DrawPixel(start_x + 3,start_y + 6, BLACK);
  OledColorDisplay::DrawPixel(start_x + 4,start_y + 6, GREEN);
  OledColorDisplay::DrawPixel(start_x + 0,start_y + 7, BLACK);
  OledColorDisplay::DrawPixel(start_x + 1,start_y + 7, GREEN);
  OledColorDisplay::DrawPixel(start_x + 2,start_y + 7, GREEN);
  OledColorDisplay::DrawPixel(start_x + 3,start_y + 7, GREEN);
  OledColorDisplay::DrawPixel(start_x + 4,start_y + 7, BLACK);
} else if ( mydrawchar == "d" ) {
  OledColorDisplay::DrawPixel(start_x + 0,start_y + 0, BLACK);
  OledColorDisplay::DrawPixel(start_x + 1,start_y + 0, BLACK);
  OledColorDisplay::DrawPixel(start_x + 2,start_y + 0, BLACK);
  OledColorDisplay::DrawPixel(start_x + 3,start_y + 0, BLACK);
  OledColorDisplay::DrawPixel(start_x + 4,start_y + 0, BLACK);
  OledColorDisplay::DrawPixel(start_x + 0,start_y + 1, BLACK);
  OledColorDisplay::DrawPixel(start_x + 1,start_y + 1, BLACK);
  OledColorDisplay::DrawPixel(start_x + 2,start_y + 1, BLACK);
  OledColorDisplay::DrawPixel(start_x + 3,start_y + 1, BLACK);
  OledColorDisplay::DrawPixel(start_x + 4,start_y + 1, GREEN);
  OledColorDisplay::DrawPixel(start_x + 0,start_y + 2, BLACK);
  OledColorDisplay::DrawPixel(start_x + 1,start_y + 2, BLACK);
  OledColorDisplay::DrawPixel(start_x + 2,start_y + 2, BLACK);
  OledColorDisplay::DrawPixel(start_x + 3,start_y + 2, BLACK);
  OledColorDisplay::DrawPixel(start_x + 4,start_y + 2, GREEN);
  OledColorDisplay::DrawPixel(start_x + 0,start_y + 3, BLACK);
  OledColorDisplay::DrawPixel(start_x + 1,start_y + 3, GREEN);
  OledColorDisplay::DrawPixel(start_x + 2,start_y + 3, GREEN);
  OledColorDisplay::DrawPixel(start_x + 3,start_y + 3, BLACK);
  OledColorDisplay::DrawPixel(start_x + 4,start_y + 3, GREEN);
  OledColorDisplay::DrawPixel(start_x + 0,start_y + 4, GREEN);
  OledColorDisplay::DrawPixel(start_x + 1,start_y + 4, BLACK);
  OledColorDisplay::DrawPixel(start_x + 2,start_y + 4, BLACK);
  OledColorDisplay::DrawPixel(start_x + 3,start_y + 4, GREEN);
  OledColorDisplay::DrawPixel(start_x + 4,start_y + 4, GREEN);
  OledColorDisplay::DrawPixel(start_x + 0,start_y + 5, GREEN);
  OledColorDisplay::DrawPixel(start_x + 1,start_y + 5, BLACK);
  OledColorDisplay::DrawPixel(start_x + 2,start_y + 5, BLACK);
  OledColorDisplay::DrawPixel(start_x + 3,start_y + 5, BLACK);
  OledColorDisplay::DrawPixel(start_x + 4,start_y + 5, GREEN);
  OledColorDisplay::DrawPixel(start_x + 0,start_y + 6, GREEN);
  OledColorDisplay::DrawPixel(start_x + 1,start_y + 6, BLACK);
  OledColorDisplay::DrawPixel(start_x + 2,start_y + 6, BLACK);
  OledColorDisplay::DrawPixel(start_x + 3,start_y + 6, BLACK);
  OledColorDisplay::DrawPixel(start_x + 4,start_y + 6, GREEN);
  OledColorDisplay::DrawPixel(start_x + 0,start_y + 7, BLACK);
  OledColorDisplay::DrawPixel(start_x + 1,start_y + 7, GREEN);
  OledColorDisplay::DrawPixel(start_x + 2,start_y + 7, GREEN);
  OledColorDisplay::DrawPixel(start_x + 3,start_y + 7, GREEN);
  OledColorDisplay::DrawPixel(start_x + 4,start_y + 7, GREEN);
} else if ( mydrawchar == "e" ) {
  OledColorDisplay::DrawPixel(start_x + 0,start_y + 0, BLACK);
  OledColorDisplay::DrawPixel(start_x + 1,start_y + 0, BLACK);
  OledColorDisplay::DrawPixel(start_x + 2,start_y + 0, BLACK);
  OledColorDisplay::DrawPixel(start_x + 3,start_y + 0, BLACK);
  OledColorDisplay::DrawPixel(start_x + 4,start_y + 0, BLACK);
  OledColorDisplay::DrawPixel(start_x + 0,start_y + 1, BLACK);
  OledColorDisplay::DrawPixel(start_x + 1,start_y + 1, BLACK);
  OledColorDisplay::DrawPixel(start_x + 2,start_y + 1, BLACK);
  OledColorDisplay::DrawPixel(start_x + 3,start_y + 1, BLACK);
  OledColorDisplay::DrawPixel(start_x + 4,start_y + 1, BLACK);
  OledColorDisplay::DrawPixel(start_x + 0,start_y + 2, BLACK);
  OledColorDisplay::DrawPixel(start_x + 1,start_y + 2, BLACK);
  OledColorDisplay::DrawPixel(start_x + 2,start_y + 2, BLACK);
  OledColorDisplay::DrawPixel(start_x + 3,start_y + 2, BLACK);
  OledColorDisplay::DrawPixel(start_x + 4,start_y + 2, BLACK);
  OledColorDisplay::DrawPixel(start_x + 0,start_y + 3, BLACK);
  OledColorDisplay::DrawPixel(start_x + 1,start_y + 3, GREEN);
  OledColorDisplay::DrawPixel(start_x + 2,start_y + 3, GREEN);
  OledColorDisplay::DrawPixel(start_x + 3,start_y + 3, GREEN);
  OledColorDisplay::DrawPixel(start_x + 4,start_y + 3, BLACK);
  OledColorDisplay::DrawPixel(start_x + 0,start_y + 4, GREEN);
  OledColorDisplay::DrawPixel(start_x + 1,start_y + 4, BLACK);
  OledColorDisplay::DrawPixel(start_x + 2,start_y + 4, BLACK);
  OledColorDisplay::DrawPixel(start_x + 3,start_y + 4, BLACK);
  OledColorDisplay::DrawPixel(start_x + 4,start_y + 4, GREEN);
  OledColorDisplay::DrawPixel(start_x + 0,start_y + 5, GREEN);
  OledColorDisplay::DrawPixel(start_x + 1,start_y + 5, GREEN);
  OledColorDisplay::DrawPixel(start_x + 2,start_y + 5, GREEN);
  OledColorDisplay::DrawPixel(start_x + 3,start_y + 5, GREEN);
  OledColorDisplay::DrawPixel(start_x + 4,start_y + 5, GREEN);
  OledColorDisplay::DrawPixel(start_x + 0,start_y + 6, GREEN);
  OledColorDisplay::DrawPixel(start_x + 1,start_y + 6, BLACK);
  OledColorDisplay::DrawPixel(start_x + 2,start_y + 6, BLACK);
  OledColorDisplay::DrawPixel(start_x + 3,start_y + 6, BLACK);
  OledColorDisplay::DrawPixel(start_x + 4,start_y + 6, BLACK);
  OledColorDisplay::DrawPixel(start_x + 0,start_y + 7, BLACK);
  OledColorDisplay::DrawPixel(start_x + 1,start_y + 7, GREEN);
  OledColorDisplay::DrawPixel(start_x + 2,start_y + 7, GREEN);
  OledColorDisplay::DrawPixel(start_x + 3,start_y + 7, GREEN);
  OledColorDisplay::DrawPixel(start_x + 4,start_y + 7, BLACK);
} else if ( mydrawchar == "f" ) {
  OledColorDisplay::DrawPixel(start_x + 0,start_y + 0, BLACK);
  OledColorDisplay::DrawPixel(start_x + 1,start_y + 0, BLACK);
  OledColorDisplay::DrawPixel(start_x + 2,start_y + 0, BLACK);
  OledColorDisplay::DrawPixel(start_x + 3,start_y + 0, BLACK);
  OledColorDisplay::DrawPixel(start_x + 4,start_y + 0, BLACK);
  OledColorDisplay::DrawPixel(start_x + 0,start_y + 1, BLACK);
  OledColorDisplay::DrawPixel(start_x + 1,start_y + 1, BLACK);
  OledColorDisplay::DrawPixel(start_x + 2,start_y + 1, GREEN);
  OledColorDisplay::DrawPixel(start_x + 3,start_y + 1, GREEN);
  OledColorDisplay::DrawPixel(start_x + 4,start_y + 1, BLACK);
  OledColorDisplay::DrawPixel(start_x + 0,start_y + 2, BLACK);
  OledColorDisplay::DrawPixel(start_x + 1,start_y + 2, GREEN);
  OledColorDisplay::DrawPixel(start_x + 2,start_y + 2, BLACK);
  OledColorDisplay::DrawPixel(start_x + 3,start_y + 2, BLACK);
  OledColorDisplay::DrawPixel(start_x + 4,start_y + 2, GREEN);
  OledColorDisplay::DrawPixel(start_x + 0,start_y + 3, BLACK);
  OledColorDisplay::DrawPixel(start_x + 1,start_y + 3, GREEN);
  OledColorDisplay::DrawPixel(start_x + 2,start_y + 3, BLACK);
  OledColorDisplay::DrawPixel(start_x + 3,start_y + 3, BLACK);
  OledColorDisplay::DrawPixel(start_x + 4,start_y + 3, BLACK);
  OledColorDisplay::DrawPixel(start_x + 0,start_y + 4, GREEN);
  OledColorDisplay::DrawPixel(start_x + 1,start_y + 4, GREEN);
  OledColorDisplay::DrawPixel(start_x + 2,start_y + 4, GREEN);
  OledColorDisplay::DrawPixel(start_x + 3,start_y + 4, BLACK);
  OledColorDisplay::DrawPixel(start_x + 4,start_y + 4, BLACK);
  OledColorDisplay::DrawPixel(start_x + 0,start_y + 5, BLACK);
  OledColorDisplay::DrawPixel(start_x + 1,start_y + 5, GREEN);
  OledColorDisplay::DrawPixel(start_x + 2,start_y + 5, BLACK);
  OledColorDisplay::DrawPixel(start_x + 3,start_y + 5, BLACK);
  OledColorDisplay::DrawPixel(start_x + 4,start_y + 5, BLACK);
  OledColorDisplay::DrawPixel(start_x + 0,start_y + 6, BLACK);
  OledColorDisplay::DrawPixel(start_x + 1,start_y + 6, GREEN);
  OledColorDisplay::DrawPixel(start_x + 2,start_y + 6, BLACK);
  OledColorDisplay::DrawPixel(start_x + 3,start_y + 6, BLACK);
  OledColorDisplay::DrawPixel(start_x + 4,start_y + 6, BLACK);
  OledColorDisplay::DrawPixel(start_x + 0,start_y + 7, BLACK);
  OledColorDisplay::DrawPixel(start_x + 1,start_y + 7, GREEN);
  OledColorDisplay::DrawPixel(start_x + 2,start_y + 7, BLACK);
  OledColorDisplay::DrawPixel(start_x + 3,start_y + 7, BLACK);
  OledColorDisplay::DrawPixel(start_x + 4,start_y + 7, BLACK);
} else if ( mydrawchar == "g" ) {
  OledColorDisplay::DrawPixel(start_x + 0,start_y + 0, BLACK);
  OledColorDisplay::DrawPixel(start_x + 1,start_y + 0, BLACK);
  OledColorDisplay::DrawPixel(start_x + 2,start_y + 0, BLACK);
  OledColorDisplay::DrawPixel(start_x + 3,start_y + 0, BLACK);
  OledColorDisplay::DrawPixel(start_x + 4,start_y + 0, BLACK);
  OledColorDisplay::DrawPixel(start_x + 0,start_y + 1, BLACK);
  OledColorDisplay::DrawPixel(start_x + 1,start_y + 1, BLACK);
  OledColorDisplay::DrawPixel(start_x + 2,start_y + 1, BLACK);
  OledColorDisplay::DrawPixel(start_x + 3,start_y + 1, BLACK);
  OledColorDisplay::DrawPixel(start_x + 4,start_y + 1, BLACK);
  OledColorDisplay::DrawPixel(start_x + 0,start_y + 2, BLACK);
  OledColorDisplay::DrawPixel(start_x + 1,start_y + 2, GREEN);
  OledColorDisplay::DrawPixel(start_x + 2,start_y + 2, GREEN);
  OledColorDisplay::DrawPixel(start_x + 3,start_y + 2, GREEN);
  OledColorDisplay::DrawPixel(start_x + 4,start_y + 2, GREEN);
  OledColorDisplay::DrawPixel(start_x + 0,start_y + 3, GREEN);
  OledColorDisplay::DrawPixel(start_x + 1,start_y + 3, BLACK);
  OledColorDisplay::DrawPixel(start_x + 2,start_y + 3, BLACK);
  OledColorDisplay::DrawPixel(start_x + 3,start_y + 3, BLACK);
  OledColorDisplay::DrawPixel(start_x + 4,start_y + 3, GREEN);
  OledColorDisplay::DrawPixel(start_x + 0,start_y + 4, GREEN);
  OledColorDisplay::DrawPixel(start_x + 1,start_y + 4, BLACK);
  OledColorDisplay::DrawPixel(start_x + 2,start_y + 4, BLACK);
  OledColorDisplay::DrawPixel(start_x + 3,start_y + 4, BLACK);
  OledColorDisplay::DrawPixel(start_x + 4,start_y + 4, GREEN);
  OledColorDisplay::DrawPixel(start_x + 0,start_y + 5, BLACK);
  OledColorDisplay::DrawPixel(start_x + 1,start_y + 5, GREEN);
  OledColorDisplay::DrawPixel(start_x + 2,start_y + 5, GREEN);
  OledColorDisplay::DrawPixel(start_x + 3,start_y + 5, GREEN);
  OledColorDisplay::DrawPixel(start_x + 4,start_y + 5, GREEN);
  OledColorDisplay::DrawPixel(start_x + 0,start_y + 6, BLACK);
  OledColorDisplay::DrawPixel(start_x + 1,start_y + 6, BLACK);
  OledColorDisplay::DrawPixel(start_x + 2,start_y + 6, BLACK);
  OledColorDisplay::DrawPixel(start_x + 3,start_y + 6, BLACK);
  OledColorDisplay::DrawPixel(start_x + 4,start_y + 6, GREEN);
  OledColorDisplay::DrawPixel(start_x + 0,start_y + 7, BLACK);
  OledColorDisplay::DrawPixel(start_x + 1,start_y + 7, GREEN);
  OledColorDisplay::DrawPixel(start_x + 2,start_y + 7, GREEN);
  OledColorDisplay::DrawPixel(start_x + 3,start_y + 7, GREEN);
  OledColorDisplay::DrawPixel(start_x + 4,start_y + 7, BLACK);
} else if ( mydrawchar == "h" ) {
  OledColorDisplay::DrawPixel(start_x + 0,start_y + 0, BLACK);
  OledColorDisplay::DrawPixel(start_x + 1,start_y + 0, BLACK);
  OledColorDisplay::DrawPixel(start_x + 2,start_y + 0, BLACK);
  OledColorDisplay::DrawPixel(start_x + 3,start_y + 0, BLACK);
  OledColorDisplay::DrawPixel(start_x + 4,start_y + 0, BLACK);
  OledColorDisplay::DrawPixel(start_x + 0,start_y + 1, GREEN);
  OledColorDisplay::DrawPixel(start_x + 1,start_y + 1, BLACK);
  OledColorDisplay::DrawPixel(start_x + 2,start_y + 1, BLACK);
  OledColorDisplay::DrawPixel(start_x + 3,start_y + 1, BLACK);
  OledColorDisplay::DrawPixel(start_x + 4,start_y + 1, BLACK);
  OledColorDisplay::DrawPixel(start_x + 0,start_y + 2, GREEN);
  OledColorDisplay::DrawPixel(start_x + 1,start_y + 2, BLACK);
  OledColorDisplay::DrawPixel(start_x + 2,start_y + 2, BLACK);
  OledColorDisplay::DrawPixel(start_x + 3,start_y + 2, BLACK);
  OledColorDisplay::DrawPixel(start_x + 4,start_y + 2, BLACK);
  OledColorDisplay::DrawPixel(start_x + 0,start_y + 3, GREEN);
  OledColorDisplay::DrawPixel(start_x + 1,start_y + 3, BLACK);
  OledColorDisplay::DrawPixel(start_x + 2,start_y + 3, GREEN);
  OledColorDisplay::DrawPixel(start_x + 3,start_y + 3, GREEN);
  OledColorDisplay::DrawPixel(start_x + 4,start_y + 3, BLACK);
  OledColorDisplay::DrawPixel(start_x + 0,start_y + 4, GREEN);
  OledColorDisplay::DrawPixel(start_x + 1,start_y + 4, GREEN);
  OledColorDisplay::DrawPixel(start_x + 2,start_y + 4, BLACK);
  OledColorDisplay::DrawPixel(start_x + 3,start_y + 4, BLACK);
  OledColorDisplay::DrawPixel(start_x + 4,start_y + 4, GREEN);
  OledColorDisplay::DrawPixel(start_x + 0,start_y + 5, GREEN);
  OledColorDisplay::DrawPixel(start_x + 1,start_y + 5, BLACK);
  OledColorDisplay::DrawPixel(start_x + 2,start_y + 5, BLACK);
  OledColorDisplay::DrawPixel(start_x + 3,start_y + 5, BLACK);
  OledColorDisplay::DrawPixel(start_x + 4,start_y + 5, GREEN);
  OledColorDisplay::DrawPixel(start_x + 0,start_y + 6, GREEN);
  OledColorDisplay::DrawPixel(start_x + 1,start_y + 6, BLACK);
  OledColorDisplay::DrawPixel(start_x + 2,start_y + 6, BLACK);
  OledColorDisplay::DrawPixel(start_x + 3,start_y + 6, BLACK);
  OledColorDisplay::DrawPixel(start_x + 4,start_y + 6, GREEN);
  OledColorDisplay::DrawPixel(start_x + 0,start_y + 7, GREEN);
  OledColorDisplay::DrawPixel(start_x + 1,start_y + 7, BLACK);
  OledColorDisplay::DrawPixel(start_x + 2,start_y + 7, BLACK);
  OledColorDisplay::DrawPixel(start_x + 3,start_y + 7, BLACK);
  OledColorDisplay::DrawPixel(start_x + 4,start_y + 7, GREEN);
} else if ( mydrawchar == "i" ) {
  OledColorDisplay::DrawPixel(start_x + 0,start_y + 0, BLACK);
  OledColorDisplay::DrawPixel(start_x + 1,start_y + 0, BLACK);
  OledColorDisplay::DrawPixel(start_x + 2,start_y + 0, BLACK);
  OledColorDisplay::DrawPixel(start_x + 3,start_y + 0, BLACK);
  OledColorDisplay::DrawPixel(start_x + 4,start_y + 0, BLACK);
  OledColorDisplay::DrawPixel(start_x + 0,start_y + 1, BLACK);
  OledColorDisplay::DrawPixel(start_x + 1,start_y + 1, BLACK);
  OledColorDisplay::DrawPixel(start_x + 2,start_y + 1, GREEN);
  OledColorDisplay::DrawPixel(start_x + 3,start_y + 1, BLACK);
  OledColorDisplay::DrawPixel(start_x + 4,start_y + 1, BLACK);
  OledColorDisplay::DrawPixel(start_x + 0,start_y + 2, BLACK);
  OledColorDisplay::DrawPixel(start_x + 1,start_y + 2, BLACK);
  OledColorDisplay::DrawPixel(start_x + 2,start_y + 2, BLACK);
  OledColorDisplay::DrawPixel(start_x + 3,start_y + 2, BLACK);
  OledColorDisplay::DrawPixel(start_x + 4,start_y + 2, BLACK);
  OledColorDisplay::DrawPixel(start_x + 0,start_y + 3, BLACK);
  OledColorDisplay::DrawPixel(start_x + 1,start_y + 3, GREEN);
  OledColorDisplay::DrawPixel(start_x + 2,start_y + 3, GREEN);
  OledColorDisplay::DrawPixel(start_x + 3,start_y + 3, BLACK);
  OledColorDisplay::DrawPixel(start_x + 4,start_y + 3, BLACK);
  OledColorDisplay::DrawPixel(start_x + 0,start_y + 4, BLACK);
  OledColorDisplay::DrawPixel(start_x + 1,start_y + 4, BLACK);
  OledColorDisplay::DrawPixel(start_x + 2,start_y + 4, GREEN);
  OledColorDisplay::DrawPixel(start_x + 3,start_y + 4, BLACK);
  OledColorDisplay::DrawPixel(start_x + 4,start_y + 4, BLACK);
  OledColorDisplay::DrawPixel(start_x + 0,start_y + 5, BLACK);
  OledColorDisplay::DrawPixel(start_x + 1,start_y + 5, BLACK);
  OledColorDisplay::DrawPixel(start_x + 2,start_y + 5, GREEN);
  OledColorDisplay::DrawPixel(start_x + 3,start_y + 5, BLACK);
  OledColorDisplay::DrawPixel(start_x + 4,start_y + 5, BLACK);
  OledColorDisplay::DrawPixel(start_x + 0,start_y + 6, BLACK);
  OledColorDisplay::DrawPixel(start_x + 1,start_y + 6, BLACK);
  OledColorDisplay::DrawPixel(start_x + 2,start_y + 6, GREEN);
  OledColorDisplay::DrawPixel(start_x + 3,start_y + 6, BLACK);
  OledColorDisplay::DrawPixel(start_x + 4,start_y + 6, BLACK);
  OledColorDisplay::DrawPixel(start_x + 0,start_y + 7, BLACK);
  OledColorDisplay::DrawPixel(start_x + 1,start_y + 7, GREEN);
  OledColorDisplay::DrawPixel(start_x + 2,start_y + 7, GREEN);
  OledColorDisplay::DrawPixel(start_x + 3,start_y + 7, GREEN);
  OledColorDisplay::DrawPixel(start_x + 4,start_y + 7, BLACK);
} else if ( mydrawchar == "j" ) {
  OledColorDisplay::DrawPixel(start_x + 0,start_y + 0, BLACK);
  OledColorDisplay::DrawPixel(start_x + 1,start_y + 0, BLACK);
  OledColorDisplay::DrawPixel(start_x + 2,start_y + 0, BLACK);
  OledColorDisplay::DrawPixel(start_x + 3,start_y + 0, BLACK);
  OledColorDisplay::DrawPixel(start_x + 4,start_y + 0, BLACK);
  OledColorDisplay::DrawPixel(start_x + 0,start_y + 1, BLACK);
  OledColorDisplay::DrawPixel(start_x + 1,start_y + 1, BLACK);
  OledColorDisplay::DrawPixel(start_x + 2,start_y + 1, BLACK);
  OledColorDisplay::DrawPixel(start_x + 3,start_y + 1, GREEN);
  OledColorDisplay::DrawPixel(start_x + 4,start_y + 1, BLACK);
  OledColorDisplay::DrawPixel(start_x + 0,start_y + 2, BLACK);
  OledColorDisplay::DrawPixel(start_x + 1,start_y + 2, BLACK);
  OledColorDisplay::DrawPixel(start_x + 2,start_y + 2, BLACK);
  OledColorDisplay::DrawPixel(start_x + 3,start_y + 2, BLACK);
  OledColorDisplay::DrawPixel(start_x + 4,start_y + 2, BLACK);
  OledColorDisplay::DrawPixel(start_x + 0,start_y + 3, BLACK);
  OledColorDisplay::DrawPixel(start_x + 1,start_y + 3, BLACK);
  OledColorDisplay::DrawPixel(start_x + 2,start_y + 3, BLACK);
  OledColorDisplay::DrawPixel(start_x + 3,start_y + 3, GREEN);
  OledColorDisplay::DrawPixel(start_x + 4,start_y + 3, BLACK);
  OledColorDisplay::DrawPixel(start_x + 0,start_y + 4, BLACK);
  OledColorDisplay::DrawPixel(start_x + 1,start_y + 4, BLACK);
  OledColorDisplay::DrawPixel(start_x + 2,start_y + 4, BLACK);
  OledColorDisplay::DrawPixel(start_x + 3,start_y + 4, GREEN);
  OledColorDisplay::DrawPixel(start_x + 4,start_y + 4, BLACK);
  OledColorDisplay::DrawPixel(start_x + 0,start_y + 5, BLACK);
  OledColorDisplay::DrawPixel(start_x + 1,start_y + 5, BLACK);
  OledColorDisplay::DrawPixel(start_x + 2,start_y + 5, BLACK);
  OledColorDisplay::DrawPixel(start_x + 3,start_y + 5, GREEN);
  OledColorDisplay::DrawPixel(start_x + 4,start_y + 5, BLACK);
  OledColorDisplay::DrawPixel(start_x + 0,start_y + 6, GREEN);
  OledColorDisplay::DrawPixel(start_x + 1,start_y + 6, BLACK);
  OledColorDisplay::DrawPixel(start_x + 2,start_y + 6, BLACK);
  OledColorDisplay::DrawPixel(start_x + 3,start_y + 6, GREEN);
  OledColorDisplay::DrawPixel(start_x + 4,start_y + 6, BLACK);
  OledColorDisplay::DrawPixel(start_x + 0,start_y + 7, BLACK);
  OledColorDisplay::DrawPixel(start_x + 1,start_y + 7, GREEN);
  OledColorDisplay::DrawPixel(start_x + 2,start_y + 7, GREEN);
  OledColorDisplay::DrawPixel(start_x + 3,start_y + 7, BLACK);
  OledColorDisplay::DrawPixel(start_x + 4,start_y + 7, BLACK);
} else if ( mydrawchar == "k" ) {
  OledColorDisplay::DrawPixel(start_x + 0,start_y + 0, BLACK);
  OledColorDisplay::DrawPixel(start_x + 1,start_y + 0, BLACK);
  OledColorDisplay::DrawPixel(start_x + 2,start_y + 0, BLACK);
  OledColorDisplay::DrawPixel(start_x + 3,start_y + 0, BLACK);
  OledColorDisplay::DrawPixel(start_x + 4,start_y + 0, BLACK);
  OledColorDisplay::DrawPixel(start_x + 0,start_y + 1, GREEN);
  OledColorDisplay::DrawPixel(start_x + 1,start_y + 1, BLACK);
  OledColorDisplay::DrawPixel(start_x + 2,start_y + 1, BLACK);
  OledColorDisplay::DrawPixel(start_x + 3,start_y + 1, BLACK);
  OledColorDisplay::DrawPixel(start_x + 4,start_y + 1, BLACK);
  OledColorDisplay::DrawPixel(start_x + 0,start_y + 2, GREEN);
  OledColorDisplay::DrawPixel(start_x + 1,start_y + 2, BLACK);
  OledColorDisplay::DrawPixel(start_x + 2,start_y + 2, BLACK);
  OledColorDisplay::DrawPixel(start_x + 3,start_y + 2, BLACK);
  OledColorDisplay::DrawPixel(start_x + 4,start_y + 2, BLACK);
  OledColorDisplay::DrawPixel(start_x + 0,start_y + 3, GREEN);
  OledColorDisplay::DrawPixel(start_x + 1,start_y + 3, BLACK);
  OledColorDisplay::DrawPixel(start_x + 2,start_y + 3, BLACK);
  OledColorDisplay::DrawPixel(start_x + 3,start_y + 3, GREEN);
  OledColorDisplay::DrawPixel(start_x + 4,start_y + 3, BLACK);
  OledColorDisplay::DrawPixel(start_x + 0,start_y + 4, GREEN);
  OledColorDisplay::DrawPixel(start_x + 1,start_y + 4, BLACK);
  OledColorDisplay::DrawPixel(start_x + 2,start_y + 4, GREEN);
  OledColorDisplay::DrawPixel(start_x + 3,start_y + 4, BLACK);
  OledColorDisplay::DrawPixel(start_x + 4,start_y + 4, BLACK);
  OledColorDisplay::DrawPixel(start_x + 0,start_y + 5, GREEN);
  OledColorDisplay::DrawPixel(start_x + 1,start_y + 5, GREEN);
  OledColorDisplay::DrawPixel(start_x + 2,start_y + 5, BLACK);
  OledColorDisplay::DrawPixel(start_x + 3,start_y + 5, BLACK);
  OledColorDisplay::DrawPixel(start_x + 4,start_y + 5, BLACK);
  OledColorDisplay::DrawPixel(start_x + 0,start_y + 6, GREEN);
  OledColorDisplay::DrawPixel(start_x + 1,start_y + 6, BLACK);
  OledColorDisplay::DrawPixel(start_x + 2,start_y + 6, GREEN);
  OledColorDisplay::DrawPixel(start_x + 3,start_y + 6, BLACK);
  OledColorDisplay::DrawPixel(start_x + 4,start_y + 6, BLACK);
  OledColorDisplay::DrawPixel(start_x + 0,start_y + 7, GREEN);
  OledColorDisplay::DrawPixel(start_x + 1,start_y + 7, BLACK);
  OledColorDisplay::DrawPixel(start_x + 2,start_y + 7, BLACK);
  OledColorDisplay::DrawPixel(start_x + 3,start_y + 7, GREEN);
  OledColorDisplay::DrawPixel(start_x + 4,start_y + 7, BLACK);
} else if ( mydrawchar == "l" ) {
  OledColorDisplay::DrawPixel(start_x + 0,start_y + 0, BLACK);
  OledColorDisplay::DrawPixel(start_x + 1,start_y + 0, BLACK);
  OledColorDisplay::DrawPixel(start_x + 2,start_y + 0, BLACK);
  OledColorDisplay::DrawPixel(start_x + 3,start_y + 0, BLACK);
  OledColorDisplay::DrawPixel(start_x + 4,start_y + 0, BLACK);
  OledColorDisplay::DrawPixel(start_x + 0,start_y + 1, BLACK);
  OledColorDisplay::DrawPixel(start_x + 1,start_y + 1, GREEN);
  OledColorDisplay::DrawPixel(start_x + 2,start_y + 1, GREEN);
  OledColorDisplay::DrawPixel(start_x + 3,start_y + 1, BLACK);
  OledColorDisplay::DrawPixel(start_x + 4,start_y + 1, BLACK);
  OledColorDisplay::DrawPixel(start_x + 0,start_y + 2, BLACK);
  OledColorDisplay::DrawPixel(start_x + 1,start_y + 2, BLACK);
  OledColorDisplay::DrawPixel(start_x + 2,start_y + 2, GREEN);
  OledColorDisplay::DrawPixel(start_x + 3,start_y + 2, BLACK);
  OledColorDisplay::DrawPixel(start_x + 4,start_y + 2, BLACK);
  OledColorDisplay::DrawPixel(start_x + 0,start_y + 3, BLACK);
  OledColorDisplay::DrawPixel(start_x + 1,start_y + 3, BLACK);
  OledColorDisplay::DrawPixel(start_x + 2,start_y + 3, GREEN);
  OledColorDisplay::DrawPixel(start_x + 3,start_y + 3, BLACK);
  OledColorDisplay::DrawPixel(start_x + 4,start_y + 3, BLACK);
  OledColorDisplay::DrawPixel(start_x + 0,start_y + 4, BLACK);
  OledColorDisplay::DrawPixel(start_x + 1,start_y + 4, BLACK);
  OledColorDisplay::DrawPixel(start_x + 2,start_y + 4, GREEN);
  OledColorDisplay::DrawPixel(start_x + 3,start_y + 4, BLACK);
  OledColorDisplay::DrawPixel(start_x + 4,start_y + 4, BLACK);
  OledColorDisplay::DrawPixel(start_x + 0,start_y + 5, BLACK);
  OledColorDisplay::DrawPixel(start_x + 1,start_y + 5, BLACK);
  OledColorDisplay::DrawPixel(start_x + 2,start_y + 5, GREEN);
  OledColorDisplay::DrawPixel(start_x + 3,start_y + 5, BLACK);
  OledColorDisplay::DrawPixel(start_x + 4,start_y + 5, BLACK);
  OledColorDisplay::DrawPixel(start_x + 0,start_y + 6, BLACK);
  OledColorDisplay::DrawPixel(start_x + 1,start_y + 6, BLACK);
  OledColorDisplay::DrawPixel(start_x + 2,start_y + 6, GREEN);
  OledColorDisplay::DrawPixel(start_x + 3,start_y + 6, BLACK);
  OledColorDisplay::DrawPixel(start_x + 4,start_y + 6, BLACK);
  OledColorDisplay::DrawPixel(start_x + 0,start_y + 7, BLACK);
  OledColorDisplay::DrawPixel(start_x + 1,start_y + 7, GREEN);
  OledColorDisplay::DrawPixel(start_x + 2,start_y + 7, GREEN);
  OledColorDisplay::DrawPixel(start_x + 3,start_y + 7, GREEN);
  OledColorDisplay::DrawPixel(start_x + 4,start_y + 7, BLACK);
} else if ( mydrawchar == "m" ) {
  OledColorDisplay::DrawPixel(start_x + 0,start_y + 0, BLACK);
  OledColorDisplay::DrawPixel(start_x + 1,start_y + 0, BLACK);
  OledColorDisplay::DrawPixel(start_x + 2,start_y + 0, BLACK);
  OledColorDisplay::DrawPixel(start_x + 3,start_y + 0, BLACK);
  OledColorDisplay::DrawPixel(start_x + 4,start_y + 0, BLACK);
  OledColorDisplay::DrawPixel(start_x + 0,start_y + 1, BLACK);
  OledColorDisplay::DrawPixel(start_x + 1,start_y + 1, BLACK);
  OledColorDisplay::DrawPixel(start_x + 2,start_y + 1, BLACK);
  OledColorDisplay::DrawPixel(start_x + 3,start_y + 1, BLACK);
  OledColorDisplay::DrawPixel(start_x + 4,start_y + 1, BLACK);
  OledColorDisplay::DrawPixel(start_x + 0,start_y + 2, BLACK);
  OledColorDisplay::DrawPixel(start_x + 1,start_y + 2, BLACK);
  OledColorDisplay::DrawPixel(start_x + 2,start_y + 2, BLACK);
  OledColorDisplay::DrawPixel(start_x + 3,start_y + 2, BLACK);
  OledColorDisplay::DrawPixel(start_x + 4,start_y + 2, BLACK);
  OledColorDisplay::DrawPixel(start_x + 0,start_y + 3, GREEN);
  OledColorDisplay::DrawPixel(start_x + 1,start_y + 3, GREEN);
  OledColorDisplay::DrawPixel(start_x + 2,start_y + 3, BLACK);
  OledColorDisplay::DrawPixel(start_x + 3,start_y + 3, GREEN);
  OledColorDisplay::DrawPixel(start_x + 4,start_y + 3, BLACK);
  OledColorDisplay::DrawPixel(start_x + 0,start_y + 4, GREEN);
  OledColorDisplay::DrawPixel(start_x + 1,start_y + 4, BLACK);
  OledColorDisplay::DrawPixel(start_x + 2,start_y + 4, GREEN);
  OledColorDisplay::DrawPixel(start_x + 3,start_y + 4, BLACK);
  OledColorDisplay::DrawPixel(start_x + 4,start_y + 4, GREEN);
  OledColorDisplay::DrawPixel(start_x + 0,start_y + 5, GREEN);
  OledColorDisplay::DrawPixel(start_x + 1,start_y + 5, BLACK);
  OledColorDisplay::DrawPixel(start_x + 2,start_y + 5, GREEN);
  OledColorDisplay::DrawPixel(start_x + 3,start_y + 5, BLACK);
  OledColorDisplay::DrawPixel(start_x + 4,start_y + 5, GREEN);
  OledColorDisplay::DrawPixel(start_x + 0,start_y + 6, GREEN);
  OledColorDisplay::DrawPixel(start_x + 1,start_y + 6, BLACK);
  OledColorDisplay::DrawPixel(start_x + 2,start_y + 6, BLACK);
  OledColorDisplay::DrawPixel(start_x + 3,start_y + 6, BLACK);
  OledColorDisplay::DrawPixel(start_x + 4,start_y + 6, GREEN);
  OledColorDisplay::DrawPixel(start_x + 0,start_y + 7, GREEN);
  OledColorDisplay::DrawPixel(start_x + 1,start_y + 7, BLACK);
  OledColorDisplay::DrawPixel(start_x + 2,start_y + 7, BLACK);
  OledColorDisplay::DrawPixel(start_x + 3,start_y + 7, BLACK);
  OledColorDisplay::DrawPixel(start_x + 4,start_y + 7, GREEN);
} else if ( mydrawchar == "n" ) {
  OledColorDisplay::DrawPixel(start_x + 0,start_y + 0, BLACK);
  OledColorDisplay::DrawPixel(start_x + 1,start_y + 0, BLACK);
  OledColorDisplay::DrawPixel(start_x + 2,start_y + 0, BLACK);
  OledColorDisplay::DrawPixel(start_x + 3,start_y + 0, BLACK);
  OledColorDisplay::DrawPixel(start_x + 4,start_y + 0, BLACK);
  OledColorDisplay::DrawPixel(start_x + 0,start_y + 1, BLACK);
  OledColorDisplay::DrawPixel(start_x + 1,start_y + 1, BLACK);
  OledColorDisplay::DrawPixel(start_x + 2,start_y + 1, BLACK);
  OledColorDisplay::DrawPixel(start_x + 3,start_y + 1, BLACK);
  OledColorDisplay::DrawPixel(start_x + 4,start_y + 1, BLACK);
  OledColorDisplay::DrawPixel(start_x + 0,start_y + 2, BLACK);
  OledColorDisplay::DrawPixel(start_x + 1,start_y + 2, BLACK);
  OledColorDisplay::DrawPixel(start_x + 2,start_y + 2, BLACK);
  OledColorDisplay::DrawPixel(start_x + 3,start_y + 2, BLACK);
  OledColorDisplay::DrawPixel(start_x + 4,start_y + 2, BLACK);
  OledColorDisplay::DrawPixel(start_x + 0,start_y + 3, GREEN);
  OledColorDisplay::DrawPixel(start_x + 1,start_y + 3, BLACK);
  OledColorDisplay::DrawPixel(start_x + 2,start_y + 3, GREEN);
  OledColorDisplay::DrawPixel(start_x + 3,start_y + 3, GREEN);
  OledColorDisplay::DrawPixel(start_x + 4,start_y + 3, BLACK);
  OledColorDisplay::DrawPixel(start_x + 0,start_y + 4, GREEN);
  OledColorDisplay::DrawPixel(start_x + 1,start_y + 4, GREEN);
  OledColorDisplay::DrawPixel(start_x + 2,start_y + 4, BLACK);
  OledColorDisplay::DrawPixel(start_x + 3,start_y + 4, BLACK);
  OledColorDisplay::DrawPixel(start_x + 4,start_y + 4, GREEN);
  OledColorDisplay::DrawPixel(start_x + 0,start_y + 5, GREEN);
  OledColorDisplay::DrawPixel(start_x + 1,start_y + 5, BLACK);
  OledColorDisplay::DrawPixel(start_x + 2,start_y + 5, BLACK);
  OledColorDisplay::DrawPixel(start_x + 3,start_y + 5, BLACK);
  OledColorDisplay::DrawPixel(start_x + 4,start_y + 5, GREEN);
  OledColorDisplay::DrawPixel(start_x + 0,start_y + 6, GREEN);
  OledColorDisplay::DrawPixel(start_x + 1,start_y + 6, BLACK);
  OledColorDisplay::DrawPixel(start_x + 2,start_y + 6, BLACK);
  OledColorDisplay::DrawPixel(start_x + 3,start_y + 6, BLACK);
  OledColorDisplay::DrawPixel(start_x + 4,start_y + 6, GREEN);
  OledColorDisplay::DrawPixel(start_x + 0,start_y + 7, GREEN);
  OledColorDisplay::DrawPixel(start_x + 1,start_y + 7, BLACK);
  OledColorDisplay::DrawPixel(start_x + 2,start_y + 7, BLACK);
  OledColorDisplay::DrawPixel(start_x + 3,start_y + 7, BLACK);
  OledColorDisplay::DrawPixel(start_x + 4,start_y + 7, GREEN);
} else if ( mydrawchar == "o" ) {
  OledColorDisplay::DrawPixel(start_x + 0,start_y + 0, BLACK);
  OledColorDisplay::DrawPixel(start_x + 1,start_y + 0, BLACK);
  OledColorDisplay::DrawPixel(start_x + 2,start_y + 0, BLACK);
  OledColorDisplay::DrawPixel(start_x + 3,start_y + 0, BLACK);
  OledColorDisplay::DrawPixel(start_x + 4,start_y + 0, BLACK);
  OledColorDisplay::DrawPixel(start_x + 0,start_y + 1, BLACK);
  OledColorDisplay::DrawPixel(start_x + 1,start_y + 1, BLACK);
  OledColorDisplay::DrawPixel(start_x + 2,start_y + 1, BLACK);
  OledColorDisplay::DrawPixel(start_x + 3,start_y + 1, BLACK);
  OledColorDisplay::DrawPixel(start_x + 4,start_y + 1, BLACK);
  OledColorDisplay::DrawPixel(start_x + 0,start_y + 2, BLACK);
  OledColorDisplay::DrawPixel(start_x + 1,start_y + 2, BLACK);
  OledColorDisplay::DrawPixel(start_x + 2,start_y + 2, BLACK);
  OledColorDisplay::DrawPixel(start_x + 3,start_y + 2, BLACK);
  OledColorDisplay::DrawPixel(start_x + 4,start_y + 2, BLACK);
  OledColorDisplay::DrawPixel(start_x + 0,start_y + 3, BLACK);
  OledColorDisplay::DrawPixel(start_x + 1,start_y + 3, GREEN);
  OledColorDisplay::DrawPixel(start_x + 2,start_y + 3, GREEN);
  OledColorDisplay::DrawPixel(start_x + 3,start_y + 3, GREEN);
  OledColorDisplay::DrawPixel(start_x + 4,start_y + 3, BLACK);
  OledColorDisplay::DrawPixel(start_x + 0,start_y + 4, GREEN);
  OledColorDisplay::DrawPixel(start_x + 1,start_y + 4, BLACK);
  OledColorDisplay::DrawPixel(start_x + 2,start_y + 4, BLACK);
  OledColorDisplay::DrawPixel(start_x + 3,start_y + 4, BLACK);
  OledColorDisplay::DrawPixel(start_x + 4,start_y + 4, GREEN);
  OledColorDisplay::DrawPixel(start_x + 0,start_y + 5, GREEN);
  OledColorDisplay::DrawPixel(start_x + 1,start_y + 5, BLACK);
  OledColorDisplay::DrawPixel(start_x + 2,start_y + 5, BLACK);
  OledColorDisplay::DrawPixel(start_x + 3,start_y + 5, BLACK);
  OledColorDisplay::DrawPixel(start_x + 4,start_y + 5, GREEN);
  OledColorDisplay::DrawPixel(start_x + 0,start_y + 6, GREEN);
  OledColorDisplay::DrawPixel(start_x + 1,start_y + 6, BLACK);
  OledColorDisplay::DrawPixel(start_x + 2,start_y + 6, BLACK);
  OledColorDisplay::DrawPixel(start_x + 3,start_y + 6, BLACK);
  OledColorDisplay::DrawPixel(start_x + 4,start_y + 6, GREEN);
  OledColorDisplay::DrawPixel(start_x + 0,start_y + 7, BLACK);
  OledColorDisplay::DrawPixel(start_x + 1,start_y + 7, GREEN);
  OledColorDisplay::DrawPixel(start_x + 2,start_y + 7, GREEN);
  OledColorDisplay::DrawPixel(start_x + 3,start_y + 7, GREEN);
  OledColorDisplay::DrawPixel(start_x + 4,start_y + 7, BLACK);
} else if ( mydrawchar == "p" ) {
  OledColorDisplay::DrawPixel(start_x + 0,start_y + 0, BLACK);
  OledColorDisplay::DrawPixel(start_x + 1,start_y + 0, BLACK);
  OledColorDisplay::DrawPixel(start_x + 2,start_y + 0, BLACK);
  OledColorDisplay::DrawPixel(start_x + 3,start_y + 0, BLACK);
  OledColorDisplay::DrawPixel(start_x + 4,start_y + 0, BLACK);
  OledColorDisplay::DrawPixel(start_x + 0,start_y + 1, BLACK);
  OledColorDisplay::DrawPixel(start_x + 1,start_y + 1, BLACK);
  OledColorDisplay::DrawPixel(start_x + 2,start_y + 1, BLACK);
  OledColorDisplay::DrawPixel(start_x + 3,start_y + 1, BLACK);
  OledColorDisplay::DrawPixel(start_x + 4,start_y + 1, BLACK);
  OledColorDisplay::DrawPixel(start_x + 0,start_y + 2, BLACK);
  OledColorDisplay::DrawPixel(start_x + 1,start_y + 2, BLACK);
  OledColorDisplay::DrawPixel(start_x + 2,start_y + 2, BLACK);
  OledColorDisplay::DrawPixel(start_x + 3,start_y + 2, BLACK);
  OledColorDisplay::DrawPixel(start_x + 4,start_y + 2, BLACK);
  OledColorDisplay::DrawPixel(start_x + 0,start_y + 3, GREEN);
  OledColorDisplay::DrawPixel(start_x + 1,start_y + 3, GREEN);
  OledColorDisplay::DrawPixel(start_x + 2,start_y + 3, GREEN);
  OledColorDisplay::DrawPixel(start_x + 3,start_y + 3, GREEN);
  OledColorDisplay::DrawPixel(start_x + 4,start_y + 3, BLACK);
  OledColorDisplay::DrawPixel(start_x + 0,start_y + 4, GREEN);
  OledColorDisplay::DrawPixel(start_x + 1,start_y + 4, BLACK);
  OledColorDisplay::DrawPixel(start_x + 2,start_y + 4, BLACK);
  OledColorDisplay::DrawPixel(start_x + 3,start_y + 4, BLACK);
  OledColorDisplay::DrawPixel(start_x + 4,start_y + 4, GREEN);
  OledColorDisplay::DrawPixel(start_x + 0,start_y + 5, GREEN);
  OledColorDisplay::DrawPixel(start_x + 1,start_y + 5, GREEN);
  OledColorDisplay::DrawPixel(start_x + 2,start_y + 5, GREEN);
  OledColorDisplay::DrawPixel(start_x + 3,start_y + 5, GREEN);
  OledColorDisplay::DrawPixel(start_x + 4,start_y + 5, BLACK);
  OledColorDisplay::DrawPixel(start_x + 0,start_y + 6, GREEN);
  OledColorDisplay::DrawPixel(start_x + 1,start_y + 6, BLACK);
  OledColorDisplay::DrawPixel(start_x + 2,start_y + 6, BLACK);
  OledColorDisplay::DrawPixel(start_x + 3,start_y + 6, BLACK);
  OledColorDisplay::DrawPixel(start_x + 4,start_y + 6, BLACK);
  OledColorDisplay::DrawPixel(start_x + 0,start_y + 7, GREEN);
  OledColorDisplay::DrawPixel(start_x + 1,start_y + 7, BLACK);
  OledColorDisplay::DrawPixel(start_x + 2,start_y + 7, BLACK);
  OledColorDisplay::DrawPixel(start_x + 3,start_y + 7, BLACK);
  OledColorDisplay::DrawPixel(start_x + 4,start_y + 7, BLACK);
} else if ( mydrawchar == "q" ) {
  OledColorDisplay::DrawPixel(start_x + 0,start_y + 0, BLACK);
  OledColorDisplay::DrawPixel(start_x + 1,start_y + 0, BLACK);
  OledColorDisplay::DrawPixel(start_x + 2,start_y + 0, BLACK);
  OledColorDisplay::DrawPixel(start_x + 3,start_y + 0, BLACK);
  OledColorDisplay::DrawPixel(start_x + 4,start_y + 0, BLACK);
  OledColorDisplay::DrawPixel(start_x + 0,start_y + 1, BLACK);
  OledColorDisplay::DrawPixel(start_x + 1,start_y + 1, BLACK);
  OledColorDisplay::DrawPixel(start_x + 2,start_y + 1, BLACK);
  OledColorDisplay::DrawPixel(start_x + 3,start_y + 1, BLACK);
  OledColorDisplay::DrawPixel(start_x + 4,start_y + 1, BLACK);
  OledColorDisplay::DrawPixel(start_x + 0,start_y + 2, BLACK);
  OledColorDisplay::DrawPixel(start_x + 1,start_y + 2, BLACK);
  OledColorDisplay::DrawPixel(start_x + 2,start_y + 2, BLACK);
  OledColorDisplay::DrawPixel(start_x + 3,start_y + 2, BLACK);
  OledColorDisplay::DrawPixel(start_x + 4,start_y + 2, BLACK);
  OledColorDisplay::DrawPixel(start_x + 0,start_y + 3, BLACK);
  OledColorDisplay::DrawPixel(start_x + 1,start_y + 3, GREEN);
  OledColorDisplay::DrawPixel(start_x + 2,start_y + 3, GREEN);
  OledColorDisplay::DrawPixel(start_x + 3,start_y + 3, BLACK);
  OledColorDisplay::DrawPixel(start_x + 4,start_y + 3, GREEN);
  OledColorDisplay::DrawPixel(start_x + 0,start_y + 4, GREEN);
  OledColorDisplay::DrawPixel(start_x + 1,start_y + 4, BLACK);
  OledColorDisplay::DrawPixel(start_x + 2,start_y + 4, BLACK);
  OledColorDisplay::DrawPixel(start_x + 3,start_y + 4, GREEN);
  OledColorDisplay::DrawPixel(start_x + 4,start_y + 4, GREEN);
  OledColorDisplay::DrawPixel(start_x + 0,start_y + 5, BLACK);
  OledColorDisplay::DrawPixel(start_x + 1,start_y + 5, GREEN);
  OledColorDisplay::DrawPixel(start_x + 2,start_y + 5, GREEN);
  OledColorDisplay::DrawPixel(start_x + 3,start_y + 5, GREEN);
  OledColorDisplay::DrawPixel(start_x + 4,start_y + 5, GREEN);
  OledColorDisplay::DrawPixel(start_x + 0,start_y + 6, BLACK);
  OledColorDisplay::DrawPixel(start_x + 1,start_y + 6, BLACK);
  OledColorDisplay::DrawPixel(start_x + 2,start_y + 6, BLACK);
  OledColorDisplay::DrawPixel(start_x + 3,start_y + 6, BLACK);
  OledColorDisplay::DrawPixel(start_x + 4,start_y + 6, GREEN);
  OledColorDisplay::DrawPixel(start_x + 0,start_y + 7, BLACK);
  OledColorDisplay::DrawPixel(start_x + 1,start_y + 7, BLACK);
  OledColorDisplay::DrawPixel(start_x + 2,start_y + 7, BLACK);
  OledColorDisplay::DrawPixel(start_x + 3,start_y + 7, BLACK);
  OledColorDisplay::DrawPixel(start_x + 4,start_y + 7, GREEN);
} else if ( mydrawchar == "r" ) {
  OledColorDisplay::DrawPixel(start_x + 0,start_y + 0, BLACK);
  OledColorDisplay::DrawPixel(start_x + 1,start_y + 0, BLACK);
  OledColorDisplay::DrawPixel(start_x + 2,start_y + 0, BLACK);
  OledColorDisplay::DrawPixel(start_x + 3,start_y + 0, BLACK);
  OledColorDisplay::DrawPixel(start_x + 4,start_y + 0, BLACK);
  OledColorDisplay::DrawPixel(start_x + 0,start_y + 1, BLACK);
  OledColorDisplay::DrawPixel(start_x + 1,start_y + 1, BLACK);
  OledColorDisplay::DrawPixel(start_x + 2,start_y + 1, BLACK);
  OledColorDisplay::DrawPixel(start_x + 3,start_y + 1, BLACK);
  OledColorDisplay::DrawPixel(start_x + 4,start_y + 1, BLACK);
  OledColorDisplay::DrawPixel(start_x + 0,start_y + 2, BLACK);
  OledColorDisplay::DrawPixel(start_x + 1,start_y + 2, BLACK);
  OledColorDisplay::DrawPixel(start_x + 2,start_y + 2, BLACK);
  OledColorDisplay::DrawPixel(start_x + 3,start_y + 2, BLACK);
  OledColorDisplay::DrawPixel(start_x + 4,start_y + 2, BLACK);
  OledColorDisplay::DrawPixel(start_x + 0,start_y + 3, GREEN);
  OledColorDisplay::DrawPixel(start_x + 1,start_y + 3, BLACK);
  OledColorDisplay::DrawPixel(start_x + 2,start_y + 3, GREEN);
  OledColorDisplay::DrawPixel(start_x + 3,start_y + 3, GREEN);
  OledColorDisplay::DrawPixel(start_x + 4,start_y + 3, BLACK);
  OledColorDisplay::DrawPixel(start_x + 0,start_y + 4, GREEN);
  OledColorDisplay::DrawPixel(start_x + 1,start_y + 4, GREEN);
  OledColorDisplay::DrawPixel(start_x + 2,start_y + 4, BLACK);
  OledColorDisplay::DrawPixel(start_x + 3,start_y + 4, BLACK);
  OledColorDisplay::DrawPixel(start_x + 4,start_y + 4, GREEN);
  OledColorDisplay::DrawPixel(start_x + 0,start_y + 5, GREEN);
  OledColorDisplay::DrawPixel(start_x + 1,start_y + 5, BLACK);
  OledColorDisplay::DrawPixel(start_x + 2,start_y + 5, BLACK);
  OledColorDisplay::DrawPixel(start_x + 3,start_y + 5, BLACK);
  OledColorDisplay::DrawPixel(start_x + 4,start_y + 5, BLACK);
  OledColorDisplay::DrawPixel(start_x + 0,start_y + 6, GREEN);
  OledColorDisplay::DrawPixel(start_x + 1,start_y + 6, BLACK);
  OledColorDisplay::DrawPixel(start_x + 2,start_y + 6, BLACK);
  OledColorDisplay::DrawPixel(start_x + 3,start_y + 6, BLACK);
  OledColorDisplay::DrawPixel(start_x + 4,start_y + 6, BLACK);
  OledColorDisplay::DrawPixel(start_x + 0,start_y + 7, GREEN);
  OledColorDisplay::DrawPixel(start_x + 1,start_y + 7, BLACK);
  OledColorDisplay::DrawPixel(start_x + 2,start_y + 7, BLACK);
  OledColorDisplay::DrawPixel(start_x + 3,start_y + 7, BLACK);
  OledColorDisplay::DrawPixel(start_x + 4,start_y + 7, BLACK);
} else if ( mydrawchar == "s" ) {
  OledColorDisplay::DrawPixel(start_x + 0,start_y + 0, BLACK);
  OledColorDisplay::DrawPixel(start_x + 1,start_y + 0, BLACK);
  OledColorDisplay::DrawPixel(start_x + 2,start_y + 0, BLACK);
  OledColorDisplay::DrawPixel(start_x + 3,start_y + 0, BLACK);
  OledColorDisplay::DrawPixel(start_x + 4,start_y + 0, BLACK);
  OledColorDisplay::DrawPixel(start_x + 0,start_y + 1, BLACK);
  OledColorDisplay::DrawPixel(start_x + 1,start_y + 1, BLACK);
  OledColorDisplay::DrawPixel(start_x + 2,start_y + 1, BLACK);
  OledColorDisplay::DrawPixel(start_x + 3,start_y + 1, BLACK);
  OledColorDisplay::DrawPixel(start_x + 4,start_y + 1, BLACK);
  OledColorDisplay::DrawPixel(start_x + 0,start_y + 2, BLACK);
  OledColorDisplay::DrawPixel(start_x + 1,start_y + 2, BLACK);
  OledColorDisplay::DrawPixel(start_x + 2,start_y + 2, BLACK);
  OledColorDisplay::DrawPixel(start_x + 3,start_y + 2, BLACK);
  OledColorDisplay::DrawPixel(start_x + 4,start_y + 2, BLACK);
  OledColorDisplay::DrawPixel(start_x + 0,start_y + 3, BLACK);
  OledColorDisplay::DrawPixel(start_x + 1,start_y + 3, GREEN);
  OledColorDisplay::DrawPixel(start_x + 2,start_y + 3, GREEN);
  OledColorDisplay::DrawPixel(start_x + 3,start_y + 3, GREEN);
  OledColorDisplay::DrawPixel(start_x + 4,start_y + 3, BLACK);
  OledColorDisplay::DrawPixel(start_x + 0,start_y + 4, GREEN);
  OledColorDisplay::DrawPixel(start_x + 1,start_y + 4, BLACK);
  OledColorDisplay::DrawPixel(start_x + 2,start_y + 4, BLACK);
  OledColorDisplay::DrawPixel(start_x + 3,start_y + 4, BLACK);
  OledColorDisplay::DrawPixel(start_x + 4,start_y + 4, BLACK);
  OledColorDisplay::DrawPixel(start_x + 0,start_y + 5, BLACK);
  OledColorDisplay::DrawPixel(start_x + 1,start_y + 5, GREEN);
  OledColorDisplay::DrawPixel(start_x + 2,start_y + 5, GREEN);
  OledColorDisplay::DrawPixel(start_x + 3,start_y + 5, GREEN);
  OledColorDisplay::DrawPixel(start_x + 4,start_y + 5, BLACK);
  OledColorDisplay::DrawPixel(start_x + 0,start_y + 6, BLACK);
  OledColorDisplay::DrawPixel(start_x + 1,start_y + 6, BLACK);
  OledColorDisplay::DrawPixel(start_x + 2,start_y + 6, BLACK);
  OledColorDisplay::DrawPixel(start_x + 3,start_y + 6, BLACK);
  OledColorDisplay::DrawPixel(start_x + 4,start_y + 6, GREEN);
  OledColorDisplay::DrawPixel(start_x + 0,start_y + 7, GREEN);
  OledColorDisplay::DrawPixel(start_x + 1,start_y + 7, GREEN);
  OledColorDisplay::DrawPixel(start_x + 2,start_y + 7, GREEN);
  OledColorDisplay::DrawPixel(start_x + 3,start_y + 7, GREEN);
  OledColorDisplay::DrawPixel(start_x + 4,start_y + 7, BLACK);
} else if ( mydrawchar == "t" ) {
  OledColorDisplay::DrawPixel(start_x + 0,start_y + 0, BLACK);
  OledColorDisplay::DrawPixel(start_x + 1,start_y + 0, BLACK);
  OledColorDisplay::DrawPixel(start_x + 2,start_y + 0, BLACK);
  OledColorDisplay::DrawPixel(start_x + 3,start_y + 0, BLACK);
  OledColorDisplay::DrawPixel(start_x + 4,start_y + 0, BLACK);
  OledColorDisplay::DrawPixel(start_x + 0,start_y + 1, BLACK);
  OledColorDisplay::DrawPixel(start_x + 1,start_y + 1, BLACK);
  OledColorDisplay::DrawPixel(start_x + 2,start_y + 1, GREEN);
  OledColorDisplay::DrawPixel(start_x + 3,start_y + 1, BLACK);
  OledColorDisplay::DrawPixel(start_x + 4,start_y + 1, BLACK);
  OledColorDisplay::DrawPixel(start_x + 0,start_y + 2, BLACK);
  OledColorDisplay::DrawPixel(start_x + 1,start_y + 2, BLACK);
  OledColorDisplay::DrawPixel(start_x + 2,start_y + 2, GREEN);
  OledColorDisplay::DrawPixel(start_x + 3,start_y + 2, BLACK);
  OledColorDisplay::DrawPixel(start_x + 4,start_y + 2, BLACK);
  OledColorDisplay::DrawPixel(start_x + 0,start_y + 3, BLACK);
  OledColorDisplay::DrawPixel(start_x + 1,start_y + 3, GREEN);
  OledColorDisplay::DrawPixel(start_x + 2,start_y + 3, GREEN);
  OledColorDisplay::DrawPixel(start_x + 3,start_y + 3, GREEN);
  OledColorDisplay::DrawPixel(start_x + 4,start_y + 3, BLACK);
  OledColorDisplay::DrawPixel(start_x + 0,start_y + 4, BLACK);
  OledColorDisplay::DrawPixel(start_x + 1,start_y + 4, BLACK);
  OledColorDisplay::DrawPixel(start_x + 2,start_y + 4, GREEN);
  OledColorDisplay::DrawPixel(start_x + 3,start_y + 4, BLACK);
  OledColorDisplay::DrawPixel(start_x + 4,start_y + 4, BLACK);
  OledColorDisplay::DrawPixel(start_x + 0,start_y + 5, BLACK);
  OledColorDisplay::DrawPixel(start_x + 1,start_y + 5, BLACK);
  OledColorDisplay::DrawPixel(start_x + 2,start_y + 5, GREEN);
  OledColorDisplay::DrawPixel(start_x + 3,start_y + 5, BLACK);
  OledColorDisplay::DrawPixel(start_x + 4,start_y + 5, BLACK);
  OledColorDisplay::DrawPixel(start_x + 0,start_y + 6, BLACK);
  OledColorDisplay::DrawPixel(start_x + 1,start_y + 6, BLACK);
  OledColorDisplay::DrawPixel(start_x + 2,start_y + 6, GREEN);
  OledColorDisplay::DrawPixel(start_x + 3,start_y + 6, BLACK);
  OledColorDisplay::DrawPixel(start_x + 4,start_y + 6, BLACK);
  OledColorDisplay::DrawPixel(start_x + 0,start_y + 7, BLACK);
  OledColorDisplay::DrawPixel(start_x + 1,start_y + 7, BLACK);
  OledColorDisplay::DrawPixel(start_x + 2,start_y + 7, GREEN);
  OledColorDisplay::DrawPixel(start_x + 3,start_y + 7, GREEN);
  OledColorDisplay::DrawPixel(start_x + 4,start_y + 7, BLACK);
} else if ( mydrawchar == "u" ) {
  OledColorDisplay::DrawPixel(start_x + 0,start_y + 0, BLACK);
  OledColorDisplay::DrawPixel(start_x + 1,start_y + 0, BLACK);
  OledColorDisplay::DrawPixel(start_x + 2,start_y + 0, BLACK);
  OledColorDisplay::DrawPixel(start_x + 3,start_y + 0, BLACK);
  OledColorDisplay::DrawPixel(start_x + 4,start_y + 0, BLACK);
  OledColorDisplay::DrawPixel(start_x + 0,start_y + 1, BLACK);
  OledColorDisplay::DrawPixel(start_x + 1,start_y + 1, BLACK);
  OledColorDisplay::DrawPixel(start_x + 2,start_y + 1, BLACK);
  OledColorDisplay::DrawPixel(start_x + 3,start_y + 1, BLACK);
  OledColorDisplay::DrawPixel(start_x + 4,start_y + 1, BLACK);
  OledColorDisplay::DrawPixel(start_x + 0,start_y + 2, BLACK);
  OledColorDisplay::DrawPixel(start_x + 1,start_y + 2, BLACK);
  OledColorDisplay::DrawPixel(start_x + 2,start_y + 2, BLACK);
  OledColorDisplay::DrawPixel(start_x + 3,start_y + 2, BLACK);
  OledColorDisplay::DrawPixel(start_x + 4,start_y + 2, BLACK);
  OledColorDisplay::DrawPixel(start_x + 0,start_y + 3, GREEN);
  OledColorDisplay::DrawPixel(start_x + 1,start_y + 3, BLACK);
  OledColorDisplay::DrawPixel(start_x + 2,start_y + 3, BLACK);
  OledColorDisplay::DrawPixel(start_x + 3,start_y + 3, BLACK);
  OledColorDisplay::DrawPixel(start_x + 4,start_y + 3, GREEN);
  OledColorDisplay::DrawPixel(start_x + 0,start_y + 4, GREEN);
  OledColorDisplay::DrawPixel(start_x + 1,start_y + 4, BLACK);
  OledColorDisplay::DrawPixel(start_x + 2,start_y + 4, BLACK);
  OledColorDisplay::DrawPixel(start_x + 3,start_y + 4, BLACK);
  OledColorDisplay::DrawPixel(start_x + 4,start_y + 4, GREEN);
  OledColorDisplay::DrawPixel(start_x + 0,start_y + 5, GREEN);
  OledColorDisplay::DrawPixel(start_x + 1,start_y + 5, BLACK);
  OledColorDisplay::DrawPixel(start_x + 2,start_y + 5, BLACK);
  OledColorDisplay::DrawPixel(start_x + 3,start_y + 5, BLACK);
  OledColorDisplay::DrawPixel(start_x + 4,start_y + 5, GREEN);
  OledColorDisplay::DrawPixel(start_x + 0,start_y + 6, GREEN);
  OledColorDisplay::DrawPixel(start_x + 1,start_y + 6, BLACK);
  OledColorDisplay::DrawPixel(start_x + 2,start_y + 6, BLACK);
  OledColorDisplay::DrawPixel(start_x + 3,start_y + 6, BLACK);
  OledColorDisplay::DrawPixel(start_x + 4,start_y + 6, GREEN);
  OledColorDisplay::DrawPixel(start_x + 0,start_y + 7, BLACK);
  OledColorDisplay::DrawPixel(start_x + 1,start_y + 7, GREEN);
  OledColorDisplay::DrawPixel(start_x + 2,start_y + 7, GREEN);
  OledColorDisplay::DrawPixel(start_x + 3,start_y + 7, GREEN);
  OledColorDisplay::DrawPixel(start_x + 4,start_y + 7, BLACK);
} else if ( mydrawchar == "v" ) {
  OledColorDisplay::DrawPixel(start_x + 0,start_y + 0, BLACK);
  OledColorDisplay::DrawPixel(start_x + 1,start_y + 0, BLACK);
  OledColorDisplay::DrawPixel(start_x + 2,start_y + 0, BLACK);
  OledColorDisplay::DrawPixel(start_x + 3,start_y + 0, BLACK);
  OledColorDisplay::DrawPixel(start_x + 4,start_y + 0, BLACK);
  OledColorDisplay::DrawPixel(start_x + 0,start_y + 1, BLACK);
  OledColorDisplay::DrawPixel(start_x + 1,start_y + 1, BLACK);
  OledColorDisplay::DrawPixel(start_x + 2,start_y + 1, BLACK);
  OledColorDisplay::DrawPixel(start_x + 3,start_y + 1, BLACK);
  OledColorDisplay::DrawPixel(start_x + 4,start_y + 1, BLACK);
  OledColorDisplay::DrawPixel(start_x + 0,start_y + 2, BLACK);
  OledColorDisplay::DrawPixel(start_x + 1,start_y + 2, BLACK);
  OledColorDisplay::DrawPixel(start_x + 2,start_y + 2, BLACK);
  OledColorDisplay::DrawPixel(start_x + 3,start_y + 2, BLACK);
  OledColorDisplay::DrawPixel(start_x + 4,start_y + 2, BLACK);
  OledColorDisplay::DrawPixel(start_x + 0,start_y + 3, GREEN);
  OledColorDisplay::DrawPixel(start_x + 1,start_y + 3, BLACK);
  OledColorDisplay::DrawPixel(start_x + 2,start_y + 3, BLACK);
  OledColorDisplay::DrawPixel(start_x + 3,start_y + 3, BLACK);
  OledColorDisplay::DrawPixel(start_x + 4,start_y + 3, GREEN);
  OledColorDisplay::DrawPixel(start_x + 0,start_y + 4, GREEN);
  OledColorDisplay::DrawPixel(start_x + 1,start_y + 4, BLACK);
  OledColorDisplay::DrawPixel(start_x + 2,start_y + 4, BLACK);
  OledColorDisplay::DrawPixel(start_x + 3,start_y + 4, BLACK);
  OledColorDisplay::DrawPixel(start_x + 4,start_y + 4, GREEN);
  OledColorDisplay::DrawPixel(start_x + 0,start_y + 5, GREEN);
  OledColorDisplay::DrawPixel(start_x + 1,start_y + 5, BLACK);
  OledColorDisplay::DrawPixel(start_x + 2,start_y + 5, BLACK);
  OledColorDisplay::DrawPixel(start_x + 3,start_y + 5, BLACK);
  OledColorDisplay::DrawPixel(start_x + 4,start_y + 5, GREEN);
  OledColorDisplay::DrawPixel(start_x + 0,start_y + 6, BLACK);
  OledColorDisplay::DrawPixel(start_x + 1,start_y + 6, GREEN);
  OledColorDisplay::DrawPixel(start_x + 2,start_y + 6, BLACK);
  OledColorDisplay::DrawPixel(start_x + 3,start_y + 6, GREEN);
  OledColorDisplay::DrawPixel(start_x + 4,start_y + 6, BLACK);
  OledColorDisplay::DrawPixel(start_x + 0,start_y + 7, BLACK);
  OledColorDisplay::DrawPixel(start_x + 1,start_y + 7, BLACK);
  OledColorDisplay::DrawPixel(start_x + 2,start_y + 7, GREEN);
  OledColorDisplay::DrawPixel(start_x + 3,start_y + 7, BLACK);
  OledColorDisplay::DrawPixel(start_x + 4,start_y + 7, BLACK);
} else if ( mydrawchar == "w" ) {
  OledColorDisplay::DrawPixel(start_x + 0,start_y + 0, BLACK);
  OledColorDisplay::DrawPixel(start_x + 1,start_y + 0, BLACK);
  OledColorDisplay::DrawPixel(start_x + 2,start_y + 0, BLACK);
  OledColorDisplay::DrawPixel(start_x + 3,start_y + 0, BLACK);
  OledColorDisplay::DrawPixel(start_x + 4,start_y + 0, BLACK);
  OledColorDisplay::DrawPixel(start_x + 0,start_y + 1, BLACK);
  OledColorDisplay::DrawPixel(start_x + 1,start_y + 1, BLACK);
  OledColorDisplay::DrawPixel(start_x + 2,start_y + 1, BLACK);
  OledColorDisplay::DrawPixel(start_x + 3,start_y + 1, BLACK);
  OledColorDisplay::DrawPixel(start_x + 4,start_y + 1, BLACK);
  OledColorDisplay::DrawPixel(start_x + 0,start_y + 2, BLACK);
  OledColorDisplay::DrawPixel(start_x + 1,start_y + 2, BLACK);
  OledColorDisplay::DrawPixel(start_x + 2,start_y + 2, BLACK);
  OledColorDisplay::DrawPixel(start_x + 3,start_y + 2, BLACK);
  OledColorDisplay::DrawPixel(start_x + 4,start_y + 2, BLACK);
  OledColorDisplay::DrawPixel(start_x + 0,start_y + 3, GREEN);
  OledColorDisplay::DrawPixel(start_x + 1,start_y + 3, BLACK);
  OledColorDisplay::DrawPixel(start_x + 2,start_y + 3, BLACK);
  OledColorDisplay::DrawPixel(start_x + 3,start_y + 3, BLACK);
  OledColorDisplay::DrawPixel(start_x + 4,start_y + 3, GREEN);
  OledColorDisplay::DrawPixel(start_x + 0,start_y + 4, GREEN);
  OledColorDisplay::DrawPixel(start_x + 1,start_y + 4, BLACK);
  OledColorDisplay::DrawPixel(start_x + 2,start_y + 4, BLACK);
  OledColorDisplay::DrawPixel(start_x + 3,start_y + 4, BLACK);
  OledColorDisplay::DrawPixel(start_x + 4,start_y + 4, GREEN);
  OledColorDisplay::DrawPixel(start_x + 0,start_y + 5, GREEN);
  OledColorDisplay::DrawPixel(start_x + 1,start_y + 5, BLACK);
  OledColorDisplay::DrawPixel(start_x + 2,start_y + 5, GREEN);
  OledColorDisplay::DrawPixel(start_x + 3,start_y + 5, BLACK);
  OledColorDisplay::DrawPixel(start_x + 4,start_y + 5, GREEN);
  OledColorDisplay::DrawPixel(start_x + 0,start_y + 6, GREEN);
  OledColorDisplay::DrawPixel(start_x + 1,start_y + 6, BLACK);
  OledColorDisplay::DrawPixel(start_x + 2,start_y + 6, GREEN);
  OledColorDisplay::DrawPixel(start_x + 3,start_y + 6, BLACK);
  OledColorDisplay::DrawPixel(start_x + 4,start_y + 6, GREEN);
  OledColorDisplay::DrawPixel(start_x + 0,start_y + 7, BLACK);
  OledColorDisplay::DrawPixel(start_x + 1,start_y + 7, GREEN);
  OledColorDisplay::DrawPixel(start_x + 2,start_y + 7, BLACK);
  OledColorDisplay::DrawPixel(start_x + 3,start_y + 7, GREEN);
  OledColorDisplay::DrawPixel(start_x + 4,start_y + 7, BLACK);
} else if ( mydrawchar == "x" ) {
  OledColorDisplay::DrawPixel(start_x + 0,start_y + 0, BLACK);
  OledColorDisplay::DrawPixel(start_x + 1,start_y + 0, BLACK);
  OledColorDisplay::DrawPixel(start_x + 2,start_y + 0, BLACK);
  OledColorDisplay::DrawPixel(start_x + 3,start_y + 0, BLACK);
  OledColorDisplay::DrawPixel(start_x + 4,start_y + 0, BLACK);
  OledColorDisplay::DrawPixel(start_x + 0,start_y + 1, BLACK);
  OledColorDisplay::DrawPixel(start_x + 1,start_y + 1, BLACK);
  OledColorDisplay::DrawPixel(start_x + 2,start_y + 1, BLACK);
  OledColorDisplay::DrawPixel(start_x + 3,start_y + 1, BLACK);
  OledColorDisplay::DrawPixel(start_x + 4,start_y + 1, BLACK);
  OledColorDisplay::DrawPixel(start_x + 0,start_y + 2, BLACK);
  OledColorDisplay::DrawPixel(start_x + 1,start_y + 2, BLACK);
  OledColorDisplay::DrawPixel(start_x + 2,start_y + 2, BLACK);
  OledColorDisplay::DrawPixel(start_x + 3,start_y + 2, BLACK);
  OledColorDisplay::DrawPixel(start_x + 4,start_y + 2, BLACK);
  OledColorDisplay::DrawPixel(start_x + 0,start_y + 3, GREEN);
  OledColorDisplay::DrawPixel(start_x + 1,start_y + 3, BLACK);
  OledColorDisplay::DrawPixel(start_x + 2,start_y + 3, BLACK);
  OledColorDisplay::DrawPixel(start_x + 3,start_y + 3, BLACK);
  OledColorDisplay::DrawPixel(start_x + 4,start_y + 3, GREEN);
  OledColorDisplay::DrawPixel(start_x + 0,start_y + 4, BLACK);
  OledColorDisplay::DrawPixel(start_x + 1,start_y + 4, GREEN);
  OledColorDisplay::DrawPixel(start_x + 2,start_y + 4, BLACK);
  OledColorDisplay::DrawPixel(start_x + 3,start_y + 4, GREEN);
  OledColorDisplay::DrawPixel(start_x + 4,start_y + 4, BLACK);
  OledColorDisplay::DrawPixel(start_x + 0,start_y + 5, BLACK);
  OledColorDisplay::DrawPixel(start_x + 1,start_y + 5, BLACK);
  OledColorDisplay::DrawPixel(start_x + 2,start_y + 5, GREEN);
  OledColorDisplay::DrawPixel(start_x + 3,start_y + 5, BLACK);
  OledColorDisplay::DrawPixel(start_x + 4,start_y + 5, BLACK);
  OledColorDisplay::DrawPixel(start_x + 0,start_y + 6, BLACK);
  OledColorDisplay::DrawPixel(start_x + 1,start_y + 6, GREEN);
  OledColorDisplay::DrawPixel(start_x + 2,start_y + 6, BLACK);
  OledColorDisplay::DrawPixel(start_x + 3,start_y + 6, GREEN);
  OledColorDisplay::DrawPixel(start_x + 4,start_y + 6, BLACK);
  OledColorDisplay::DrawPixel(start_x + 0,start_y + 7, GREEN);
  OledColorDisplay::DrawPixel(start_x + 1,start_y + 7, BLACK);
  OledColorDisplay::DrawPixel(start_x + 2,start_y + 7, BLACK);
  OledColorDisplay::DrawPixel(start_x + 3,start_y + 7, BLACK);
  OledColorDisplay::DrawPixel(start_x + 4,start_y + 7, GREEN);
} else if ( mydrawchar == "y" ) {
  OledColorDisplay::DrawPixel(start_x + 0,start_y + 0, BLACK);
  OledColorDisplay::DrawPixel(start_x + 1,start_y + 0, BLACK);
  OledColorDisplay::DrawPixel(start_x + 2,start_y + 0, BLACK);
  OledColorDisplay::DrawPixel(start_x + 3,start_y + 0, BLACK);
  OledColorDisplay::DrawPixel(start_x + 4,start_y + 0, BLACK);
  OledColorDisplay::DrawPixel(start_x + 0,start_y + 1, BLACK);
  OledColorDisplay::DrawPixel(start_x + 1,start_y + 1, BLACK);
  OledColorDisplay::DrawPixel(start_x + 2,start_y + 1, BLACK);
  OledColorDisplay::DrawPixel(start_x + 3,start_y + 1, BLACK);
  OledColorDisplay::DrawPixel(start_x + 4,start_y + 1, BLACK);
  OledColorDisplay::DrawPixel(start_x + 0,start_y + 2, BLACK);
  OledColorDisplay::DrawPixel(start_x + 1,start_y + 2, BLACK);
  OledColorDisplay::DrawPixel(start_x + 2,start_y + 2, BLACK);
  OledColorDisplay::DrawPixel(start_x + 3,start_y + 2, BLACK);
  OledColorDisplay::DrawPixel(start_x + 4,start_y + 2, BLACK);
  OledColorDisplay::DrawPixel(start_x + 0,start_y + 3, GREEN);
  OledColorDisplay::DrawPixel(start_x + 1,start_y + 3, BLACK);
  OledColorDisplay::DrawPixel(start_x + 2,start_y + 3, BLACK);
  OledColorDisplay::DrawPixel(start_x + 3,start_y + 3, BLACK);
  OledColorDisplay::DrawPixel(start_x + 4,start_y + 3, GREEN);
  OledColorDisplay::DrawPixel(start_x + 0,start_y + 4, GREEN);
  OledColorDisplay::DrawPixel(start_x + 1,start_y + 4, BLACK);
  OledColorDisplay::DrawPixel(start_x + 2,start_y + 4, BLACK);
  OledColorDisplay::DrawPixel(start_x + 3,start_y + 4, BLACK);
  OledColorDisplay::DrawPixel(start_x + 4,start_y + 4, GREEN);
  OledColorDisplay::DrawPixel(start_x + 0,start_y + 5, BLACK);
  OledColorDisplay::DrawPixel(start_x + 1,start_y + 5, GREEN);
  OledColorDisplay::DrawPixel(start_x + 2,start_y + 5, GREEN);
  OledColorDisplay::DrawPixel(start_x + 3,start_y + 5, GREEN);
  OledColorDisplay::DrawPixel(start_x + 4,start_y + 5, GREEN);
  OledColorDisplay::DrawPixel(start_x + 0,start_y + 6, BLACK);
  OledColorDisplay::DrawPixel(start_x + 1,start_y + 6, BLACK);
  OledColorDisplay::DrawPixel(start_x + 2,start_y + 6, BLACK);
  OledColorDisplay::DrawPixel(start_x + 3,start_y + 6, BLACK);
  OledColorDisplay::DrawPixel(start_x + 4,start_y + 6, GREEN);
  OledColorDisplay::DrawPixel(start_x + 0,start_y + 7, BLACK);
  OledColorDisplay::DrawPixel(start_x + 1,start_y + 7, GREEN);
  OledColorDisplay::DrawPixel(start_x + 2,start_y + 7, GREEN);
  OledColorDisplay::DrawPixel(start_x + 3,start_y + 7, GREEN);
  OledColorDisplay::DrawPixel(start_x + 4,start_y + 7, BLACK);
} else if ( mydrawchar == "z" ) {
  OledColorDisplay::DrawPixel(start_x + 0,start_y + 0, BLACK);
  OledColorDisplay::DrawPixel(start_x + 1,start_y + 0, BLACK);
  OledColorDisplay::DrawPixel(start_x + 2,start_y + 0, BLACK);
  OledColorDisplay::DrawPixel(start_x + 3,start_y + 0, BLACK);
  OledColorDisplay::DrawPixel(start_x + 4,start_y + 0, BLACK);
  OledColorDisplay::DrawPixel(start_x + 0,start_y + 1, BLACK);
  OledColorDisplay::DrawPixel(start_x + 1,start_y + 1, BLACK);
  OledColorDisplay::DrawPixel(start_x + 2,start_y + 1, BLACK);
  OledColorDisplay::DrawPixel(start_x + 3,start_y + 1, BLACK);
  OledColorDisplay::DrawPixel(start_x + 4,start_y + 1, BLACK);
  OledColorDisplay::DrawPixel(start_x + 0,start_y + 2, BLACK);
  OledColorDisplay::DrawPixel(start_x + 1,start_y + 2, BLACK);
  OledColorDisplay::DrawPixel(start_x + 2,start_y + 2, BLACK);
  OledColorDisplay::DrawPixel(start_x + 3,start_y + 2, BLACK);
  OledColorDisplay::DrawPixel(start_x + 4,start_y + 2, BLACK);
  OledColorDisplay::DrawPixel(start_x + 0,start_y + 3, GREEN);
  OledColorDisplay::DrawPixel(start_x + 1,start_y + 3, GREEN);
  OledColorDisplay::DrawPixel(start_x + 2,start_y + 3, GREEN);
  OledColorDisplay::DrawPixel(start_x + 3,start_y + 3, GREEN);
  OledColorDisplay::DrawPixel(start_x + 4,start_y + 3, GREEN);
  OledColorDisplay::DrawPixel(start_x + 0,start_y + 4, BLACK);
  OledColorDisplay::DrawPixel(start_x + 1,start_y + 4, BLACK);
  OledColorDisplay::DrawPixel(start_x + 2,start_y + 4, BLACK);
  OledColorDisplay::DrawPixel(start_x + 3,start_y + 4, GREEN);
  OledColorDisplay::DrawPixel(start_x + 4,start_y + 4, BLACK);
  OledColorDisplay::DrawPixel(start_x + 0,start_y + 5, BLACK);
  OledColorDisplay::DrawPixel(start_x + 1,start_y + 5, BLACK);
  OledColorDisplay::DrawPixel(start_x + 2,start_y + 5, GREEN);
  OledColorDisplay::DrawPixel(start_x + 3,start_y + 5, BLACK);
  OledColorDisplay::DrawPixel(start_x + 4,start_y + 5, BLACK);
  OledColorDisplay::DrawPixel(start_x + 0,start_y + 6, BLACK);
  OledColorDisplay::DrawPixel(start_x + 1,start_y + 6, GREEN);
  OledColorDisplay::DrawPixel(start_x + 2,start_y + 6, BLACK);
  OledColorDisplay::DrawPixel(start_x + 3,start_y + 6, BLACK);
  OledColorDisplay::DrawPixel(start_x + 4,start_y + 6, BLACK);
  OledColorDisplay::DrawPixel(start_x + 0,start_y + 7, GREEN);
  OledColorDisplay::DrawPixel(start_x + 1,start_y + 7, GREEN);
  OledColorDisplay::DrawPixel(start_x + 2,start_y + 7, GREEN);
  OledColorDisplay::DrawPixel(start_x + 3,start_y + 7, GREEN);
  OledColorDisplay::DrawPixel(start_x + 4,start_y + 7, GREEN);
} else if ( mydrawchar == "." ) {
  OledColorDisplay::DrawPixel(start_x + 0,start_y + 0, BLACK);
  OledColorDisplay::DrawPixel(start_x + 1,start_y + 0, BLACK);
  OledColorDisplay::DrawPixel(start_x + 2,start_y + 0, BLACK);
  OledColorDisplay::DrawPixel(start_x + 3,start_y + 0, BLACK);
  OledColorDisplay::DrawPixel(start_x + 4,start_y + 0, BLACK);
  OledColorDisplay::DrawPixel(start_x + 0,start_y + 1, BLACK);
  OledColorDisplay::DrawPixel(start_x + 1,start_y + 1, BLACK);
  OledColorDisplay::DrawPixel(start_x + 2,start_y + 1, BLACK);
  OledColorDisplay::DrawPixel(start_x + 3,start_y + 1, BLACK);
  OledColorDisplay::DrawPixel(start_x + 4,start_y + 1, BLACK);
  OledColorDisplay::DrawPixel(start_x + 0,start_y + 2, BLACK);
  OledColorDisplay::DrawPixel(start_x + 1,start_y + 2, BLACK);
  OledColorDisplay::DrawPixel(start_x + 2,start_y + 2, BLACK);
  OledColorDisplay::DrawPixel(start_x + 3,start_y + 2, BLACK);
  OledColorDisplay::DrawPixel(start_x + 4,start_y + 2, BLACK);
  OledColorDisplay::DrawPixel(start_x + 0,start_y + 3, BLACK);
  OledColorDisplay::DrawPixel(start_x + 1,start_y + 3, BLACK);
  OledColorDisplay::DrawPixel(start_x + 2,start_y + 3, BLACK);
  OledColorDisplay::DrawPixel(start_x + 3,start_y + 3, BLACK);
  OledColorDisplay::DrawPixel(start_x + 4,start_y + 3, BLACK);
  OledColorDisplay::DrawPixel(start_x + 0,start_y + 4, BLACK);
  OledColorDisplay::DrawPixel(start_x + 1,start_y + 4, BLACK);
  OledColorDisplay::DrawPixel(start_x + 2,start_y + 4, BLACK);
  OledColorDisplay::DrawPixel(start_x + 3,start_y + 4, BLACK);
  OledColorDisplay::DrawPixel(start_x + 4,start_y + 4, BLACK);
  OledColorDisplay::DrawPixel(start_x + 0,start_y + 5, BLACK);
  OledColorDisplay::DrawPixel(start_x + 1,start_y + 5, BLACK);
  OledColorDisplay::DrawPixel(start_x + 2,start_y + 5, BLACK);
  OledColorDisplay::DrawPixel(start_x + 3,start_y + 5, BLACK);
  OledColorDisplay::DrawPixel(start_x + 4,start_y + 5, BLACK);
  OledColorDisplay::DrawPixel(start_x + 0,start_y + 6, BLACK);
  OledColorDisplay::DrawPixel(start_x + 1,start_y + 6, GREEN);
  OledColorDisplay::DrawPixel(start_x + 2,start_y + 6, GREEN);
  OledColorDisplay::DrawPixel(start_x + 3,start_y + 6, BLACK);
  OledColorDisplay::DrawPixel(start_x + 4,start_y + 6, BLACK);
  OledColorDisplay::DrawPixel(start_x + 0,start_y + 7, BLACK);
  OledColorDisplay::DrawPixel(start_x + 1,start_y + 7, GREEN);
  OledColorDisplay::DrawPixel(start_x + 2,start_y + 7, GREEN);
  OledColorDisplay::DrawPixel(start_x + 3,start_y + 7, BLACK);
  OledColorDisplay::DrawPixel(start_x + 4,start_y + 7, BLACK);
} else if ( mydrawchar == "," ) {
  OledColorDisplay::DrawPixel(start_x + 0,start_y + 0, BLACK);
  OledColorDisplay::DrawPixel(start_x + 1,start_y + 0, BLACK);
  OledColorDisplay::DrawPixel(start_x + 2,start_y + 0, BLACK);
  OledColorDisplay::DrawPixel(start_x + 3,start_y + 0, BLACK);
  OledColorDisplay::DrawPixel(start_x + 4,start_y + 0, BLACK);
  OledColorDisplay::DrawPixel(start_x + 0,start_y + 1, BLACK);
  OledColorDisplay::DrawPixel(start_x + 1,start_y + 1, BLACK);
  OledColorDisplay::DrawPixel(start_x + 2,start_y + 1, BLACK);
  OledColorDisplay::DrawPixel(start_x + 3,start_y + 1, BLACK);
  OledColorDisplay::DrawPixel(start_x + 4,start_y + 1, BLACK);
  OledColorDisplay::DrawPixel(start_x + 0,start_y + 2, BLACK);
  OledColorDisplay::DrawPixel(start_x + 1,start_y + 2, BLACK);
  OledColorDisplay::DrawPixel(start_x + 2,start_y + 2, BLACK);
  OledColorDisplay::DrawPixel(start_x + 3,start_y + 2, BLACK);
  OledColorDisplay::DrawPixel(start_x + 4,start_y + 2, BLACK);
  OledColorDisplay::DrawPixel(start_x + 0,start_y + 3, BLACK);
  OledColorDisplay::DrawPixel(start_x + 1,start_y + 3, BLACK);
  OledColorDisplay::DrawPixel(start_x + 2,start_y + 3, BLACK);
  OledColorDisplay::DrawPixel(start_x + 3,start_y + 3, BLACK);
  OledColorDisplay::DrawPixel(start_x + 4,start_y + 3, BLACK);
  OledColorDisplay::DrawPixel(start_x + 0,start_y + 4, BLACK);
  OledColorDisplay::DrawPixel(start_x + 1,start_y + 4, BLACK);
  OledColorDisplay::DrawPixel(start_x + 2,start_y + 4, BLACK);
  OledColorDisplay::DrawPixel(start_x + 3,start_y + 4, BLACK);
  OledColorDisplay::DrawPixel(start_x + 4,start_y + 4, BLACK);
  OledColorDisplay::DrawPixel(start_x + 0,start_y + 5, BLACK);
  OledColorDisplay::DrawPixel(start_x + 1,start_y + 5, GREEN);
  OledColorDisplay::DrawPixel(start_x + 2,start_y + 5, GREEN);
  OledColorDisplay::DrawPixel(start_x + 3,start_y + 5, BLACK);
  OledColorDisplay::DrawPixel(start_x + 4,start_y + 5, BLACK);
  OledColorDisplay::DrawPixel(start_x + 0,start_y + 6, BLACK);
  OledColorDisplay::DrawPixel(start_x + 1,start_y + 6, BLACK);
  OledColorDisplay::DrawPixel(start_x + 2,start_y + 6, GREEN);
  OledColorDisplay::DrawPixel(start_x + 3,start_y + 6, BLACK);
  OledColorDisplay::DrawPixel(start_x + 4,start_y + 6, BLACK);
  OledColorDisplay::DrawPixel(start_x + 0,start_y + 7, BLACK);
  OledColorDisplay::DrawPixel(start_x + 1,start_y + 7, GREEN);
  OledColorDisplay::DrawPixel(start_x + 2,start_y + 7, BLACK);
  OledColorDisplay::DrawPixel(start_x + 3,start_y + 7, BLACK);
  OledColorDisplay::DrawPixel(start_x + 4,start_y + 7, BLACK);
} else if ( mydrawchar == "'" ) {
  OledColorDisplay::DrawPixel(start_x + 0,start_y + 0, BLACK);
  OledColorDisplay::DrawPixel(start_x + 1,start_y + 0, BLACK);
  OledColorDisplay::DrawPixel(start_x + 2,start_y + 0, BLACK);
  OledColorDisplay::DrawPixel(start_x + 3,start_y + 0, BLACK);
  OledColorDisplay::DrawPixel(start_x + 4,start_y + 0, BLACK);
  OledColorDisplay::DrawPixel(start_x + 0,start_y + 1, BLACK);
  OledColorDisplay::DrawPixel(start_x + 1,start_y + 1, GREEN);
  OledColorDisplay::DrawPixel(start_x + 2,start_y + 1, GREEN);
  OledColorDisplay::DrawPixel(start_x + 3,start_y + 1, BLACK);
  OledColorDisplay::DrawPixel(start_x + 4,start_y + 1, BLACK);
  OledColorDisplay::DrawPixel(start_x + 0,start_y + 2, BLACK);
  OledColorDisplay::DrawPixel(start_x + 1,start_y + 2, BLACK);
  OledColorDisplay::DrawPixel(start_x + 2,start_y + 2, GREEN);
  OledColorDisplay::DrawPixel(start_x + 3,start_y + 2, BLACK);
  OledColorDisplay::DrawPixel(start_x + 4,start_y + 2, BLACK);
  OledColorDisplay::DrawPixel(start_x + 0,start_y + 3, BLACK);
  OledColorDisplay::DrawPixel(start_x + 1,start_y + 3, GREEN);
  OledColorDisplay::DrawPixel(start_x + 2,start_y + 3, BLACK);
  OledColorDisplay::DrawPixel(start_x + 3,start_y + 3, BLACK);
  OledColorDisplay::DrawPixel(start_x + 4,start_y + 3, BLACK);
  OledColorDisplay::DrawPixel(start_x + 0,start_y + 4, BLACK);
  OledColorDisplay::DrawPixel(start_x + 1,start_y + 4, BLACK);
  OledColorDisplay::DrawPixel(start_x + 2,start_y + 4, BLACK);
  OledColorDisplay::DrawPixel(start_x + 3,start_y + 4, BLACK);
  OledColorDisplay::DrawPixel(start_x + 4,start_y + 4, BLACK);
  OledColorDisplay::DrawPixel(start_x + 0,start_y + 5, BLACK);
  OledColorDisplay::DrawPixel(start_x + 1,start_y + 5, BLACK);
  OledColorDisplay::DrawPixel(start_x + 2,start_y + 5, BLACK);
  OledColorDisplay::DrawPixel(start_x + 3,start_y + 5, BLACK);
  OledColorDisplay::DrawPixel(start_x + 4,start_y + 5, BLACK);
  OledColorDisplay::DrawPixel(start_x + 0,start_y + 6, BLACK);
  OledColorDisplay::DrawPixel(start_x + 1,start_y + 6, BLACK);
  OledColorDisplay::DrawPixel(start_x + 2,start_y + 6, BLACK);
  OledColorDisplay::DrawPixel(start_x + 3,start_y + 6, BLACK);
  OledColorDisplay::DrawPixel(start_x + 4,start_y + 6, BLACK);
  OledColorDisplay::DrawPixel(start_x + 0,start_y + 7, BLACK);
  OledColorDisplay::DrawPixel(start_x + 1,start_y + 7, BLACK);
  OledColorDisplay::DrawPixel(start_x + 2,start_y + 7, BLACK);
  OledColorDisplay::DrawPixel(start_x + 3,start_y + 7, BLACK);
  OledColorDisplay::DrawPixel(start_x + 4,start_y + 7, BLACK);
} else if ( mydrawchar == "?" ) {
  OledColorDisplay::DrawPixel(start_x + 0,start_y + 0, BLACK);
  OledColorDisplay::DrawPixel(start_x + 1,start_y + 0, BLACK);
  OledColorDisplay::DrawPixel(start_x + 2,start_y + 0, BLACK);
  OledColorDisplay::DrawPixel(start_x + 3,start_y + 0, BLACK);
  OledColorDisplay::DrawPixel(start_x + 4,start_y + 0, BLACK);
  OledColorDisplay::DrawPixel(start_x + 0,start_y + 1, BLACK);
  OledColorDisplay::DrawPixel(start_x + 1,start_y + 1, GREEN);
  OledColorDisplay::DrawPixel(start_x + 2,start_y + 1, GREEN);
  OledColorDisplay::DrawPixel(start_x + 3,start_y + 1, GREEN);
  OledColorDisplay::DrawPixel(start_x + 4,start_y + 1, BLACK);
  OledColorDisplay::DrawPixel(start_x + 0,start_y + 2, GREEN);
  OledColorDisplay::DrawPixel(start_x + 1,start_y + 2, BLACK);
  OledColorDisplay::DrawPixel(start_x + 2,start_y + 2, BLACK);
  OledColorDisplay::DrawPixel(start_x + 3,start_y + 2, BLACK);
  OledColorDisplay::DrawPixel(start_x + 4,start_y + 2, GREEN);
  OledColorDisplay::DrawPixel(start_x + 0,start_y + 3, BLACK);
  OledColorDisplay::DrawPixel(start_x + 1,start_y + 3, BLACK);
  OledColorDisplay::DrawPixel(start_x + 2,start_y + 3, BLACK);
  OledColorDisplay::DrawPixel(start_x + 3,start_y + 3, BLACK);
  OledColorDisplay::DrawPixel(start_x + 4,start_y + 3, GREEN);
  OledColorDisplay::DrawPixel(start_x + 0,start_y + 4, BLACK);
  OledColorDisplay::DrawPixel(start_x + 1,start_y + 4, BLACK);
  OledColorDisplay::DrawPixel(start_x + 2,start_y + 4, BLACK);
  OledColorDisplay::DrawPixel(start_x + 3,start_y + 4, GREEN);
  OledColorDisplay::DrawPixel(start_x + 4,start_y + 4, BLACK);
  OledColorDisplay::DrawPixel(start_x + 0,start_y + 5, BLACK);
  OledColorDisplay::DrawPixel(start_x + 1,start_y + 5, BLACK);
  OledColorDisplay::DrawPixel(start_x + 2,start_y + 5, GREEN);
  OledColorDisplay::DrawPixel(start_x + 3,start_y + 5, BLACK);
  OledColorDisplay::DrawPixel(start_x + 4,start_y + 5, BLACK);
  OledColorDisplay::DrawPixel(start_x + 0,start_y + 6, BLACK);
  OledColorDisplay::DrawPixel(start_x + 1,start_y + 6, BLACK);
  OledColorDisplay::DrawPixel(start_x + 2,start_y + 6, BLACK);
  OledColorDisplay::DrawPixel(start_x + 3,start_y + 6, BLACK);
  OledColorDisplay::DrawPixel(start_x + 4,start_y + 6, BLACK);
  OledColorDisplay::DrawPixel(start_x + 0,start_y + 7, BLACK);
  OledColorDisplay::DrawPixel(start_x + 1,start_y + 7, BLACK);
  OledColorDisplay::DrawPixel(start_x + 2,start_y + 7, GREEN);
  OledColorDisplay::DrawPixel(start_x + 3,start_y + 7, BLACK);
  OledColorDisplay::DrawPixel(start_x + 4,start_y + 7, BLACK);
} else if ( mydrawchar == "-" ) {
  OledColorDisplay::DrawPixel(start_x + 0,start_y + 0, BLACK);
  OledColorDisplay::DrawPixel(start_x + 1,start_y + 0, BLACK);
  OledColorDisplay::DrawPixel(start_x + 2,start_y + 0, BLACK);
  OledColorDisplay::DrawPixel(start_x + 3,start_y + 0, BLACK);
  OledColorDisplay::DrawPixel(start_x + 4,start_y + 0, BLACK);
  OledColorDisplay::DrawPixel(start_x + 0,start_y + 1, BLACK);
  OledColorDisplay::DrawPixel(start_x + 1,start_y + 1, BLACK);
  OledColorDisplay::DrawPixel(start_x + 2,start_y + 1, BLACK);
  OledColorDisplay::DrawPixel(start_x + 3,start_y + 1, BLACK);
  OledColorDisplay::DrawPixel(start_x + 4,start_y + 1, BLACK);
  OledColorDisplay::DrawPixel(start_x + 0,start_y + 2, BLACK);
  OledColorDisplay::DrawPixel(start_x + 1,start_y + 2, BLACK);
  OledColorDisplay::DrawPixel(start_x + 2,start_y + 2, BLACK);
  OledColorDisplay::DrawPixel(start_x + 3,start_y + 2, BLACK);
  OledColorDisplay::DrawPixel(start_x + 4,start_y + 2, BLACK);
  OledColorDisplay::DrawPixel(start_x + 0,start_y + 3, BLACK);
  OledColorDisplay::DrawPixel(start_x + 1,start_y + 3, BLACK);
  OledColorDisplay::DrawPixel(start_x + 2,start_y + 3, BLACK);
  OledColorDisplay::DrawPixel(start_x + 3,start_y + 3, BLACK);
  OledColorDisplay::DrawPixel(start_x + 4,start_y + 3, BLACK);
  OledColorDisplay::DrawPixel(start_x + 0,start_y + 4, GREEN);
  OledColorDisplay::DrawPixel(start_x + 1,start_y + 4, GREEN);
  OledColorDisplay::DrawPixel(start_x + 2,start_y + 4, GREEN);
  OledColorDisplay::DrawPixel(start_x + 3,start_y + 4, GREEN);
  OledColorDisplay::DrawPixel(start_x + 4,start_y + 4, GREEN);
  OledColorDisplay::DrawPixel(start_x + 0,start_y + 5, BLACK);
  OledColorDisplay::DrawPixel(start_x + 1,start_y + 5, BLACK);
  OledColorDisplay::DrawPixel(start_x + 2,start_y + 5, BLACK);
  OledColorDisplay::DrawPixel(start_x + 3,start_y + 5, BLACK);
  OledColorDisplay::DrawPixel(start_x + 4,start_y + 5, BLACK);
  OledColorDisplay::DrawPixel(start_x + 0,start_y + 6, BLACK);
  OledColorDisplay::DrawPixel(start_x + 1,start_y + 6, BLACK);
  OledColorDisplay::DrawPixel(start_x + 2,start_y + 6, BLACK);
  OledColorDisplay::DrawPixel(start_x + 3,start_y + 6, BLACK);
  OledColorDisplay::DrawPixel(start_x + 4,start_y + 6, BLACK);
  OledColorDisplay::DrawPixel(start_x + 0,start_y + 7, BLACK);
  OledColorDisplay::DrawPixel(start_x + 1,start_y + 7, BLACK);
  OledColorDisplay::DrawPixel(start_x + 2,start_y + 7, BLACK);
  OledColorDisplay::DrawPixel(start_x + 3,start_y + 7, BLACK);
  OledColorDisplay::DrawPixel(start_x + 4,start_y + 7, BLACK);
} else if ( mydrawchar == "1" ) {
  OledColorDisplay::DrawPixel(start_x + 0,start_y + 0, BLACK);
  OledColorDisplay::DrawPixel(start_x + 1,start_y + 0, BLACK);
  OledColorDisplay::DrawPixel(start_x + 2,start_y + 0, BLACK);
  OledColorDisplay::DrawPixel(start_x + 3,start_y + 0, BLACK);
  OledColorDisplay::DrawPixel(start_x + 4,start_y + 0, BLACK);
  OledColorDisplay::DrawPixel(start_x + 0,start_y + 1, BLACK);
  OledColorDisplay::DrawPixel(start_x + 1,start_y + 1, BLACK);
  OledColorDisplay::DrawPixel(start_x + 2,start_y + 1, GREEN);
  OledColorDisplay::DrawPixel(start_x + 3,start_y + 1, BLACK);
  OledColorDisplay::DrawPixel(start_x + 4,start_y + 1, BLACK);
  OledColorDisplay::DrawPixel(start_x + 0,start_y + 2, BLACK);
  OledColorDisplay::DrawPixel(start_x + 1,start_y + 2, GREEN);
  OledColorDisplay::DrawPixel(start_x + 2,start_y + 2, GREEN);
  OledColorDisplay::DrawPixel(start_x + 3,start_y + 2, BLACK);
  OledColorDisplay::DrawPixel(start_x + 4,start_y + 2, BLACK);
  OledColorDisplay::DrawPixel(start_x + 0,start_y + 3, BLACK);
  OledColorDisplay::DrawPixel(start_x + 1,start_y + 3, BLACK);
  OledColorDisplay::DrawPixel(start_x + 2,start_y + 3, GREEN);
  OledColorDisplay::DrawPixel(start_x + 3,start_y + 3, BLACK);
  OledColorDisplay::DrawPixel(start_x + 4,start_y + 3, BLACK);
  OledColorDisplay::DrawPixel(start_x + 0,start_y + 4, BLACK);
  OledColorDisplay::DrawPixel(start_x + 1,start_y + 4, BLACK);
  OledColorDisplay::DrawPixel(start_x + 2,start_y + 4, GREEN);
  OledColorDisplay::DrawPixel(start_x + 3,start_y + 4, BLACK);
  OledColorDisplay::DrawPixel(start_x + 4,start_y + 4, BLACK);
  OledColorDisplay::DrawPixel(start_x + 0,start_y + 5, BLACK);
  OledColorDisplay::DrawPixel(start_x + 1,start_y + 5, BLACK);
  OledColorDisplay::DrawPixel(start_x + 2,start_y + 5, GREEN);
  OledColorDisplay::DrawPixel(start_x + 3,start_y + 5, BLACK);
  OledColorDisplay::DrawPixel(start_x + 4,start_y + 5, BLACK);
  OledColorDisplay::DrawPixel(start_x + 0,start_y + 6, BLACK);
  OledColorDisplay::DrawPixel(start_x + 1,start_y + 6, BLACK);
  OledColorDisplay::DrawPixel(start_x + 2,start_y + 6, GREEN);
  OledColorDisplay::DrawPixel(start_x + 3,start_y + 6, BLACK);
  OledColorDisplay::DrawPixel(start_x + 4,start_y + 6, BLACK);
  OledColorDisplay::DrawPixel(start_x + 0,start_y + 7, BLACK);
  OledColorDisplay::DrawPixel(start_x + 1,start_y + 7, GREEN);
  OledColorDisplay::DrawPixel(start_x + 2,start_y + 7, GREEN);
  OledColorDisplay::DrawPixel(start_x + 3,start_y + 7, GREEN);
  OledColorDisplay::DrawPixel(start_x + 4,start_y + 7, BLACK);
} else if ( mydrawchar == "2" ) {
  OledColorDisplay::DrawPixel(start_x + 0,start_y + 0, BLACK);
  OledColorDisplay::DrawPixel(start_x + 1,start_y + 0, BLACK);
  OledColorDisplay::DrawPixel(start_x + 2,start_y + 0, BLACK);
  OledColorDisplay::DrawPixel(start_x + 3,start_y + 0, BLACK);
  OledColorDisplay::DrawPixel(start_x + 4,start_y + 0, BLACK);
  OledColorDisplay::DrawPixel(start_x + 0,start_y + 1, BLACK);
  OledColorDisplay::DrawPixel(start_x + 1,start_y + 1, GREEN);
  OledColorDisplay::DrawPixel(start_x + 2,start_y + 1, GREEN);
  OledColorDisplay::DrawPixel(start_x + 3,start_y + 1, GREEN);
  OledColorDisplay::DrawPixel(start_x + 4,start_y + 1, BLACK);
  OledColorDisplay::DrawPixel(start_x + 0,start_y + 2, GREEN);
  OledColorDisplay::DrawPixel(start_x + 1,start_y + 2, BLACK);
  OledColorDisplay::DrawPixel(start_x + 2,start_y + 2, BLACK);
  OledColorDisplay::DrawPixel(start_x + 3,start_y + 2, BLACK);
  OledColorDisplay::DrawPixel(start_x + 4,start_y + 2, GREEN);
  OledColorDisplay::DrawPixel(start_x + 0,start_y + 3, BLACK);
  OledColorDisplay::DrawPixel(start_x + 1,start_y + 3, BLACK);
  OledColorDisplay::DrawPixel(start_x + 2,start_y + 3, BLACK);
  OledColorDisplay::DrawPixel(start_x + 3,start_y + 3, BLACK);
  OledColorDisplay::DrawPixel(start_x + 4,start_y + 3, GREEN);
  OledColorDisplay::DrawPixel(start_x + 0,start_y + 4, BLACK);
  OledColorDisplay::DrawPixel(start_x + 1,start_y + 4, BLACK);
  OledColorDisplay::DrawPixel(start_x + 2,start_y + 4, BLACK);
  OledColorDisplay::DrawPixel(start_x + 3,start_y + 4, GREEN);
  OledColorDisplay::DrawPixel(start_x + 4,start_y + 4, BLACK);
  OledColorDisplay::DrawPixel(start_x + 0,start_y + 5, BLACK);
  OledColorDisplay::DrawPixel(start_x + 1,start_y + 5, BLACK);
  OledColorDisplay::DrawPixel(start_x + 2,start_y + 5, GREEN);
  OledColorDisplay::DrawPixel(start_x + 3,start_y + 5, BLACK);
  OledColorDisplay::DrawPixel(start_x + 4,start_y + 5, BLACK);
  OledColorDisplay::DrawPixel(start_x + 0,start_y + 6, BLACK);
  OledColorDisplay::DrawPixel(start_x + 1,start_y + 6, GREEN);
  OledColorDisplay::DrawPixel(start_x + 2,start_y + 6, BLACK);
  OledColorDisplay::DrawPixel(start_x + 3,start_y + 6, BLACK);
  OledColorDisplay::DrawPixel(start_x + 4,start_y + 6, BLACK);
  OledColorDisplay::DrawPixel(start_x + 0,start_y + 7, GREEN);
  OledColorDisplay::DrawPixel(start_x + 1,start_y + 7, GREEN);
  OledColorDisplay::DrawPixel(start_x + 2,start_y + 7, GREEN);
  OledColorDisplay::DrawPixel(start_x + 3,start_y + 7, GREEN);
  OledColorDisplay::DrawPixel(start_x + 4,start_y + 7, GREEN);
} else if ( mydrawchar == "3" ) {
  OledColorDisplay::DrawPixel(start_x + 0,start_y + 0, BLACK);
  OledColorDisplay::DrawPixel(start_x + 1,start_y + 0, BLACK);
  OledColorDisplay::DrawPixel(start_x + 2,start_y + 0, BLACK);
  OledColorDisplay::DrawPixel(start_x + 3,start_y + 0, BLACK);
  OledColorDisplay::DrawPixel(start_x + 4,start_y + 0, BLACK);
  OledColorDisplay::DrawPixel(start_x + 0,start_y + 1, BLACK);
  OledColorDisplay::DrawPixel(start_x + 1,start_y + 1, GREEN);
  OledColorDisplay::DrawPixel(start_x + 2,start_y + 1, GREEN);
  OledColorDisplay::DrawPixel(start_x + 3,start_y + 1, GREEN);
  OledColorDisplay::DrawPixel(start_x + 4,start_y + 1, BLACK);
  OledColorDisplay::DrawPixel(start_x + 0,start_y + 2, GREEN);
  OledColorDisplay::DrawPixel(start_x + 1,start_y + 2, BLACK);
  OledColorDisplay::DrawPixel(start_x + 2,start_y + 2, BLACK);
  OledColorDisplay::DrawPixel(start_x + 3,start_y + 2, BLACK);
  OledColorDisplay::DrawPixel(start_x + 4,start_y + 2, GREEN);
  OledColorDisplay::DrawPixel(start_x + 0,start_y + 3, BLACK);
  OledColorDisplay::DrawPixel(start_x + 1,start_y + 3, BLACK);
  OledColorDisplay::DrawPixel(start_x + 2,start_y + 3, BLACK);
  OledColorDisplay::DrawPixel(start_x + 3,start_y + 3, BLACK);
  OledColorDisplay::DrawPixel(start_x + 4,start_y + 3, GREEN);
  OledColorDisplay::DrawPixel(start_x + 0,start_y + 4, BLACK);
  OledColorDisplay::DrawPixel(start_x + 1,start_y + 4, BLACK);
  OledColorDisplay::DrawPixel(start_x + 2,start_y + 4, GREEN);
  OledColorDisplay::DrawPixel(start_x + 3,start_y + 4, GREEN);
  OledColorDisplay::DrawPixel(start_x + 4,start_y + 4, BLACK);
  OledColorDisplay::DrawPixel(start_x + 0,start_y + 5, BLACK);
  OledColorDisplay::DrawPixel(start_x + 1,start_y + 5, BLACK);
  OledColorDisplay::DrawPixel(start_x + 2,start_y + 5, BLACK);
  OledColorDisplay::DrawPixel(start_x + 3,start_y + 5, BLACK);
  OledColorDisplay::DrawPixel(start_x + 4,start_y + 5, GREEN);
  OledColorDisplay::DrawPixel(start_x + 0,start_y + 6, GREEN);
  OledColorDisplay::DrawPixel(start_x + 1,start_y + 6, BLACK);
  OledColorDisplay::DrawPixel(start_x + 2,start_y + 6, BLACK);
  OledColorDisplay::DrawPixel(start_x + 3,start_y + 6, BLACK);
  OledColorDisplay::DrawPixel(start_x + 4,start_y + 6, GREEN);
  OledColorDisplay::DrawPixel(start_x + 0,start_y + 7, BLACK);
  OledColorDisplay::DrawPixel(start_x + 1,start_y + 7, GREEN);
  OledColorDisplay::DrawPixel(start_x + 2,start_y + 7, GREEN);
  OledColorDisplay::DrawPixel(start_x + 3,start_y + 7, GREEN);
  OledColorDisplay::DrawPixel(start_x + 4,start_y + 7, BLACK);
} else if ( mydrawchar == "4" ) {
  OledColorDisplay::DrawPixel(start_x + 0,start_y + 0, BLACK);
  OledColorDisplay::DrawPixel(start_x + 1,start_y + 0, BLACK);
  OledColorDisplay::DrawPixel(start_x + 2,start_y + 0, BLACK);
  OledColorDisplay::DrawPixel(start_x + 3,start_y + 0, BLACK);
  OledColorDisplay::DrawPixel(start_x + 4,start_y + 0, BLACK);
  OledColorDisplay::DrawPixel(start_x + 0,start_y + 1, GREEN);
  OledColorDisplay::DrawPixel(start_x + 1,start_y + 1, BLACK);
  OledColorDisplay::DrawPixel(start_x + 2,start_y + 1, BLACK);
  OledColorDisplay::DrawPixel(start_x + 3,start_y + 1, BLACK);
  OledColorDisplay::DrawPixel(start_x + 4,start_y + 1, GREEN);
  OledColorDisplay::DrawPixel(start_x + 0,start_y + 2, GREEN);
  OledColorDisplay::DrawPixel(start_x + 1,start_y + 2, BLACK);
  OledColorDisplay::DrawPixel(start_x + 2,start_y + 2, BLACK);
  OledColorDisplay::DrawPixel(start_x + 3,start_y + 2, BLACK);
  OledColorDisplay::DrawPixel(start_x + 4,start_y + 2, GREEN);
  OledColorDisplay::DrawPixel(start_x + 0,start_y + 3, GREEN);
  OledColorDisplay::DrawPixel(start_x + 1,start_y + 3, BLACK);
  OledColorDisplay::DrawPixel(start_x + 2,start_y + 3, BLACK);
  OledColorDisplay::DrawPixel(start_x + 3,start_y + 3, BLACK);
  OledColorDisplay::DrawPixel(start_x + 4,start_y + 3, GREEN);
  OledColorDisplay::DrawPixel(start_x + 0,start_y + 4, GREEN);
  OledColorDisplay::DrawPixel(start_x + 1,start_y + 4, GREEN);
  OledColorDisplay::DrawPixel(start_x + 2,start_y + 4, GREEN);
  OledColorDisplay::DrawPixel(start_x + 3,start_y + 4, GREEN);
  OledColorDisplay::DrawPixel(start_x + 4,start_y + 4, GREEN);
  OledColorDisplay::DrawPixel(start_x + 0,start_y + 5, BLACK);
  OledColorDisplay::DrawPixel(start_x + 1,start_y + 5, BLACK);
  OledColorDisplay::DrawPixel(start_x + 2,start_y + 5, BLACK);
  OledColorDisplay::DrawPixel(start_x + 3,start_y + 5, BLACK);
  OledColorDisplay::DrawPixel(start_x + 4,start_y + 5, GREEN);
  OledColorDisplay::DrawPixel(start_x + 0,start_y + 6, BLACK);
  OledColorDisplay::DrawPixel(start_x + 1,start_y + 6, BLACK);
  OledColorDisplay::DrawPixel(start_x + 2,start_y + 6, BLACK);
  OledColorDisplay::DrawPixel(start_x + 3,start_y + 6, BLACK);
  OledColorDisplay::DrawPixel(start_x + 4,start_y + 6, GREEN);
  OledColorDisplay::DrawPixel(start_x + 0,start_y + 7, BLACK);
  OledColorDisplay::DrawPixel(start_x + 1,start_y + 7, BLACK);
  OledColorDisplay::DrawPixel(start_x + 2,start_y + 7, BLACK);
  OledColorDisplay::DrawPixel(start_x + 3,start_y + 7, BLACK);
  OledColorDisplay::DrawPixel(start_x + 4,start_y + 7, GREEN);
} else if ( mydrawchar == "5" ) {
  OledColorDisplay::DrawPixel(start_x + 0,start_y + 0, BLACK);
  OledColorDisplay::DrawPixel(start_x + 1,start_y + 0, BLACK);
  OledColorDisplay::DrawPixel(start_x + 2,start_y + 0, BLACK);
  OledColorDisplay::DrawPixel(start_x + 3,start_y + 0, BLACK);
  OledColorDisplay::DrawPixel(start_x + 4,start_y + 0, BLACK);
  OledColorDisplay::DrawPixel(start_x + 0,start_y + 1, GREEN);
  OledColorDisplay::DrawPixel(start_x + 1,start_y + 1, GREEN);
  OledColorDisplay::DrawPixel(start_x + 2,start_y + 1, GREEN);
  OledColorDisplay::DrawPixel(start_x + 3,start_y + 1, GREEN);
  OledColorDisplay::DrawPixel(start_x + 4,start_y + 1, GREEN);
  OledColorDisplay::DrawPixel(start_x + 0,start_y + 2, GREEN);
  OledColorDisplay::DrawPixel(start_x + 1,start_y + 2, BLACK);
  OledColorDisplay::DrawPixel(start_x + 2,start_y + 2, BLACK);
  OledColorDisplay::DrawPixel(start_x + 3,start_y + 2, BLACK);
  OledColorDisplay::DrawPixel(start_x + 4,start_y + 2, BLACK);
  OledColorDisplay::DrawPixel(start_x + 0,start_y + 3, GREEN);
  OledColorDisplay::DrawPixel(start_x + 1,start_y + 3, BLACK);
  OledColorDisplay::DrawPixel(start_x + 2,start_y + 3, BLACK);
  OledColorDisplay::DrawPixel(start_x + 3,start_y + 3, BLACK);
  OledColorDisplay::DrawPixel(start_x + 4,start_y + 3, BLACK);
  OledColorDisplay::DrawPixel(start_x + 0,start_y + 4, GREEN);
  OledColorDisplay::DrawPixel(start_x + 1,start_y + 4, GREEN);
  OledColorDisplay::DrawPixel(start_x + 2,start_y + 4, GREEN);
  OledColorDisplay::DrawPixel(start_x + 3,start_y + 4, GREEN);
  OledColorDisplay::DrawPixel(start_x + 4,start_y + 4, BLACK);
  OledColorDisplay::DrawPixel(start_x + 0,start_y + 5, BLACK);
  OledColorDisplay::DrawPixel(start_x + 1,start_y + 5, BLACK);
  OledColorDisplay::DrawPixel(start_x + 2,start_y + 5, BLACK);
  OledColorDisplay::DrawPixel(start_x + 3,start_y + 5, BLACK);
  OledColorDisplay::DrawPixel(start_x + 4,start_y + 5, GREEN);
  OledColorDisplay::DrawPixel(start_x + 0,start_y + 6, BLACK);
  OledColorDisplay::DrawPixel(start_x + 1,start_y + 6, BLACK);
  OledColorDisplay::DrawPixel(start_x + 2,start_y + 6, BLACK);
  OledColorDisplay::DrawPixel(start_x + 3,start_y + 6, BLACK);
  OledColorDisplay::DrawPixel(start_x + 4,start_y + 6, GREEN);
  OledColorDisplay::DrawPixel(start_x + 0,start_y + 7, GREEN);
  OledColorDisplay::DrawPixel(start_x + 1,start_y + 7, GREEN);
  OledColorDisplay::DrawPixel(start_x + 2,start_y + 7, GREEN);
  OledColorDisplay::DrawPixel(start_x + 3,start_y + 7, GREEN);
  OledColorDisplay::DrawPixel(start_x + 4,start_y + 7, BLACK);
} else if ( mydrawchar == "6" ) {
  OledColorDisplay::DrawPixel(start_x + 0,start_y + 0, BLACK);
  OledColorDisplay::DrawPixel(start_x + 1,start_y + 0, BLACK);
  OledColorDisplay::DrawPixel(start_x + 2,start_y + 0, BLACK);
  OledColorDisplay::DrawPixel(start_x + 3,start_y + 0, BLACK);
  OledColorDisplay::DrawPixel(start_x + 4,start_y + 0, BLACK);
  OledColorDisplay::DrawPixel(start_x + 0,start_y + 1, BLACK);
  OledColorDisplay::DrawPixel(start_x + 1,start_y + 1, GREEN);
  OledColorDisplay::DrawPixel(start_x + 2,start_y + 1, GREEN);
  OledColorDisplay::DrawPixel(start_x + 3,start_y + 1, GREEN);
  OledColorDisplay::DrawPixel(start_x + 4,start_y + 1, BLACK);
  OledColorDisplay::DrawPixel(start_x + 0,start_y + 2, GREEN);
  OledColorDisplay::DrawPixel(start_x + 1,start_y + 2, BLACK);
  OledColorDisplay::DrawPixel(start_x + 2,start_y + 2, BLACK);
  OledColorDisplay::DrawPixel(start_x + 3,start_y + 2, BLACK);
  OledColorDisplay::DrawPixel(start_x + 4,start_y + 2, GREEN);
  OledColorDisplay::DrawPixel(start_x + 0,start_y + 3, GREEN);
  OledColorDisplay::DrawPixel(start_x + 1,start_y + 3, BLACK);
  OledColorDisplay::DrawPixel(start_x + 2,start_y + 3, BLACK);
  OledColorDisplay::DrawPixel(start_x + 3,start_y + 3, BLACK);
  OledColorDisplay::DrawPixel(start_x + 4,start_y + 3, BLACK);
  OledColorDisplay::DrawPixel(start_x + 0,start_y + 4, GREEN);
  OledColorDisplay::DrawPixel(start_x + 1,start_y + 4, GREEN);
  OledColorDisplay::DrawPixel(start_x + 2,start_y + 4, GREEN);
  OledColorDisplay::DrawPixel(start_x + 3,start_y + 4, GREEN);
  OledColorDisplay::DrawPixel(start_x + 4,start_y + 4, BLACK);
  OledColorDisplay::DrawPixel(start_x + 0,start_y + 5, GREEN);
  OledColorDisplay::DrawPixel(start_x + 1,start_y + 5, BLACK);
  OledColorDisplay::DrawPixel(start_x + 2,start_y + 5, BLACK);
  OledColorDisplay::DrawPixel(start_x + 3,start_y + 5, BLACK);
  OledColorDisplay::DrawPixel(start_x + 4,start_y + 5, GREEN);
  OledColorDisplay::DrawPixel(start_x + 0,start_y + 6, GREEN);
  OledColorDisplay::DrawPixel(start_x + 1,start_y + 6, BLACK);
  OledColorDisplay::DrawPixel(start_x + 2,start_y + 6, BLACK);
  OledColorDisplay::DrawPixel(start_x + 3,start_y + 6, BLACK);
  OledColorDisplay::DrawPixel(start_x + 4,start_y + 6, GREEN);
  OledColorDisplay::DrawPixel(start_x + 0,start_y + 7, BLACK);
  OledColorDisplay::DrawPixel(start_x + 1,start_y + 7, GREEN);
  OledColorDisplay::DrawPixel(start_x + 2,start_y + 7, GREEN);
  OledColorDisplay::DrawPixel(start_x + 3,start_y + 7, GREEN);
  OledColorDisplay::DrawPixel(start_x + 4,start_y + 7, BLACK);
} else if ( mydrawchar == "7" ) {
  OledColorDisplay::DrawPixel(start_x + 0,start_y + 0, BLACK);
  OledColorDisplay::DrawPixel(start_x + 1,start_y + 0, BLACK);
  OledColorDisplay::DrawPixel(start_x + 2,start_y + 0, BLACK);
  OledColorDisplay::DrawPixel(start_x + 3,start_y + 0, BLACK);
  OledColorDisplay::DrawPixel(start_x + 4,start_y + 0, BLACK);
  OledColorDisplay::DrawPixel(start_x + 0,start_y + 1, GREEN);
  OledColorDisplay::DrawPixel(start_x + 1,start_y + 1, GREEN);
  OledColorDisplay::DrawPixel(start_x + 2,start_y + 1, GREEN);
  OledColorDisplay::DrawPixel(start_x + 3,start_y + 1, GREEN);
  OledColorDisplay::DrawPixel(start_x + 4,start_y + 1, GREEN);
  OledColorDisplay::DrawPixel(start_x + 0,start_y + 2, BLACK);
  OledColorDisplay::DrawPixel(start_x + 1,start_y + 2, BLACK);
  OledColorDisplay::DrawPixel(start_x + 2,start_y + 2, BLACK);
  OledColorDisplay::DrawPixel(start_x + 3,start_y + 2, BLACK);
  OledColorDisplay::DrawPixel(start_x + 4,start_y + 2, GREEN);
  OledColorDisplay::DrawPixel(start_x + 0,start_y + 3, BLACK);
  OledColorDisplay::DrawPixel(start_x + 1,start_y + 3, BLACK);
  OledColorDisplay::DrawPixel(start_x + 2,start_y + 3, BLACK);
  OledColorDisplay::DrawPixel(start_x + 3,start_y + 3, BLACK);
  OledColorDisplay::DrawPixel(start_x + 4,start_y + 3, GREEN);
  OledColorDisplay::DrawPixel(start_x + 0,start_y + 4, BLACK);
  OledColorDisplay::DrawPixel(start_x + 1,start_y + 4, BLACK);
  OledColorDisplay::DrawPixel(start_x + 2,start_y + 4, BLACK);
  OledColorDisplay::DrawPixel(start_x + 3,start_y + 4, GREEN);
  OledColorDisplay::DrawPixel(start_x + 4,start_y + 4, BLACK);
  OledColorDisplay::DrawPixel(start_x + 0,start_y + 5, BLACK);
  OledColorDisplay::DrawPixel(start_x + 1,start_y + 5, BLACK);
  OledColorDisplay::DrawPixel(start_x + 2,start_y + 5, GREEN);
  OledColorDisplay::DrawPixel(start_x + 3,start_y + 5, BLACK);
  OledColorDisplay::DrawPixel(start_x + 4,start_y + 5, BLACK);
  OledColorDisplay::DrawPixel(start_x + 0,start_y + 6, BLACK);
  OledColorDisplay::DrawPixel(start_x + 1,start_y + 6, GREEN);
  OledColorDisplay::DrawPixel(start_x + 2,start_y + 6, BLACK);
  OledColorDisplay::DrawPixel(start_x + 3,start_y + 6, BLACK);
  OledColorDisplay::DrawPixel(start_x + 4,start_y + 6, BLACK);
  OledColorDisplay::DrawPixel(start_x + 0,start_y + 7, GREEN);
  OledColorDisplay::DrawPixel(start_x + 1,start_y + 7, BLACK);
  OledColorDisplay::DrawPixel(start_x + 2,start_y + 7, BLACK);
  OledColorDisplay::DrawPixel(start_x + 3,start_y + 7, BLACK);
  OledColorDisplay::DrawPixel(start_x + 4,start_y + 7, BLACK);
} else if ( mydrawchar == "8" ) {
  OledColorDisplay::DrawPixel(start_x + 0,start_y + 0, BLACK);
  OledColorDisplay::DrawPixel(start_x + 1,start_y + 0, BLACK);
  OledColorDisplay::DrawPixel(start_x + 2,start_y + 0, BLACK);
  OledColorDisplay::DrawPixel(start_x + 3,start_y + 0, BLACK);
  OledColorDisplay::DrawPixel(start_x + 4,start_y + 0, BLACK);
  OledColorDisplay::DrawPixel(start_x + 0,start_y + 1, BLACK);
  OledColorDisplay::DrawPixel(start_x + 1,start_y + 1, GREEN);
  OledColorDisplay::DrawPixel(start_x + 2,start_y + 1, GREEN);
  OledColorDisplay::DrawPixel(start_x + 3,start_y + 1, GREEN);
  OledColorDisplay::DrawPixel(start_x + 4,start_y + 1, BLACK);
  OledColorDisplay::DrawPixel(start_x + 0,start_y + 2, GREEN);
  OledColorDisplay::DrawPixel(start_x + 1,start_y + 2, BLACK);
  OledColorDisplay::DrawPixel(start_x + 2,start_y + 2, BLACK);
  OledColorDisplay::DrawPixel(start_x + 3,start_y + 2, BLACK);
  OledColorDisplay::DrawPixel(start_x + 4,start_y + 2, GREEN);
  OledColorDisplay::DrawPixel(start_x + 0,start_y + 3, GREEN);
  OledColorDisplay::DrawPixel(start_x + 1,start_y + 3, BLACK);
  OledColorDisplay::DrawPixel(start_x + 2,start_y + 3, BLACK);
  OledColorDisplay::DrawPixel(start_x + 3,start_y + 3, BLACK);
  OledColorDisplay::DrawPixel(start_x + 4,start_y + 3, GREEN);
  OledColorDisplay::DrawPixel(start_x + 0,start_y + 4, BLACK);
  OledColorDisplay::DrawPixel(start_x + 1,start_y + 4, GREEN);
  OledColorDisplay::DrawPixel(start_x + 2,start_y + 4, GREEN);
  OledColorDisplay::DrawPixel(start_x + 3,start_y + 4, GREEN);
  OledColorDisplay::DrawPixel(start_x + 4,start_y + 4, BLACK);
  OledColorDisplay::DrawPixel(start_x + 0,start_y + 5, GREEN);
  OledColorDisplay::DrawPixel(start_x + 1,start_y + 5, BLACK);
  OledColorDisplay::DrawPixel(start_x + 2,start_y + 5, BLACK);
  OledColorDisplay::DrawPixel(start_x + 3,start_y + 5, BLACK);
  OledColorDisplay::DrawPixel(start_x + 4,start_y + 5, GREEN);
  OledColorDisplay::DrawPixel(start_x + 0,start_y + 6, GREEN);
  OledColorDisplay::DrawPixel(start_x + 1,start_y + 6, BLACK);
  OledColorDisplay::DrawPixel(start_x + 2,start_y + 6, BLACK);
  OledColorDisplay::DrawPixel(start_x + 3,start_y + 6, BLACK);
  OledColorDisplay::DrawPixel(start_x + 4,start_y + 6, GREEN);
  OledColorDisplay::DrawPixel(start_x + 0,start_y + 7, BLACK);
  OledColorDisplay::DrawPixel(start_x + 1,start_y + 7, GREEN);
  OledColorDisplay::DrawPixel(start_x + 2,start_y + 7, GREEN);
  OledColorDisplay::DrawPixel(start_x + 3,start_y + 7, GREEN);
  OledColorDisplay::DrawPixel(start_x + 4,start_y + 7, BLACK);
} else if ( mydrawchar == "9" ) {
  OledColorDisplay::DrawPixel(start_x + 0,start_y + 0, BLACK);
  OledColorDisplay::DrawPixel(start_x + 1,start_y + 0, BLACK);
  OledColorDisplay::DrawPixel(start_x + 2,start_y + 0, BLACK);
  OledColorDisplay::DrawPixel(start_x + 3,start_y + 0, BLACK);
  OledColorDisplay::DrawPixel(start_x + 4,start_y + 0, BLACK);
  OledColorDisplay::DrawPixel(start_x + 0,start_y + 1, BLACK);
  OledColorDisplay::DrawPixel(start_x + 1,start_y + 1, GREEN);
  OledColorDisplay::DrawPixel(start_x + 2,start_y + 1, GREEN);
  OledColorDisplay::DrawPixel(start_x + 3,start_y + 1, GREEN);
  OledColorDisplay::DrawPixel(start_x + 4,start_y + 1, BLACK);
  OledColorDisplay::DrawPixel(start_x + 0,start_y + 2, GREEN);
  OledColorDisplay::DrawPixel(start_x + 1,start_y + 2, BLACK);
  OledColorDisplay::DrawPixel(start_x + 2,start_y + 2, BLACK);
  OledColorDisplay::DrawPixel(start_x + 3,start_y + 2, BLACK);
  OledColorDisplay::DrawPixel(start_x + 4,start_y + 2, GREEN);
  OledColorDisplay::DrawPixel(start_x + 0,start_y + 3, GREEN);
  OledColorDisplay::DrawPixel(start_x + 1,start_y + 3, BLACK);
  OledColorDisplay::DrawPixel(start_x + 2,start_y + 3, BLACK);
  OledColorDisplay::DrawPixel(start_x + 3,start_y + 3, BLACK);
  OledColorDisplay::DrawPixel(start_x + 4,start_y + 3, GREEN);
  OledColorDisplay::DrawPixel(start_x + 0,start_y + 4, BLACK);
  OledColorDisplay::DrawPixel(start_x + 1,start_y + 4, GREEN);
  OledColorDisplay::DrawPixel(start_x + 2,start_y + 4, GREEN);
  OledColorDisplay::DrawPixel(start_x + 3,start_y + 4, GREEN);
  OledColorDisplay::DrawPixel(start_x + 4,start_y + 4, GREEN);
  OledColorDisplay::DrawPixel(start_x + 0,start_y + 5, BLACK);
  OledColorDisplay::DrawPixel(start_x + 1,start_y + 5, BLACK);
  OledColorDisplay::DrawPixel(start_x + 2,start_y + 5, BLACK);
  OledColorDisplay::DrawPixel(start_x + 3,start_y + 5, BLACK);
  OledColorDisplay::DrawPixel(start_x + 4,start_y + 5, GREEN);
  OledColorDisplay::DrawPixel(start_x + 0,start_y + 6, GREEN);
  OledColorDisplay::DrawPixel(start_x + 1,start_y + 6, BLACK);
  OledColorDisplay::DrawPixel(start_x + 2,start_y + 6, BLACK);
  OledColorDisplay::DrawPixel(start_x + 3,start_y + 6, BLACK);
  OledColorDisplay::DrawPixel(start_x + 4,start_y + 6, GREEN);
  OledColorDisplay::DrawPixel(start_x + 0,start_y + 7, BLACK);
  OledColorDisplay::DrawPixel(start_x + 1,start_y + 7, GREEN);
  OledColorDisplay::DrawPixel(start_x + 2,start_y + 7, GREEN);
  OledColorDisplay::DrawPixel(start_x + 3,start_y + 7, GREEN);
  OledColorDisplay::DrawPixel(start_x + 4,start_y + 7, BLACK);
} else if ( mydrawchar == "0" ) {
  OledColorDisplay::DrawPixel(start_x + 0,start_y + 0, BLACK);
  OledColorDisplay::DrawPixel(start_x + 1,start_y + 0, BLACK);
  OledColorDisplay::DrawPixel(start_x + 2,start_y + 0, BLACK);
  OledColorDisplay::DrawPixel(start_x + 3,start_y + 0, BLACK);
  OledColorDisplay::DrawPixel(start_x + 4,start_y + 0, BLACK);
  OledColorDisplay::DrawPixel(start_x + 0,start_y + 1, BLACK);
  OledColorDisplay::DrawPixel(start_x + 1,start_y + 1, GREEN);
  OledColorDisplay::DrawPixel(start_x + 2,start_y + 1, GREEN);
  OledColorDisplay::DrawPixel(start_x + 3,start_y + 1, GREEN);
  OledColorDisplay::DrawPixel(start_x + 4,start_y + 1, BLACK);
  OledColorDisplay::DrawPixel(start_x + 0,start_y + 2, GREEN);
  OledColorDisplay::DrawPixel(start_x + 1,start_y + 2, BLACK);
  OledColorDisplay::DrawPixel(start_x + 2,start_y + 2, BLACK);
  OledColorDisplay::DrawPixel(start_x + 3,start_y + 2, BLACK);
  OledColorDisplay::DrawPixel(start_x + 4,start_y + 2, GREEN);
  OledColorDisplay::DrawPixel(start_x + 0,start_y + 3, GREEN);
  OledColorDisplay::DrawPixel(start_x + 1,start_y + 3, BLACK);
  OledColorDisplay::DrawPixel(start_x + 2,start_y + 3, BLACK);
  OledColorDisplay::DrawPixel(start_x + 3,start_y + 3, GREEN);
  OledColorDisplay::DrawPixel(start_x + 4,start_y + 3, GREEN);
  OledColorDisplay::DrawPixel(start_x + 0,start_y + 4, GREEN);
  OledColorDisplay::DrawPixel(start_x + 1,start_y + 4, BLACK);
  OledColorDisplay::DrawPixel(start_x + 2,start_y + 4, GREEN);
  OledColorDisplay::DrawPixel(start_x + 3,start_y + 4, BLACK);
  OledColorDisplay::DrawPixel(start_x + 4,start_y + 4, GREEN);
  OledColorDisplay::DrawPixel(start_x + 0,start_y + 5, GREEN);
  OledColorDisplay::DrawPixel(start_x + 1,start_y + 5, GREEN);
  OledColorDisplay::DrawPixel(start_x + 2,start_y + 5, BLACK);
  OledColorDisplay::DrawPixel(start_x + 3,start_y + 5, BLACK);
  OledColorDisplay::DrawPixel(start_x + 4,start_y + 5, GREEN);
  OledColorDisplay::DrawPixel(start_x + 0,start_y + 6, GREEN);
  OledColorDisplay::DrawPixel(start_x + 1,start_y + 6, BLACK);
  OledColorDisplay::DrawPixel(start_x + 2,start_y + 6, BLACK);
  OledColorDisplay::DrawPixel(start_x + 3,start_y + 6, BLACK);
  OledColorDisplay::DrawPixel(start_x + 4,start_y + 6, GREEN);
  OledColorDisplay::DrawPixel(start_x + 0,start_y + 7, BLACK);
  OledColorDisplay::DrawPixel(start_x + 1,start_y + 7, GREEN);
  OledColorDisplay::DrawPixel(start_x + 2,start_y + 7, GREEN);
  OledColorDisplay::DrawPixel(start_x + 3,start_y + 7, GREEN);
  OledColorDisplay::DrawPixel(start_x + 4,start_y + 7, BLACK);
} else if ( mydrawchar == " " ) {
  OledColorDisplay::DrawPixel(start_x + 0,start_y + 0, BLACK);
  OledColorDisplay::DrawPixel(start_x + 1,start_y + 0, BLACK);
  OledColorDisplay::DrawPixel(start_x + 2,start_y + 0, BLACK);
  OledColorDisplay::DrawPixel(start_x + 3,start_y + 0, BLACK);
  OledColorDisplay::DrawPixel(start_x + 4,start_y + 0, BLACK);
  OledColorDisplay::DrawPixel(start_x + 0,start_y + 1, BLACK);
  OledColorDisplay::DrawPixel(start_x + 1,start_y + 1, BLACK);
  OledColorDisplay::DrawPixel(start_x + 2,start_y + 1, BLACK);
  OledColorDisplay::DrawPixel(start_x + 3,start_y + 1, BLACK);
  OledColorDisplay::DrawPixel(start_x + 4,start_y + 1, BLACK);
  OledColorDisplay::DrawPixel(start_x + 0,start_y + 2, BLACK);
  OledColorDisplay::DrawPixel(start_x + 1,start_y + 2, BLACK);
  OledColorDisplay::DrawPixel(start_x + 2,start_y + 2, BLACK);
  OledColorDisplay::DrawPixel(start_x + 3,start_y + 2, BLACK);
  OledColorDisplay::DrawPixel(start_x + 4,start_y + 2, BLACK);
  OledColorDisplay::DrawPixel(start_x + 0,start_y + 3, BLACK);
  OledColorDisplay::DrawPixel(start_x + 1,start_y + 3, BLACK);
  OledColorDisplay::DrawPixel(start_x + 2,start_y + 3, BLACK);
  OledColorDisplay::DrawPixel(start_x + 3,start_y + 3, BLACK);
  OledColorDisplay::DrawPixel(start_x + 4,start_y + 3, BLACK);
  OledColorDisplay::DrawPixel(start_x + 0,start_y + 4, BLACK);
  OledColorDisplay::DrawPixel(start_x + 1,start_y + 4, BLACK);
  OledColorDisplay::DrawPixel(start_x + 2,start_y + 4, BLACK);
  OledColorDisplay::DrawPixel(start_x + 3,start_y + 4, BLACK);
  OledColorDisplay::DrawPixel(start_x + 4,start_y + 4, BLACK);
  OledColorDisplay::DrawPixel(start_x + 0,start_y + 5, BLACK);
  OledColorDisplay::DrawPixel(start_x + 1,start_y + 5, BLACK);
  OledColorDisplay::DrawPixel(start_x + 2,start_y + 5, BLACK);
  OledColorDisplay::DrawPixel(start_x + 3,start_y + 5, BLACK);
  OledColorDisplay::DrawPixel(start_x + 4,start_y + 5, BLACK);
  OledColorDisplay::DrawPixel(start_x + 0,start_y + 6, BLACK);
  OledColorDisplay::DrawPixel(start_x + 1,start_y + 6, BLACK);
  OledColorDisplay::DrawPixel(start_x + 2,start_y + 6, BLACK);
  OledColorDisplay::DrawPixel(start_x + 3,start_y + 6, BLACK);
  OledColorDisplay::DrawPixel(start_x + 4,start_y + 6, BLACK);
  OledColorDisplay::DrawPixel(start_x + 0,start_y + 7, BLACK);
  OledColorDisplay::DrawPixel(start_x + 1,start_y + 7, BLACK);
  OledColorDisplay::DrawPixel(start_x + 2,start_y + 7, BLACK);
  OledColorDisplay::DrawPixel(start_x + 3,start_y + 7, BLACK);
  OledColorDisplay::DrawPixel(start_x + 4,start_y + 7, BLACK);
} else {
  OledColorDisplay::DrawPixel(start_x + 0,start_y + 0, GREEN);
  OledColorDisplay::DrawPixel(start_x + 1,start_y + 0, GREEN);
  OledColorDisplay::DrawPixel(start_x + 2,start_y + 0, GREEN);
  OledColorDisplay::DrawPixel(start_x + 3,start_y + 0, GREEN);
  OledColorDisplay::DrawPixel(start_x + 4,start_y + 0, GREEN);
  OledColorDisplay::DrawPixel(start_x + 0,start_y + 1, GREEN);
  OledColorDisplay::DrawPixel(start_x + 1,start_y + 1, GREEN);
  OledColorDisplay::DrawPixel(start_x + 2,start_y + 1, GREEN);
  OledColorDisplay::DrawPixel(start_x + 3,start_y + 1, GREEN);
  OledColorDisplay::DrawPixel(start_x + 4,start_y + 1, GREEN);
  OledColorDisplay::DrawPixel(start_x + 0,start_y + 2, GREEN);
  OledColorDisplay::DrawPixel(start_x + 1,start_y + 2, GREEN);
  OledColorDisplay::DrawPixel(start_x + 2,start_y + 2, GREEN);
  OledColorDisplay::DrawPixel(start_x + 3,start_y + 2, GREEN);
  OledColorDisplay::DrawPixel(start_x + 4,start_y + 2, GREEN);
  OledColorDisplay::DrawPixel(start_x + 0,start_y + 3, GREEN);
  OledColorDisplay::DrawPixel(start_x + 1,start_y + 3, GREEN);
  OledColorDisplay::DrawPixel(start_x + 2,start_y + 3, GREEN);
  OledColorDisplay::DrawPixel(start_x + 3,start_y + 3, GREEN);
  OledColorDisplay::DrawPixel(start_x + 4,start_y + 3, GREEN);
  OledColorDisplay::DrawPixel(start_x + 0,start_y + 4, GREEN);
  OledColorDisplay::DrawPixel(start_x + 1,start_y + 4, GREEN);
  OledColorDisplay::DrawPixel(start_x + 2,start_y + 4, GREEN);
  OledColorDisplay::DrawPixel(start_x + 3,start_y + 4, GREEN);
  OledColorDisplay::DrawPixel(start_x + 4,start_y + 4, GREEN);
  OledColorDisplay::DrawPixel(start_x + 0,start_y + 5, GREEN);
  OledColorDisplay::DrawPixel(start_x + 1,start_y + 5, GREEN);
  OledColorDisplay::DrawPixel(start_x + 2,start_y + 5, GREEN);
  OledColorDisplay::DrawPixel(start_x + 3,start_y + 5, GREEN);
  OledColorDisplay::DrawPixel(start_x + 4,start_y + 5, GREEN);
  OledColorDisplay::DrawPixel(start_x + 0,start_y + 6, GREEN);
  OledColorDisplay::DrawPixel(start_x + 1,start_y + 6, GREEN);
  OledColorDisplay::DrawPixel(start_x + 2,start_y + 6, GREEN);
  OledColorDisplay::DrawPixel(start_x + 3,start_y + 6, GREEN);
  OledColorDisplay::DrawPixel(start_x + 4,start_y + 6, GREEN);
  OledColorDisplay::DrawPixel(start_x + 0,start_y + 7, GREEN);
  OledColorDisplay::DrawPixel(start_x + 1,start_y + 7, GREEN);
  OledColorDisplay::DrawPixel(start_x + 2,start_y + 7, GREEN);
  OledColorDisplay::DrawPixel(start_x + 3,start_y + 7, GREEN);
  OledColorDisplay::DrawPixel(start_x + 4,start_y + 7, GREEN);
}

}
